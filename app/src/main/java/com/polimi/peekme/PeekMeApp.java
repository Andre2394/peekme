package com.polimi.peekme;

import android.app.Application;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.polimi.peekme.api.PeekMeServer;
import com.polimi.peekme.api.ServerConfig;
import com.polimi.peekme.storage.SQLiteCache;

/**
 * This class is called every time the android framework needs to start tha app.
 */
public class PeekMeApp extends Application {

    // Generic server info
    private static final String SERVER_HOSTNAME = "peekme.ddns.net";
    private static final String API_VERSION = "v1";
    private static final String CERTIFICATE_HASH = "tdFH7jyV5i7TxMcWtP+YrhXK7twk+LyRC8nWVlut3mc=";

    // OAuth2 client credentials
    private static final String CLIENT_ID = "jBOziYnXA5U5hEi";
    private static final String CLIENT_SECRET = "HbkcbfAF~dg-dC3secQUbUgHQrviTi_-z/N9.fyZ";

    @Override
    public void onCreate() {
        super.onCreate();
        ServerConfig configuration = new ServerConfig.Builder(this, SERVER_HOSTNAME)
                .withProtocol(ServerConfig.Protocol.HTTPS)
                .withApiVersion(API_VERSION)
                .withCertificatePinning(ServerConfig.PinAlgorithm.SHA256, CERTIFICATE_HASH)
                .withCache(new SQLiteCache(this))
                .build();
        PeekMeServer.initialize(configuration, CLIENT_ID, CLIENT_SECRET);
        DrawerImageLoader.init(new AbstractDrawerImageLoader() {

            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                Glide.with(PeekMeApp.this).load(uri).centerCrop().placeholder(placeholder).into(imageView);
            }

            /*
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder, String tag) {
                super.set(imageView, uri, placeholder, tag);
            }*/

            @Override
            public void cancel(ImageView imageView) {
                Glide.clear(imageView);
            }

            /*
            @Override
            public Drawable placeholder(Context ctx) {
                return super.placeholder(ctx);
            }

            @Override
            public Drawable placeholder(Context ctx, String tag) {
                return super.placeholder(ctx, tag);
            }*/

        });
    }
}