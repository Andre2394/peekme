package com.polimi.peekme.api;

import android.content.ContentResolver;
import android.content.Context;

import com.polimi.peekme.api.oauth2.OAuth2ClientCredentials;
import com.polimi.peekme.api.oauth2.OAuth2Manager;
import com.polimi.peekme.api.oauth2.OAuth2TokenStorage;
import com.polimi.peekme.api.storage.ICache;

import java.io.IOException;

import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by andre on 13/09/2016.
 */
public interface IServer {

    String getBaseUrl();

    String getTokenEndpoint();

    String getMediaEndpoint();

    OAuth2ClientCredentials getClientCredentials();

    OAuth2TokenStorage getTokenStorage();

    OAuth2Manager getOAuthManager();

    Response execute(Request request) throws IOException;

    ContentResolver getContentResolver();

    Context getContext();

    ICache getCache();
}