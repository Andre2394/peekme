package com.polimi.peekme.api;

import android.content.ContentResolver;
import android.content.Context;

import com.polimi.peekme.api.oauth2.OAuth2AccessToken;
import com.polimi.peekme.api.oauth2.OAuth2ClientCredentials;
import com.polimi.peekme.api.oauth2.OAuth2Interceptor;
import com.polimi.peekme.api.oauth2.OAuth2Manager;
import com.polimi.peekme.api.oauth2.OAuth2SessionManager;
import com.polimi.peekme.api.oauth2.OAuth2SharedPreferenceStorage;
import com.polimi.peekme.api.oauth2.OAuth2TokenStorage;
import com.polimi.peekme.api.service.ExploreService;
import com.polimi.peekme.api.service.PostService;
import com.polimi.peekme.api.service.SearchService;
import com.polimi.peekme.api.service.UserService;
import com.polimi.peekme.api.storage.ICache;
import com.polimi.peekme.storage.SQLiteCache;

import java.io.IOException;

import okhttp3.CertificatePinner;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by andre on 13/09/2016.
 */
public class PeekMeServer implements IServer {

    private static PeekMeServer mInstance;

    public static void initialize(ServerConfig serverConfig, String clientId, String clientSecret) {
        mInstance = new PeekMeServer(serverConfig, new OAuth2ClientCredentials(clientId, clientSecret));
    }

    private ServerConfig mConfiguration;

    private OkHttpClient mClient;
    private OAuth2ClientCredentials mClientCredentials;
    private OAuth2TokenStorage mTokenStorage;
    private OAuth2Manager mOAuth2Manager;
    private ICache mCache;

    private UserService mUserService;
    private PostService mPostService;
    private ExploreService mExploreService;
    private SearchService mSearchService;

    private PeekMeServer(ServerConfig configuration, OAuth2ClientCredentials clientCredentials) {
        mConfiguration = configuration;
        mClientCredentials = clientCredentials;

        initializeClient();
        initializeServices();
    }

    private void initializeClient() {
        Context context = mConfiguration.getContext();
        mTokenStorage = new OAuth2SharedPreferenceStorage(context);
        mOAuth2Manager = new OAuth2SessionManager(this, mTokenStorage);
        mCache = new SQLiteCache(context);
        CertificatePinner certificatePinner = mConfiguration.getCertificatePin();
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(new OAuth2Interceptor(mOAuth2Manager));
        if (certificatePinner != null) {
            builder.certificatePinner(certificatePinner);
        }
        mClient = builder.build();
    }

    private void initializeServices() {
        mUserService = new UserService(this);
        mPostService = new PostService(this);
        mExploreService = new ExploreService(this);
        mSearchService = new SearchService(this);
    }

    @Override
    public String getBaseUrl() {
        return mConfiguration.getBaseUrl();
    }

    @Override
    public String getTokenEndpoint() {
        return mConfiguration.getBaseUrl() + "/oauth/token";
    }

    @Override
    public String getMediaEndpoint() {
        return mConfiguration.getBaseUrl() + "/media";
    }

    @Override
    public OAuth2ClientCredentials getClientCredentials() {
        return mClientCredentials;
    }

    @Override
    public OAuth2TokenStorage getTokenStorage() {
        return mTokenStorage;
    }

    @Override
    public OAuth2Manager getOAuthManager() {
        return mOAuth2Manager;
    }

    @Override
    public Response execute(Request request) throws IOException {
        return mClient.newCall(request).execute();
    }

    @Override
    public ContentResolver getContentResolver() {
        return mConfiguration.getContext().getContentResolver();
    }

    @Override
    public Context getContext() {
        return mConfiguration.getContext();
    }

    @Override
    public ICache getCache() {
        return mCache;
    }

    public static UserService getUserService() {
        return mInstance.mUserService;
    }

    public static PostService getPostService() {
        return mInstance.mPostService;
    }

    public static ExploreService getExploreService() {
        return mInstance.mExploreService;
    }

    public static SearchService getSearchService() {
        return mInstance.mSearchService;
    }

    public static OAuth2AccessToken getToken() {
        return mInstance.getOAuthManager().getAccessToken();
    }

    public static String getResourceServerEndpoint() {
        return mInstance.getBaseUrl();
    }

    public static ServerState checkServerState(String version) {
        Request request = new Request.Builder()
                .url(mInstance.getBaseUrl() + "/status")
                .post(new FormBody.Builder()
                        .add(Protocol.PLATFORM, "android")
                        .add(Protocol.VERSION, version)
                        .build()
                )
                .build();
        try {
            Response response = mInstance.execute(request);
            // TODO check in response if status is "up_to_date" or "deprecated"
            return ServerState.ONLINE;
        } catch (IOException e) {
            e.printStackTrace();
            return ServerState.OFFLINE;
        }
    }
}