package com.polimi.peekme.api;

/**
 * Created by andre on 11/10/2016.
 */
public class Protocol {

    public static final String TIMESTAMP_PATTERN = "MM dd yyyy kk:mm:ss z";

    public static final String ID = "id";
    public static final String USER = "user";
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String FILE_URL = "file_url";
    public static final String MIME_TYPE = "mime_type";
    public static final String PICTURE = "picture";
    public static final String FOLLOWER_COUNT = "follower_count";
    public static final String FOLLOWED_COUNT = "followed_count";
    public static final String FOLLOWED_BY_YOU = "followed_by_you";
    public static final String POST = "post";

    public static final String MEDIA = "media";
    public static final String MEDIA_ID = "media_id";
    public static final String MEDIA_FILE = "media";
    public static final String PICTURE_ID = "picture_id";
    public static final String DESCRIPTION = "description";
    public static final String RATING = "rating";
    public static final String COMMENT_COUNT = "comment_count";
    public static final String COMMENT_TEXT = "comment_text";
    public static final String CREATED_AT = "created_at";
    public static final String UPDATED_AT = "updated_at";

    public static final String COUNT = "count";
    public static final String TOTAL = "total";
    public static final String USER_RATE = "user_rate";
    public static final String METADATA = "metadata";
    public static final String DATA = "data";
    public static final String GCM_TOKEN = "gcm_token";
    public static final String BEFORE = "before";
    public static final String AFTER = "after";

    public static final String PLATFORM = "platform";
    public static final String VERSION = "version";
    public static final String KEYWORD = "keyword";

    public static final String INVALID_PARAMETER = "invalid_parameter";
    public static final String MISSING_PARAMETER = "missing_parameter";
}