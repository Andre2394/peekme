package com.polimi.peekme.api;

/**
 * Created by andre on 27/09/2016.
 */
public interface ServerCallback<T> {

    void onRequestDone(T item, ServerException e);
}