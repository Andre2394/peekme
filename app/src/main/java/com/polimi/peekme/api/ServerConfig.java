package com.polimi.peekme.api;

import android.content.Context;

import com.polimi.peekme.api.storage.DefaultCache;
import com.polimi.peekme.api.storage.ICache;

import okhttp3.CertificatePinner;

/**
 * Created by andre on 19/10/2016.
 */
public class ServerConfig {

    private static final Protocol DEFAULT_PROTOCOL = Protocol.HTTPS;
    private static final String DEFAULT_API_VERSION = "v1";

    private Context mContext;
    private Protocol mProtocol;
    private String mHostname;
    private String mApiVersion;
    private CertificatePinner.Builder mCertificatePin;
    private ICache mCache;

    private ServerConfig() {
        // private constructor
    }

    /*package-local*/ Context getContext() {
        return mContext;
    }

    /*package-local*/ String getHostname() {
        return mHostname;
    }

    /*package-local*/ String getAddress() {
        return mProtocol.mProtocol + "://" + mHostname;
    }

    /*package-local*/ String getBaseUrl() {
        return getAddress() + "/" + mApiVersion;
    }

    /*package-local*/ CertificatePinner getCertificatePin() {
        return mCertificatePin != null ? mCertificatePin.build() : null;
    }

    public static class Builder {

        private ServerConfig mConfig;

        public Builder(Context context, String hostname) {
            mConfig = new ServerConfig();
            mConfig.mContext = context.getApplicationContext();
            mConfig.mHostname = hostname;
            mConfig.mProtocol = DEFAULT_PROTOCOL;
            mConfig.mApiVersion = DEFAULT_API_VERSION;
            mConfig.mCache = new DefaultCache(context);
        }

        public Builder withProtocol(Protocol protocol) {
            mConfig.mProtocol = protocol;
            return this;
        }

        public Builder withApiVersion(String apiVersion) {
            mConfig.mApiVersion = apiVersion;
            return this;
        }

        public Builder withCertificatePinning(PinAlgorithm algorithm, String hash) {
            if (mConfig.mCertificatePin == null) {
                mConfig.mCertificatePin = new CertificatePinner.Builder();
            }
            mConfig.mCertificatePin.add(mConfig.mHostname, algorithm.toString() + "/" + hash);
            return this;
        }

        public Builder withCache(ICache cache) {
            mConfig.mCache = cache;
            return this;
        }

        public ServerConfig build() {
            return mConfig;
        }
    }

    public enum Protocol {

        HTTP("http"),

        HTTPS("https");

        private final String mProtocol;

        Protocol(String protocol) {
            mProtocol = protocol;
        }

        public String toString() {
            return mProtocol;
        }
    }

    public enum PinAlgorithm {

        SHA256("sha256"),

        SHA1("sha1");

        private final String mAlgorithm;

        PinAlgorithm(String algorithm) {
            mAlgorithm = algorithm;
        }

        public String toString() {
            return mAlgorithm;
        }
    }
}