package com.polimi.peekme.api;

import com.polimi.peekme.api.oauth2.OAuth2Constants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andre on 13/09/2016.
 */
public class ServerException extends Exception {

    private String mError;
    private String mErrorDescription;

    public ServerException() {
        super();
        mError = OAuth2Constants.UNKNOWN_ERROR;
        mErrorDescription = null;
    }

    public ServerException(Throwable throwable) {
        super(throwable);
        mError = OAuth2Constants.UNKNOWN_ERROR;
        mErrorDescription = throwable.getMessage();
    }

    public ServerException(String body) {
        super(body);
        try {
            JSONObject jsonBody = new JSONObject(body);
            mError = jsonBody.getString(OAuth2Constants.ERROR);
            mErrorDescription = jsonBody.optString(OAuth2Constants.ERROR_DESCRIPTION, null);
        } catch (JSONException e) {
            mError = OAuth2Constants.UNKNOWN_ERROR;
            mErrorDescription = body;
        }
    }

    public ServerException(String error, String errorDescription) {
        super(error);
        mError = error;
        mErrorDescription = errorDescription;
    }

    public String getError() {
        return mError;
    }

    public String getErrorDescription() {
        return mErrorDescription;
    }
}