package com.polimi.peekme.api;

/**
 * Created by andre on 14/10/2016.
 */
public enum ServerState {

    ONLINE,

    UPDATE_APP,

    OFFLINE
}