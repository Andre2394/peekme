package com.polimi.peekme.api.model;

import android.content.ContentValues;

import com.polimi.peekme.api.Protocol;
import com.polimi.peekme.api.storage.Cacheable;
import com.polimi.peekme.storage.Schema;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andre on 26/09/2016.
 */
public class BaseUser extends ServerObject implements Identifiable, Cacheable {

    private final long mId;
    private final String mUsername;
    private final Picture mPicture;
    private final boolean mFollowedByYou;

    public BaseUser(JSONObject jsonObject) throws JSONException {
        super(jsonObject);
        mId = jsonObject.getLong(Protocol.ID);
        mUsername = jsonObject.getString(Protocol.USERNAME);
        mPicture = ModelFactory.parsePicture(jsonObject.optJSONObject(Protocol.PICTURE));
        mFollowedByYou = jsonObject.optBoolean(Protocol.FOLLOWED_BY_YOU, false);
    }

    @Override
    public long getId() {
        return mId;
    }

    public String getUsername() {
        return mUsername;
    }

    public Picture getPicture() {
        return mPicture;
    }

    public boolean hasPicture() {
        return mPicture != null;
    }

    public boolean isFollowedByYou() {
        return mFollowedByYou;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Schema.User.USER_ID, mId);
        contentValues.put(Schema.User.USER_USERNAME, mUsername);
        contentValues.put(Schema.User.USER_NAME, (String) null);
        contentValues.put(Schema.User.USER_SURNAME, (String) null);
        contentValues.put(Schema.User.USER_SEX, (String) null);
        contentValues.put(Schema.User.USER_BIRTHDAY, (String) null);
        contentValues.put(Schema.User.USER_PICTURE, mPicture != null ? mPicture.getId() : 0L);
        contentValues.put(Schema.User.USER_FOLLOWED_BY_YOU, mFollowedByYou);
        return contentValues;
    }
}