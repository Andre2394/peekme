package com.polimi.peekme.api.model;

import android.content.ContentValues;

import com.polimi.peekme.api.Protocol;
import com.polimi.peekme.api.storage.Cacheable;
import com.polimi.peekme.storage.Schema;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by andre on 22/10/2016.
 */
public class Comment extends ServerObject implements Identifiable, Cacheable {

    private long mId;
    private long mPostId;
    private BaseUser mUser;
    private String mText;
    private DateTime mCreatedAt;

    public Comment(JSONObject jsonObject) throws JSONException {
        super(jsonObject);
        mId = jsonObject.getLong(Protocol.ID);
        mPostId = jsonObject.getLong(Protocol.POST);
        mUser = new BaseUser(jsonObject.getJSONObject(Protocol.USER));
        mText = jsonObject.getString(Protocol.COMMENT_TEXT);
        mCreatedAt = parseTimestamp(jsonObject.getString(Protocol.CREATED_AT));
    }

    private DateTime parseTimestamp(String timestamp) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(Protocol.TIMESTAMP_PATTERN);
        return formatter.parseDateTime(timestamp);
    }

    @Override
    public long getId() {
        return mId;
    }

    public long getPostId() {
        return mPostId;
    }

    public BaseUser getUser() {
        return mUser;
    }

    public String getText() {
        return mText;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Schema.Comment.COMMENT_ID, mId);
        contentValues.put(Schema.Comment.COMMENT_POST_ID, mPostId);
        contentValues.put(Schema.Comment.COMMENT_USER_ID, mUser.getId());
        contentValues.put(Schema.Comment.COMMENT_TEXT, mText);
        contentValues.put(Schema.Comment.COMMENT_CREATED_AT, mCreatedAt.getMillis());
        return contentValues;
    }
}