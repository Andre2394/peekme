package com.polimi.peekme.api.model;

/**
 * Created by andre on 27/12/2016.
 */
public interface Identifiable {

    long getId();
}