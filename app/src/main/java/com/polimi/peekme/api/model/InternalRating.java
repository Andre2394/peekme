package com.polimi.peekme.api.model;

import com.polimi.peekme.api.Protocol;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andre on 18/10/2016.
 */
/*package-local*/ class InternalRating extends ServerObject {

    /*package-local*/ int mCount;
    /*package-local*/ int mTotal;
    /*package-local*/ int mUserRating;

    public InternalRating(JSONObject jsonObject) throws JSONException {
        super(jsonObject);
        mCount = jsonObject.getInt(Protocol.COUNT);
        mTotal = jsonObject.getInt(Protocol.TOTAL);
        mUserRating = jsonObject.optInt(Protocol.RATING);
    }
}