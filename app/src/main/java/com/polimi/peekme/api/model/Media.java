package com.polimi.peekme.api.model;

import com.polimi.peekme.api.Protocol;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andre on 15/10/2016.
 */
public class Media extends ServerObject {

    private long mId;
    private String mFileUrl;
    private String mMimeType;

    public Media(JSONObject jsonObject) throws JSONException {
        super(jsonObject);
        mId = jsonObject.getLong(Protocol.ID);
        mFileUrl = jsonObject.getString(Protocol.FILE_URL);
        mMimeType = jsonObject.getString(Protocol.MIME_TYPE);
    }

    public long getId() {
        return mId;
    }

    public String getFileUrl() {
        return mFileUrl;
    }

    public String getMimeType() {
        return mMimeType;
    }
}