package com.polimi.peekme.api.model;

import java.util.List;

/**
 * Created by andre on 26/10/2016.
 */
public class Metadata {

    private final int mCount;
    private final Long mFirstId;
    private final Long mLastId;

    public Metadata(List<? extends Identifiable> list) {
        mCount = list.size();
        if (mCount > 0) {
            mFirstId = list.get(0).getId();
            mLastId = list.get(mCount - 1).getId();
        } else {
            mFirstId = null;
            mLastId = null;
        }
    }

    public int size() {
        return mCount;
    }

    public boolean hasFirstItem() {
        return mFirstId != null;
    }

    public boolean hasLastItem() {
        return mLastId != null;
    }

    public Long getFirstId() {
        return mFirstId;
    }

    public Long getLastId() {
        return mLastId;
    }
}