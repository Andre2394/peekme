package com.polimi.peekme.api.model;

import com.polimi.peekme.api.ServerException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by andre on 11/10/2016.
 */
public class ModelFactory {

    public static Picture parsePicture(JSONObject jsonObject) {
        try {
            if (jsonObject != null) {
                return new Picture(jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Post parsePost(Response response) throws ServerException {
        ResponseBody responseBody = response.body();
        try {
            String body = responseBody.string();
            JSONObject jsonObject = new JSONObject(body);
            return new Post(jsonObject);
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        }
    }
}