package com.polimi.peekme.api.model;

import android.content.ContentValues;

import com.polimi.peekme.api.Protocol;
import com.polimi.peekme.api.storage.Cacheable;
import com.polimi.peekme.storage.Schema;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andre on 11/10/2016.
 */
public class Picture extends ServerObject implements Cacheable {

    private final long mId;
    private final String mFileUrl;
    private final String mMimeType;

    public Picture(JSONObject jsonObject) throws JSONException {
        super(jsonObject);
        mId = jsonObject.getLong(Protocol.ID);
        mFileUrl = jsonObject.getString(Protocol.FILE_URL);
        mMimeType = jsonObject.getString(Protocol.MIME_TYPE);
    }

    public long getId() {
        return mId;
    }

    public String getFileUrl() {
        return mFileUrl;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Schema.Picture.PICTURE_ID, mId);
        contentValues.put(Schema.Picture.PICTURE_URL, mFileUrl);
        contentValues.put(Schema.Picture.PICTURE_MIME_TYPE, mMimeType);
        return contentValues;
    }
}