package com.polimi.peekme.api.model;

import android.content.ContentValues;

import com.polimi.peekme.api.Protocol;
import com.polimi.peekme.api.storage.Cacheable;
import com.polimi.peekme.storage.Schema;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andre on 15/10/2016.
 */
public class Post extends ServerObject implements Identifiable, Cacheable {

    private long mId;
    private BaseUser mUser;
    private Media mMedia;
    private String mDescription;
    private InternalRating mRating;
    private int mCommentCount;
    private DateTime mCreatedAt;
    private DateTime mUpdatedAt;

    public Post(JSONObject jsonObject) throws JSONException {
        super(jsonObject);
        mId = jsonObject.getLong(Protocol.ID);
        mUser = new BaseUser(jsonObject.getJSONObject(Protocol.USER));
        mMedia = new Media(jsonObject.getJSONObject(Protocol.MEDIA));
        mDescription = jsonObject.optString(Protocol.DESCRIPTION);
        mRating = new InternalRating(jsonObject.getJSONObject(Protocol.RATING));
        mCommentCount = jsonObject.getInt(Protocol.COMMENT_COUNT);
        mCreatedAt = parseTimestamp(jsonObject.getString(Protocol.CREATED_AT));
        mUpdatedAt = parseTimestamp(jsonObject.getString(Protocol.UPDATED_AT));
    }

    private DateTime parseTimestamp(String timestamp) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(Protocol.TIMESTAMP_PATTERN);
        return formatter.parseDateTime(timestamp);
    }

    @Override
    public long getId() {
        return mId;
    }

    public BaseUser getUser() {
        return mUser;
    }

    public Media getMedia() {
        return mMedia;
    }

    public String getDescription() {
        return mDescription;
    }

    public int getRatingCount() {
        return mRating.mCount;
    }

    public float getRating() {
        int count = getRatingCount();
        if (count > 0) {
            return (float) mRating.mTotal / count;
        }
        return 0;
    }

    public int getUserRating() {
        return mRating.mUserRating;
    }

    public int getCommentCount() {
        return mCommentCount;
    }

    public DateTime getCreatedAt() {
        return mCreatedAt;
    }

    public DateTime getUpdatedAt() {
        return mUpdatedAt;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Schema.Post.POST_ID, mId);
        contentValues.put(Schema.Post.POST_USER_ID, mUser.getId());
        contentValues.put(Schema.Post.POST_DESCRIPTION, mDescription);
        contentValues.put(Schema.Post.POST_MEDIA_URL, mMedia.getFileUrl());
        contentValues.put(Schema.Post.POST_MEDIA_MIME_TYPE, mMedia.getMimeType());
        contentValues.put(Schema.Post.POST_RATING_TOTAL, mRating.mTotal);
        contentValues.put(Schema.Post.POST_RATING_COUNT, mRating.mCount);
        contentValues.put(Schema.Post.POST_USER_RATING, mRating.mUserRating);
        contentValues.put(Schema.Post.POST_COMMENT_COUNT, mCommentCount);
        contentValues.put(Schema.Post.POST_CREATED_AT, mCreatedAt.getMillis());
        contentValues.put(Schema.Post.POST_UPDATED_AT, mUpdatedAt.getMillis());
        return contentValues;
    }
}