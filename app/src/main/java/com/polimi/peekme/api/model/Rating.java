package com.polimi.peekme.api.model;

import android.content.ContentValues;

import com.polimi.peekme.api.Protocol;
import com.polimi.peekme.api.storage.Cacheable;
import com.polimi.peekme.storage.Schema;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andre on 23/10/2016.
 */
public class Rating extends ServerObject implements Identifiable, Cacheable {

    private long mId;
    private BaseUser mUser;
    private int mValue;
    private long mPostId;

    public Rating(JSONObject jsonObject) throws JSONException {
        super(jsonObject);
        mId = jsonObject.getLong(Protocol.ID);
        mPostId = jsonObject.getLong(Protocol.POST);
        mUser = new BaseUser(jsonObject.getJSONObject(Protocol.USER));
        mValue = jsonObject.getInt(Protocol.RATING);
    }

    @Override
    public long getId() {
        return mId;
    }

    public long getPostId() {
        return mPostId;
    }

    public BaseUser getUser() {
        return mUser;
    }

    public int getValue() {
        return mValue;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Schema.Rating.RATING_ID, mId);
        contentValues.put(Schema.Rating.RATING_USER_ID, mUser.getId());
        contentValues.put(Schema.Rating.RATING_POST_ID, mPostId);
        contentValues.put(Schema.Rating.RATING_VALUE, mValue);
        return contentValues;
    }
}