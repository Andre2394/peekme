package com.polimi.peekme.api.model;

import android.content.ContentValues;

import com.polimi.peekme.api.Protocol;
import com.polimi.peekme.storage.Schema;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andre on 27/10/2016.
 */
public class User extends BaseUser {

    private final String mEmail;
    private final int mFollowersCount;
    private final int mFollowedCount;

    public User(JSONObject jsonObject) throws JSONException {
        super(jsonObject);
        mEmail = jsonObject.optString(Protocol.EMAIL, null);
        mFollowersCount = jsonObject.optInt(Protocol.FOLLOWER_COUNT, 0);
        mFollowedCount = jsonObject.optInt(Protocol.FOLLOWED_COUNT, 0);
    }

    public String getEmail() {
        return mEmail;
    }

    public int getFollowerCount() {
        return mFollowersCount;
    }

    public int getFollowedCount() {
        return mFollowedCount;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = super.getContentValues();
        contentValues.put(Schema.User.USER_EMAIL, mEmail);
        contentValues.put(Schema.User.USER_FOLLOWER_COUNT, mFollowersCount);
        contentValues.put(Schema.User.USER_FOLLOWED_COUNT, mFollowedCount);
        return contentValues;
    }
}