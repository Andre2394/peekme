package com.polimi.peekme.api.network;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.webkit.MimeTypeMap;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;

/**
 * Created by andre on 24/12/2016.
 */
public class UriRequestBody extends RequestBody {

    private final MediaType mMediaType;
    private final InputStream mInputStream;

    public UriRequestBody(ContentResolver contentResolver, Uri uri) throws IOException {
        String type = getMimeType(contentResolver, uri);
        mMediaType = MediaType.parse(type);
        mInputStream = contentResolver.openInputStream(uri);
        if (mInputStream == null) {
            throw new NullPointerException("file stream == null");
        }
    }

    private String getMimeType(ContentResolver contentResolver, Uri uri) {
        String mimeType = contentResolver.getType(uri);
        if (mimeType == null) {
            // do some trick to get the mime type from file extension.
            String extension = MimeTypeMap.getFileExtensionFromUrl(uri.getPath());
            if (extension != null) {
                return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
        }
        return mimeType;
    }

    @Override
    public MediaType contentType() {
        return mMediaType;
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        Source source = null;
        try {
            source = Okio.source(mInputStream);
            sink.writeAll(source);
        } finally {
            Util.closeQuietly(source);
        }
    }
}