package com.polimi.peekme.api.oauth2;

/**
 * Created by andre on 26/09/2016.
 */
public interface OAuth2AccessToken {

    String getAccessToken();

    String getTokenType();

    boolean hasRefreshToken();

    String getRefreshToken();
}