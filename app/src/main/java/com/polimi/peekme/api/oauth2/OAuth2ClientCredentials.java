package com.polimi.peekme.api.oauth2;

/**
 * Created by andre on 26/09/2016.
 */
public class OAuth2ClientCredentials {

    private final String mClientId;
    private final String mClientSecret;

    public OAuth2ClientCredentials(String clientId, String clientSecret) {
        mClientId = clientId;
        mClientSecret = clientSecret;
    }

    public String getClientId() {
        return mClientId;
    }

    public String getClientSecret() {
        return mClientSecret;
    }
}