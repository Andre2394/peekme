package com.polimi.peekme.api.oauth2;

import com.polimi.peekme.api.ServerException;

/**
 * Created by andre on 26/09/2016.
 */
public class OAuth2Exception extends ServerException {

    private int mResultCode;

    public OAuth2Exception(int resultCode, String error) {
        super(error, null);
        mResultCode = resultCode;
    }

    public OAuth2Exception(int resultCode, String error, String errorDescription) {
        super(error, errorDescription);
        mResultCode = resultCode;
    }

    public int getResultCode() {
        return mResultCode;
    }
}