package com.polimi.peekme.api.oauth2;

import android.text.TextUtils;

import com.polimi.peekme.api.ResponseCode;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by andre on 26/09/2016.
 */
public class OAuth2Interceptor implements Interceptor {

    private static final String AUTHORIZATION_HEADER = "Authorization";

    private static final Object REFRESH_MUTEX = new Object();

    private final OAuth2Manager mOAuth2Manager;

    public OAuth2Interceptor(OAuth2Manager oAuth2Manager) {
        mOAuth2Manager = oAuth2Manager;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder builder = request.newBuilder();
        OAuth2AccessToken accessToken = mOAuth2Manager.getAccessToken();
        appendAccessToken(builder, accessToken);
        request = builder.build();
        Response response = chain.proceed(request);
        if (response.code() == ResponseCode.UNAUTHORIZED) {
            synchronized (REFRESH_MUTEX) {
                if (mOAuth2Manager.isCurrentAccessToken(accessToken)) {
                    try {
                        if (accessToken != null) {
                            accessToken = mOAuth2Manager.refreshAccessToken(accessToken);
                        }
                    } catch (OAuth2Exception e) {
                        if (e.getResultCode() == ResponseCode.UNAUTHORIZED) {
                            mOAuth2Manager.logoutUser();
                            return response;
                        }
                    }
                }
                appendAccessToken(builder, accessToken);
                request = builder.build();
                return chain.proceed(request);
            }
        }
        return response;
    }

    private void appendAccessToken(Request.Builder builder, OAuth2AccessToken accessToken) {
        if (accessToken != null) {
            String token = accessToken.getAccessToken();
            if (!TextUtils.isEmpty(token)) {
                String header = String.format("%s %s", accessToken.getTokenType(), accessToken.getAccessToken());
                builder.addHeader(AUTHORIZATION_HEADER, header);
            }
        }
    }
}