package com.polimi.peekme.api.oauth2;

import com.polimi.peekme.api.ServerException;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andre on 26/09/2016.
 */
public class OAuth2JsonToken implements OAuth2AccessToken {

    private String mTokenType;
    private String mAccessToken;
    private String mRefreshToken;


    public OAuth2JsonToken(String jsonBody) throws ServerException {
        try {
            JSONObject jsonToken = new JSONObject(jsonBody);
            mTokenType = jsonToken.getString("token_type");
            mAccessToken = jsonToken.getString("access_token");
            mRefreshToken = jsonToken.optString("refresh_token");
        } catch (JSONException e) {
            throw new ServerException(e);
        }
    }

    @Override
    public String getAccessToken() {
        return mAccessToken;
    }

    @Override
    public String getTokenType() {
        return mTokenType;
    }

    @Override
    public boolean hasRefreshToken() {
        return mRefreshToken != null;
    }

    @Override
    public String getRefreshToken() {
        return mRefreshToken;
    }
}