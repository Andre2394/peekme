package com.polimi.peekme.api.oauth2;

/**
 * Created by andre on 26/09/2016.
 */
public interface OAuth2Manager {

    OAuth2AccessToken getAccessToken();

    boolean isCurrentAccessToken(OAuth2AccessToken accessToken);

    OAuth2AccessToken refreshAccessToken(OAuth2AccessToken accessToken) throws OAuth2Exception;

    void storeAccessToken(OAuth2AccessToken accessToken);

    void logoutUser();
}