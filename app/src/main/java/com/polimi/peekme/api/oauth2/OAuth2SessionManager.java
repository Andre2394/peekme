package com.polimi.peekme.api.oauth2;

import com.polimi.peekme.api.IServer;
import com.polimi.peekme.api.ResponseCode;
import com.polimi.peekme.api.ServerException;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by andre on 26/09/2016.
 */
public class OAuth2SessionManager implements OAuth2Manager {

    private final IServer mServer;
    private final OAuth2TokenStorage mTokenStorage;

    public OAuth2SessionManager(IServer server, OAuth2TokenStorage tokenStorage) {
        mServer = server;
        mTokenStorage = tokenStorage;
    }

    @Override
    public OAuth2AccessToken getAccessToken() {
        return mTokenStorage.getAccessToken();
    }

    @Override
    public boolean isCurrentAccessToken(OAuth2AccessToken accessToken) {
        OAuth2AccessToken currentToken = getAccessToken();
        if (currentToken == null) {
            return accessToken == null;
        } else {
            return accessToken != null && currentToken.getAccessToken().equals(accessToken.getAccessToken());
        }
    }

    @Override
    public OAuth2AccessToken refreshAccessToken(OAuth2AccessToken accessToken) throws OAuth2Exception {
        if (accessToken.hasRefreshToken()) {
            OAuth2ClientCredentials clientCredentials = mServer.getClientCredentials();
            Request request = new Request.Builder()
                    .url(mServer.getTokenEndpoint())
                    .post(new FormBody.Builder()
                            .add(OAuth2Constants.GRANT_TYPE, OAuth2Constants.GRANT_TYPE_REFRESH_TOKEN)
                            .add(OAuth2Constants.CLIENT_ID, clientCredentials.getClientId())
                            .add(OAuth2Constants.CLIENT_SECRET, clientCredentials.getClientSecret())
                            .add(OAuth2Constants.REFRESH_TOKEN, accessToken.getRefreshToken())
                            .build()
                    )
                    .build();
            try {
                Response response = mServer.execute(request);
                if (response.isSuccessful()) {
                    OAuth2AccessToken refreshedToken = new OAuth2JsonToken(response.body().string());
                    storeAccessToken(refreshedToken);
                    return refreshedToken;
                }
            } catch (IOException | ServerException e) {
                e.printStackTrace();
            }
        }
        throw new OAuth2Exception(ResponseCode.UNAUTHORIZED, "invalid_token", "no refresh token found");
    }

    @Override
    public void storeAccessToken(OAuth2AccessToken accessToken) {
        mTokenStorage.storeAccessToken(accessToken);
    }

    @Override
    public void logoutUser() {
        mTokenStorage.clearAccessToken();
    }
}