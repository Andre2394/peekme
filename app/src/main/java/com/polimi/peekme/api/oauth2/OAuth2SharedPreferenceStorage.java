package com.polimi.peekme.api.oauth2;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by andre on 26/09/2016.
 */
public class OAuth2SharedPreferenceStorage implements OAuth2TokenStorage {

    private static final String FILE_NAME = "account.xml";

    private SharedPreferences mSharedPreferences;

    public OAuth2SharedPreferenceStorage(Context context) {
        mSharedPreferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public OAuth2AccessToken getAccessToken() {
        if (mSharedPreferences.getAll().size() > 0) {
            return new OAuth2SharedPreferencesToken(mSharedPreferences);
        }
        return null;
    }

    @Override
    public void storeAccessToken(OAuth2AccessToken accessToken) {
        OAuth2SharedPreferencesToken.store(mSharedPreferences, accessToken);
    }

    @Override
    public void clearAccessToken() {
        mSharedPreferences.edit().clear().apply();
    }
}