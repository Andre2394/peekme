package com.polimi.peekme.api.oauth2;

import android.content.SharedPreferences;

/**
 * Created by andre on 26/09/2016.
 */
public class OAuth2SharedPreferencesToken implements OAuth2AccessToken {

    private static final String TOKEN_TYPE = "token_type";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String REFRESH_TOKEN = "refresh_token";

    private SharedPreferences mPreferences;

    public OAuth2SharedPreferencesToken(SharedPreferences preferences) {
        mPreferences = preferences;
    }

    @Override
    public String getAccessToken() {
        return mPreferences.getString(ACCESS_TOKEN, null);
    }

    @Override
    public String getTokenType() {
        return mPreferences.getString(TOKEN_TYPE, null);
    }

    @Override
    public boolean hasRefreshToken() {
        return mPreferences.contains(REFRESH_TOKEN);
    }

    @Override
    public String getRefreshToken() {
        return mPreferences.getString(REFRESH_TOKEN, null);
    }

    public static void store(SharedPreferences preferences, OAuth2AccessToken accessToken) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TOKEN_TYPE, accessToken.getTokenType());
        editor.putString(ACCESS_TOKEN, accessToken.getAccessToken());
        editor.putString(REFRESH_TOKEN, accessToken.getRefreshToken());
        editor.apply();
    }
}