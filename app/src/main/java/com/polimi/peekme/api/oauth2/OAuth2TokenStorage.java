package com.polimi.peekme.api.oauth2;

/**
 * Created by andre on 26/09/2016.
 */
public interface OAuth2TokenStorage {

    OAuth2AccessToken getAccessToken();

    void storeAccessToken(OAuth2AccessToken accessToken);

    void clearAccessToken();
}