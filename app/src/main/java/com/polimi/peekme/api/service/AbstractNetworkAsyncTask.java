package com.polimi.peekme.api.service;

import android.os.AsyncTask;

import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;

/**
 * Created by andre on 27/09/2016.
 */
public abstract class AbstractNetworkAsyncTask<P, T> extends AsyncTask<Void, Void, T> {

    private final P mParameters;
    private final ServerCallback<T> mCallback;
    private ServerException mServerException;

    public AbstractNetworkAsyncTask(P parameters, ServerCallback<T> callback) {
        mParameters = parameters;
        mCallback = callback;
    }

    @Override
    protected T doInBackground(Void... voids) {
        try {
            return doNetworkInBackground(mParameters);
        } catch (ServerException e) {
            mServerException = e;
        }
        return null;
    }

    protected abstract T doNetworkInBackground(P parameters) throws ServerException;

    @Override
    public void onPostExecute(T result) {
        if (mCallback != null) {
            mCallback.onRequestDone(result, mServerException);
        }
    }
}