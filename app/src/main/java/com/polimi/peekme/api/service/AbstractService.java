package com.polimi.peekme.api.service;

import com.polimi.peekme.api.IServer;

/**
 * Created by andre on 26/09/2016.
 */
public abstract class AbstractService {

    protected final IServer mServer;
    private final String mPath;

    public AbstractService(IServer server, String path) {
        mServer = server;
        mPath = path;
    }

    protected String getServiceUrl() {
        return mServer.getBaseUrl() + "/" + mPath;
    }
}