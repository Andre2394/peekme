package com.polimi.peekme.api.service;

import com.polimi.peekme.api.IServer;
import com.polimi.peekme.api.Protocol;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.Metadata;
import com.polimi.peekme.api.model.Post;
import com.polimi.peekme.api.storage.ICache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by andre on 27/12/2016.
 */
public class ExploreService extends AbstractService {

    public ExploreService(IServer server) {
        super(server, "explore");
    }

    private Metadata downloadLatestPosts(Long lastId) throws ServerException {
        // step 0: create query url
        HttpUrl url = HttpUrl.parse(getServiceUrl() + "/posts");
        if (lastId != null) {
            url = url.newBuilder()
                    .addQueryParameter(Protocol.AFTER, String.valueOf(lastId))
                    .build();
        }
        // step 1: download everything
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Response response = null;
        Metadata metadata;
        List<Post> postList;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                JSONArray jsonArray = jsonObject.getJSONArray(Protocol.DATA);
                postList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    postList.add(new Post(jsonArray.getJSONObject(i)));
                }
                metadata = new Metadata(postList);
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
        // step 2: clear cache if this is the first loading
        ICache cache = mServer.getCache();
        if (lastId == null) {
            cache.clearExplorePostsCache();
        }
        // step 3: update cache
        cache.storeExplorePosts(postList);
        // step 4: return metadata
        return metadata;
    }

    public void downloadLatestPostsAsync(ServerCallback<Metadata> callback) {
        new AbstractNetworkAsyncTask<Void, Metadata>(null, callback) {

            @Override
            protected Metadata doNetworkInBackground(Void parameters) throws ServerException {
                return downloadLatestPosts(null);
            }

        }.execute();
    }

    public void downloadLatestPostsAsync(long lastId, ServerCallback<Metadata> callback) {
        new AbstractNetworkAsyncTask<Long, Metadata>(lastId, callback) {

            @Override
            protected Metadata doNetworkInBackground(Long lastId) throws ServerException {
                return downloadLatestPosts(lastId);
            }

        }.execute();
    }
}