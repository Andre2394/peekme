package com.polimi.peekme.api.service;

import android.net.Uri;

import com.polimi.peekme.api.IServer;
import com.polimi.peekme.api.Protocol;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.Comment;
import com.polimi.peekme.api.model.Media;
import com.polimi.peekme.api.model.Metadata;
import com.polimi.peekme.api.model.Post;
import com.polimi.peekme.api.model.Rating;
import com.polimi.peekme.api.network.UriRequestBody;
import com.polimi.peekme.api.storage.ICache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by andre on 18/10/2016.
 */
public class PostService extends AbstractService {

    public PostService(IServer server) {
        super(server, "posts");
    }

    public Post uploadPost(Uri uri, String description) throws ServerException {
        Media media = uploadMedia(uri);
        Request request = new Request.Builder()
                .post(new FormBody.Builder()
                        .add(Protocol.MEDIA_ID, String.valueOf(media.getId()))
                        .add(Protocol.DESCRIPTION, description)
                        .build()
                )
                .url(getServiceUrl())
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                Post post = new Post(jsonObject);
                ICache cache = mServer.getCache();
                cache.storeCurrentUserPost(post);
                return post;
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    private Media uploadMedia(Uri uri) throws ServerException {
        Response response = null;
        try {
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(Protocol.MEDIA_FILE, "picture", new UriRequestBody(mServer.getContentResolver(), uri))
                    .build();
            Request request = new Request.Builder()
                    .post(requestBody)
                    .url(mServer.getMediaEndpoint())
                    .build();
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                return new Media(jsonObject);
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    public Post getPost(long postId) throws ServerException {
        Request request = new Request.Builder()
                .get()
                .url(getServiceUrl() + "/" + String.valueOf(postId))
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                ICache cache = mServer.getCache();
                JSONObject jsonObject = new JSONObject(responseBody);
                Post post = new Post(jsonObject);
                cache.storeStandardPost(post);
                return post;
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    public void getPostAsync(long postId, ServerCallback<Post> callback) {
        new AbstractNetworkAsyncTask<Long, Post>(postId, callback) {

            @Override
            protected Post doNetworkInBackground(Long postId) throws ServerException {
                return getPost(postId);
            }

        }.execute();
    }

    public Post ratePost(long postId, int value) throws ServerException {
        Request request = new Request.Builder()
                .post(new FormBody.Builder()
                        .add(Protocol.RATING, String.valueOf(value))
                        .build()
                )
                .url(getServiceUrl() + "/" + String.valueOf(postId) + "/rate")
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonResponse = new JSONObject(responseBody);
                ICache cache = mServer.getCache();
                Post post = new Post(jsonResponse);
                cache.storeStandardPost(post);
                return post;
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    public void ratePostAsync(long postId, int value, ServerCallback<Post> callback) {
        Object[] objects = new Object[] {postId, value};
        new AbstractNetworkAsyncTask<Object[], Post>(objects, callback) {

            @Override
            protected Post doNetworkInBackground(Object[] parameters) throws ServerException {
                long postId = (long) parameters[0];
                int value = (int) parameters[1];
                return ratePost(postId, value);
            }

        }.execute();
    }

    public Post unRatePost(long postId) throws ServerException {
        Request request = new Request.Builder()
                .delete()
                .url(getServiceUrl() + "/" + String.valueOf(postId) + "/rate")
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonResponse = new JSONObject(responseBody);
                ICache cache = mServer.getCache();
                Post post = new Post(jsonResponse);
                cache.storeStandardPost(post);
                return post;
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    public void unRatePostAsync(long postId, ServerCallback<Post> callback) {
        new AbstractNetworkAsyncTask<Long, Post>(postId, callback) {

            @Override
            protected Post doNetworkInBackground(Long postId) throws ServerException {
                return unRatePost(postId);
            }

        }.execute();
    }

    /*
    public Rating getRating(long ratingId) throws ServerException {
        Request request = new Request.Builder()
                .get()
                .url(getServiceUrl() + "/rating/" + String.valueOf(ratingId))
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                Rating rating = new Rating(jsonObject);
                mServer.getCache().storePostRating(rating);
                return rating;
            } else {
                throw new ServerException(response.body().string());
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }*/

    public Comment addComment(long postId, String text) throws ServerException {
        Request request = new Request.Builder()
                .post(new FormBody.Builder()
                        .add(Protocol.COMMENT_TEXT, text)
                        .build()
                )
                .url(getServiceUrl() + "/" + String.valueOf(postId) + "/comment")
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                Comment comment = new Comment(jsonObject);
                ICache cache = mServer.getCache();
                cache.storePostComment(comment);
                return comment;
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    public void addCommentAsync(long postId, String comment, ServerCallback<Comment> callback) {
        Object[] objects = new Object[] {postId, comment};
        new AbstractNetworkAsyncTask<Object[], Comment>(objects, callback) {

            @Override
            protected Comment doNetworkInBackground(Object[] parameters) throws ServerException {
                long postId = (long) parameters[0];
                String comment = (String) parameters[1];
                return addComment(postId, comment);
            }

        }.execute();
    }

    public Comment getComment(long commentId) throws ServerException {
        Request request = new Request.Builder()
                .get()
                .url(getServiceUrl() + "/comment/" + String.valueOf(commentId))
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                Comment comment = new Comment(jsonObject);
                mServer.getCache().storePostComment(comment);
                return comment;
            } else {
                throw new ServerException(response.body().string());
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    public void deleteCommentAsync(long postId, long commentId, ServerCallback<Void> callback) {
        Long[] parameters = new Long[] {postId, commentId};
        new AbstractNetworkAsyncTask<Long[], Void>(parameters, callback) {

            @Override
            protected Void doNetworkInBackground(Long[] parameters) throws ServerException {
                long postId = parameters[0];
                long commentId = parameters[1];
                deleteComment(postId, commentId);
                return null;
            }

        }.execute();
    }

    public void deleteComment(long postId, long commentId) throws ServerException {
        Request request = new Request.Builder()
                .delete()
                .url(getServiceUrl() + "/comment/" + String.valueOf(commentId))
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            if (response.isSuccessful()) {
                mServer.getCache().deletePostComment(postId, commentId);
            } else {
                throw new ServerException(response.body().string());
            }
        } catch (IOException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    /**
     * Download latest posts to show in the home page of the app.
     * @param lastId id of the latest posts, null if page is empty.
     * @return the metadata which contains all info about pagination.
     * @throws ServerException if something went wrong.
     */
    private Metadata downloadLatestPosts(Long lastId) throws ServerException {
        // step 0: create query url
        HttpUrl url = HttpUrl.parse(getServiceUrl());
        if (lastId != null) {
            url = url.newBuilder()
                    .addQueryParameter(Protocol.AFTER, String.valueOf(lastId))
                    .build();
        }
        // step 1: download everything
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Response response = null;
        Metadata metadata;
        List<Post> postList;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                JSONArray jsonArray = jsonObject.getJSONArray(Protocol.DATA);
                postList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    postList.add(new Post(jsonArray.getJSONObject(i)));
                }
                metadata = new Metadata(postList);
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
        // step 2: clear cache if this is the first loading
        ICache cache = mServer.getCache();
        if (lastId == null) {
            cache.clearHomePostsCache();
        }
        // step 3: update cache
        cache.storeHomePosts(postList);
        // step 4: return metadata
        return metadata;
    }

    /**
     * Download latest posts from the server. Every post retrieved here is to mark as 'show_in_home'
     * in the database cache because it means that it belongs to a followed user or to the current user.
     * @param callback where you wont to get the result of the operation.
     */
    public void downloadLatestPostsAsync(ServerCallback<Metadata> callback) {
        new AbstractNetworkAsyncTask<Void, Metadata>(null, callback) {

            @Override
            protected Metadata doNetworkInBackground(Void parameters) throws ServerException {
                return downloadLatestPosts(null);
            }

        }.execute();
    }

    /**
     * Download latest posts from the server. Every post retrieved here is to mark as 'show_in_home'
     * in the database cache because it means that it belongs to a followed user or to the current user.
     * @param lastId id of the latest posts, null if page is empty.
     * @param callback where you wont to get the result of the operation.
     */
    public void downloadLatestPostsAsync(long lastId, ServerCallback<Metadata> callback) {
        new AbstractNetworkAsyncTask<Long, Metadata>(lastId, callback) {

            @Override
            protected Metadata doNetworkInBackground(Long lastId) throws ServerException {
                return downloadLatestPosts(lastId);
            }

        }.execute();
    }

    /**
     * Download latest comments of the post from the server. This will cache them in the cache and
     * clear the comments stored previously.
     * @param postId that identify the post where the comments are related to.
     * @param callback where you wont to get the result of the operation.
     */
    public void downloadLatestPostCommentsAsync(long postId, ServerCallback<Metadata> callback) {
        new AbstractNetworkAsyncTask<Long, Metadata>(postId, callback) {

            @Override
            protected Metadata doNetworkInBackground(Long postId) throws ServerException {
                // step 1: download everything
                Request request = new Request.Builder()
                        .url(getServiceUrl() + "/" + String.valueOf(postId) + "/comment")
                        .get()
                        .build();
                Response response = null;
                Metadata metadata;
                List<Comment> commentList;
                try {
                    response = mServer.execute(request);
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        JSONObject jsonObject = new JSONObject(responseBody);
                        JSONArray jsonArray = jsonObject.getJSONArray(Protocol.DATA);
                        commentList = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            commentList.add(new Comment(jsonArray.getJSONObject(i)));
                        }
                        metadata = new Metadata(commentList);
                    } else {
                        throw new ServerException(responseBody);
                    }
                } catch (IOException | JSONException e) {
                    throw new ServerException(e);
                } finally {
                    if (response != null) {
                        response.close();
                    }
                }
                // step 2: clear comment cache for post id
                ICache cache = mServer.getCache();
                cache.clearPostCommentsCache(postId);
                // step 3: update cache
                cache.storePostComments(commentList);
                // step 4: return metadata
                return metadata;
            }

        }.execute();
    }

    /*
    public void downloadLatestPostRatingsAsync(long postId, ServerCallback<Metadata> callback) {
        new AbstractNetworkAsyncTask<Long, Metadata>(postId, callback) {

            @Override
            protected Metadata doNetworkInBackground(Long postId) throws ServerException {
                // step 1: download everything
                Request request = new Request.Builder()
                        .url(getServiceUrl() + "/" + String.valueOf(postId) + "/rate")
                        .get()
                        .build();
                Response response = null;
                Metadata metadata;
                List<Rating> ratingList;
                try {
                    response = mServer.execute(request);
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        JSONObject jsonObject = new JSONObject(responseBody);
                        JSONArray jsonArray = jsonObject.getJSONArray(Protocol.DATA);
                        ratingList = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            ratingList.add(new Rating(jsonArray.getJSONObject(i)));
                        }
                        metadata = new Metadata(ratingList);
                    } else {
                        throw new ServerException(responseBody);
                    }
                } catch (IOException | JSONException e) {
                    throw new ServerException(e);
                } finally {
                    if (response != null) {
                        response.close();
                    }
                }
                // step 2: clear rating cache for post id
                ICache cache = mServer.getCache();
                cache.clearPostRatingsCache(postId);
                // step 3: update cache
                cache.storePostRatings(ratingList);
                // step 4: return metadata
                return metadata;
            }

        }.execute();
    }*/

    private Metadata downloadTagPostList(String tag, Long lastId) throws ServerException {
        // step 0: create query url
        HttpUrl url = HttpUrl.parse(getServiceUrl() + "/tag/" + tag);
        if (lastId != null) {
            url = url.newBuilder()
                    .addQueryParameter(Protocol.AFTER, String.valueOf(lastId))
                    .build();
        }
        // step 1: download everything
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Response response = null;
        Metadata metadata;
        List<Post> postList;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                JSONArray jsonArray = jsonObject.getJSONArray(Protocol.DATA);
                postList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    postList.add(new Post(jsonArray.getJSONObject(i)));
                }
                metadata = new Metadata(postList);
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
        // step 2: update cache
        ICache cache = mServer.getCache();
        if (lastId == null) {
            cache.clearTagPostsCache(tag);
        }
        cache.storeTagPosts(tag, postList);
        // step 3: return metadata
        return metadata;
    }

    /**
     * Download latest posts from the server.
     * @param callback where you wont to get the result of the operation.
     */
    public void downloadTagPostsAsync(String tag, ServerCallback<Metadata> callback) {
        new AbstractNetworkAsyncTask<String, Metadata>(tag, callback) {

            @Override
            protected Metadata doNetworkInBackground(String tag) throws ServerException {
                return downloadTagPostList(tag, null);
            }

        }.execute();
    }

    /**
     * Download latest posts from the server.
     * @param lastId id of the latest post.
     * @param callback where you wont to get the result of the operation.
     */
    public void downloadTagPostsAsync(String tag, long lastId, ServerCallback<Metadata> callback) {
        Object[] parameters = new Object[] {tag, lastId};
        new AbstractNetworkAsyncTask<Object[], Metadata>(parameters, callback) {

            @Override
            protected Metadata doNetworkInBackground(Object[] parameters) throws ServerException {
                String tag = (String) parameters[0];
                long afterId = (long) parameters[1];
                return downloadTagPostList(tag, afterId);
            }

        }.execute();
    }
}