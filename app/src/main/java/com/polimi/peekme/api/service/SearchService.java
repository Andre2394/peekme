package com.polimi.peekme.api.service;

import com.polimi.peekme.api.IServer;
import com.polimi.peekme.api.Protocol;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.BaseUser;
import com.polimi.peekme.api.model.Metadata;
import com.polimi.peekme.api.storage.ICache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by andre on 21/10/2016.
 */
public class SearchService extends AbstractService {

    public SearchService(IServer server) {
        super(server, "search");
    }

    public void findUsersAsync(String keyword, ServerCallback<Metadata> callback) {
        new AbstractNetworkAsyncTask<String, Metadata>(keyword, callback) {

            @Override
            protected Metadata doNetworkInBackground(String keyword) throws ServerException {
                // step 1: download everything
                HttpUrl httpUrl = HttpUrl.parse(getServiceUrl())
                        .newBuilder()
                        .addPathSegment("users")
                        .addQueryParameter(Protocol.KEYWORD, keyword)
                        .build();
                Request request = new Request.Builder()
                        .url(httpUrl)
                        .get()
                        .build();
                Response response = null;
                Metadata metadata;
                List<BaseUser> userList;
                try {
                    response = mServer.execute(request);
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        JSONObject jsonObject = new JSONObject(responseBody);
                        JSONArray jsonArray = jsonObject.getJSONArray(Protocol.DATA);
                        userList = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            userList.add(new BaseUser(jsonArray.getJSONObject(i)));
                        }
                        metadata = new Metadata(userList);
                    } else {
                        throw new ServerException(responseBody);
                    }
                } catch (IOException | JSONException e) {
                    throw new ServerException(e);
                } finally {
                    if (response != null) {
                        response.close();
                    }
                }
                // step 2: clear follower cache for user id
                ICache cache = mServer.getCache();
                // step 3: update cache
                cache.storeUserList(userList);
                // step 4: return metadata
                return metadata;
            }

        }.execute();
    }
}