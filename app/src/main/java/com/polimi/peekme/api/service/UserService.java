package com.polimi.peekme.api.service;

import android.content.ContentResolver;
import android.net.Uri;
import android.text.TextUtils;

import com.google.firebase.iid.FirebaseInstanceId;
import com.polimi.peekme.api.IServer;
import com.polimi.peekme.api.Protocol;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.BaseUser;
import com.polimi.peekme.api.model.Metadata;
import com.polimi.peekme.api.model.Picture;
import com.polimi.peekme.api.model.Post;
import com.polimi.peekme.api.model.User;
import com.polimi.peekme.api.network.UriRequestBody;
import com.polimi.peekme.api.oauth2.OAuth2ClientCredentials;
import com.polimi.peekme.api.oauth2.OAuth2Constants;
import com.polimi.peekme.api.oauth2.OAuth2JsonToken;
import com.polimi.peekme.api.storage.ICache;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Internal service that handle the interactions with the RESTFul server to manage users.
 */
public class UserService extends AbstractService {

    private static final String CURRENT_USER_IDENTIFIER = "me";

    public UserService(IServer server) {
        super(server, "users");
    }

    /**
     * Blocking method to login user with his credentials.
     * @param username of the user.
     * @param password of the user.
     * @throws ServerException if something went wrong.
     */
    public void login(String username, String password) throws ServerException {
        OAuth2ClientCredentials clientCredentials = mServer.getClientCredentials();
        Request request = new Request.Builder()
                .post(new FormBody.Builder()
                        .add(OAuth2Constants.GRANT_TYPE, OAuth2Constants.GRANT_TYPE_PASSWORD)
                        .add(OAuth2Constants.CLIENT_ID, clientCredentials.getClientId())
                        .add(OAuth2Constants.CLIENT_SECRET, clientCredentials.getClientSecret())
                        .add(OAuth2Constants.USERNAME, username)
                        .add(OAuth2Constants.PASSWORD, password)
                        .build()
                )
                .url(mServer.getTokenEndpoint())
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                mServer.getOAuthManager().storeAccessToken(new OAuth2JsonToken(responseBody));
                BaseUser user = getCurrentUserProfile();
                mServer.getCache().setCurrentUserId(user.getId());
                // register firebase device token
                FirebaseInstanceId firebaseInstanceId = FirebaseInstanceId.getInstance();
                String token = firebaseInstanceId.getToken();
                if (!TextUtils.isEmpty(token)) {
                    registerFirebaseDeviceToken(token);
                }
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    /**
     * Asynchronous method to login a user with his credentials.
     * @param username of the user.
     * @param password of the user.
     * @param callback to handle the response.
     */
    public void loginAsync(String username, String password, ServerCallback<Void> callback) {
        String[] parameters = new String[] {username, password};
        new AbstractNetworkAsyncTask<String[], Void>(parameters, callback) {

            @Override
            protected Void doNetworkInBackground(String[] parameters) throws ServerException {
                login(parameters[0], parameters[1]);
                return null;
            }

        }.execute();
    }

    /**
     * Blocking method to register a new user.
     * @param username of the user.
     * @param email of the user.
     * @param password of the user.
     * @throws ServerException if something went wrong.
     */
    public void register(String username, String email, String password) throws ServerException {
        OAuth2ClientCredentials clientCredentials = mServer.getClientCredentials();
        Request request = new Request.Builder()
                .post(new FormBody.Builder()
                        .add(OAuth2Constants.CLIENT_ID, clientCredentials.getClientId())
                        .add(OAuth2Constants.USERNAME, username)
                        .add(Protocol.EMAIL, email)
                        .add(OAuth2Constants.PASSWORD, password)
                        .build()
                )
                .url(getServiceUrl())
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            if (!response.isSuccessful()) {
                throw new ServerException(response.body().string());
            }
        } catch (IOException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    /**
     * Asynchronous method to register a new user.
     * @param username of the user.
     * @param email of the user.
     * @param password of the user.
     * @param callback to handle the response.
     */
    public void registerAsync(String username, String email, String password, ServerCallback<Void> callback) {
        String[] parameters = new String[] {username, email, password};
        new AbstractNetworkAsyncTask<String[], Void>(parameters, callback) {

            @Override
            protected Void doNetworkInBackground(String[] parameters) throws ServerException {
                register(parameters[0], parameters[1], parameters[2]);
                return null;
            }

        }.execute();
    }

    /**
     * Register device token on server in order to receive push notification.
     * @param token of the device.
     * @throws ServerException if something went wrong.
     */
    public void registerFirebaseDeviceToken(String token) throws ServerException {
        Request request = new Request.Builder()
                .post(new FormBody.Builder()
                        .add(Protocol.GCM_TOKEN, token)
                        .build()
                )
                .url(getServiceUrl() + "/token")
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            if (!response.isSuccessful()) {
                throw new ServerException(response.body().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    /**
     * Register asynchronously in device token on server in order to receive push notification.
     * @param token of the device.
     * @param callback where you want to be notified of the result of the operation.
     */
    public void registerFirebaseDeviceTokenAsync(String token, ServerCallback<Void> callback) {
        new AbstractNetworkAsyncTask<String, Void>(token, callback) {

            @Override
            protected Void doNetworkInBackground(String token) throws ServerException {
                registerFirebaseDeviceToken(token);
                return null;
            }

        }.execute();
    }

    /**
     * Retrieve from cache the current user id.
     * @return the current user id.
     */
    public long getCurrentUserId() {
        return mServer.getCache().getCurrentUserId();
    }

    /**
     * Query current user from server.
     * @return the current user that is making the request.
     * @throws ServerException if something went wrong.
     */
    public User getCurrentUserProfile() throws ServerException {
        return getUserProfile(CURRENT_USER_IDENTIFIER);
    }

    /**
     * Query user from server.
     * @param identifier can be a long id or the keyword 'me'
     * @return the user from the server.
     * @throws ServerException if something went wrong or not a valid identifier.
     */
    public User getUserProfile(String identifier) throws ServerException {
        Request request = new Request.Builder()
                .get()
                .url(getServiceUrl() + "/" + identifier)
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                User user = new User(jsonObject);
                mServer.getCache().storeUser(user);
                return user;
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    /**
     * Query user from server asynchronous.
     * @param userId id of the user to query.
     * @param callback where you wont to be notified of the result of the operation.
     */
    public void getUserProfileAsync(long userId, ServerCallback<User> callback) {
        getUserProfileAsync(String.valueOf(userId), callback);
    }

    /**
     * Query user from server asynchronous.
     * @param identifier can be a long id or the keyword 'me'
     * @param callback where you wont to be notified of the result of the operation.
     */
    public void getUserProfileAsync(String identifier, ServerCallback<User> callback) {
        new AbstractNetworkAsyncTask<String, User>(identifier, callback) {

            @Override
            protected User doNetworkInBackground(String identifier) throws ServerException {
                return getUserProfile(identifier);
            }

        }.execute();
    }

    /**
     * Upload an user picture to the server.
     * @param uri of the file to upload.
     * @return the picture instance of the server.
     * @throws ServerException if something went wrong.
     */
    public Picture uploadUserPicture(Uri uri) throws ServerException {
        Response response = null;
        try {
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(Protocol.MEDIA_FILE, "picture", new UriRequestBody(mServer.getContentResolver(), uri))
                    .build();
            Request request = new Request.Builder()
                    .post(requestBody)
                    .url(getServiceUrl() + "/picture")
                    .build();
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                return new Picture(jsonObject);
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    /**
     * Upload an user picture to the server.
     * @param uri of the file to upload.
     * @param callback where you wont to be notified of the result of the operation.
     */
    public void uploadUserPictureAsync(Uri uri, ServerCallback<Picture> callback) {
        new AbstractNetworkAsyncTask<Uri, Picture>(uri, callback) {

            @Override
            protected Picture doNetworkInBackground(Uri uri) throws ServerException {
                return uploadUserPicture(uri);
            }

        }.execute();
    }

    /**
     * Update current user picture with a provided picture id.
     * @param pictureId to set as current picture.
     * @return the current user updated.
     * @throws ServerException if something went wrong.
     */
    public User updateUserPicture(long pictureId) throws ServerException {
        Response response = null;
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Protocol.PICTURE_ID, pictureId);
            Request request = new Request.Builder()
                    .put(RequestBody.create(MediaType.parse("application/json"), jsonObject.toString()))
                    .url(getServiceUrl())
                    .build();
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonBody = new JSONObject(responseBody);
                User user = new User(jsonBody);
                ICache cache = mServer.getCache();
                cache.storeUser(user);
                return user;
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    /**
     * Update current user picture with a provided picture id.
     * @param pictureId to set as current picture.
     * @param callback where you wont to be notified of the result of the operation.
     */
    public void updateUserPictureAsync(long pictureId, ServerCallback<User> callback) {
        new AbstractNetworkAsyncTask<Long, User>(pictureId, callback) {

            @Override
            protected User doNetworkInBackground(Long pictureId) throws ServerException {
                return updateUserPicture(pictureId);
            }

        }.execute();
    }

    /**
     * Check if user is currently logged in.
     * @return true if logged, false if not.
     */
    public boolean isUserLoggedIn() {
        return mServer.getOAuthManager().getAccessToken() != null;
    }

    public User followUser(long userId) throws ServerException {
        Request request = new Request.Builder()
                .url(getServiceUrl() + "/" + String.valueOf(userId) + "/follow")
                .post(RequestBody.create(MediaType.parse("text/plain"), ""))
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                User user = new User(jsonObject);
                ICache cache = mServer.getCache();
                long currentUser = cache.getCurrentUserId();
                cache.storeFollowedUser(currentUser, user);
                return user;
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    public void followUserAsync(long userId, ServerCallback<User> callback) {
        new AbstractNetworkAsyncTask<Long, User>(userId, callback) {

            @Override
            protected User doNetworkInBackground(Long userId) throws ServerException {
                return followUser(userId);
            }

        }.execute();
    }

    public User unfollowUser(long userId) throws ServerException {
        Request request = new Request.Builder()
                .url(getServiceUrl() + "/" + String.valueOf(userId) + "/follow")
                .delete()
                .build();
        Response response = null;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                User user = new User(jsonObject);
                ICache cache = mServer.getCache();
                long currentUser = cache.getCurrentUserId();
                cache.clearFollowedUser(currentUser, user);
                return user;
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    public void unfollowUserAsync(long userId, ServerCallback<User> callback) {
        new AbstractNetworkAsyncTask<Long, User>(userId, callback) {

            @Override
            protected User doNetworkInBackground(Long userId) throws ServerException {
                return unfollowUser(userId);
            }

        }.execute();
    }

    private Metadata downloadLatestUserPosts(long userId, Long lastId) throws ServerException {
        // step 0: create query url
        HttpUrl url = HttpUrl.parse(getServiceUrl() + "/" + String.valueOf(userId) + "/posts");
        if (lastId != null) {
            url = url.newBuilder()
                    .addQueryParameter(Protocol.AFTER, String.valueOf(lastId))
                    .build();
        }
        // step 1: download everything
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Response response = null;
        Metadata metadata;
        List<Post> postList;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                JSONArray jsonArray = jsonObject.getJSONArray(Protocol.DATA);
                postList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    postList.add(new Post(jsonArray.getJSONObject(i)));
                }
                metadata = new Metadata(postList);
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
        // step 2: clear user cache if this is the first loading
        ICache cache = mServer.getCache();
        if (lastId == null) {
            cache.clearUserPostsCache(userId);
        }
        // step 3: update cache
        cache.storeUserPosts(postList);
        // step 4: return metadata
        return metadata;
    }

    public void downloadLatestUserPostsAsync(long userId, ServerCallback<Metadata> callback) {
        new AbstractNetworkAsyncTask<Long, Metadata>(userId, callback) {

            @Override
            protected Metadata doNetworkInBackground(Long userId) throws ServerException {
                return downloadLatestUserPosts(userId, null);
            }

        }.execute();
    }

    public void downloadLatestUserPostsAsync(long userId, long afterId, ServerCallback<Metadata> callback) {
        Long[] parameters = new Long[] {userId, afterId};
        new AbstractNetworkAsyncTask<Long[], Metadata>(parameters, callback) {

            @Override
            protected Metadata doNetworkInBackground(Long[] parameters) throws ServerException {
                return downloadLatestUserPosts(parameters[0], parameters[1]);
            }

        }.execute();
    }

    private Metadata downloadUserFollowerList(long userId, Long afterId) throws ServerException {
        // step 0: build uri
        HttpUrl url = HttpUrl.parse(getServiceUrl() + "/" + String.valueOf(userId) + "/follower");
        if (afterId != null) {
            url = url.newBuilder()
                    .addQueryParameter(Protocol.AFTER, String.valueOf(afterId))
                    .build();
        }
        // step 1: download everything
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Response response = null;
        Metadata metadata;
        List<BaseUser> userList;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                JSONArray jsonArray = jsonObject.getJSONArray(Protocol.DATA);
                userList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    userList.add(new BaseUser(jsonArray.getJSONObject(i)));
                }
                metadata = new Metadata(userList);
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
        // step 2: clear follower cache for user id only if this is the first request
        ICache cache = mServer.getCache();
        if (afterId == null) {
            cache.clearUserFollowerListCache(userId);
        }
        // step 3: update cache
        cache.storeUserFollowerList(userId, userList);
        // step 4: return metadata
        return metadata;
    }

    public void downloadUserFollowerListAsync(long userId, ServerCallback<Metadata> callback) {
        new AbstractNetworkAsyncTask<Long, Metadata>(userId, callback) {

            @Override
            protected Metadata doNetworkInBackground(Long userId) throws ServerException {
                return downloadUserFollowerList(userId, null);
            }

        }.execute();
    }

    public void downloadUserFollowerListAsync(long userId, long afterId, ServerCallback<Metadata> callback) {
        Long[] parameters = new Long[] {userId, afterId};
        new AbstractNetworkAsyncTask<Long[], Metadata>(parameters, callback) {

            @Override
            protected Metadata doNetworkInBackground(Long[] parameters) throws ServerException {
                return downloadUserFollowerList(parameters[0], parameters[1]);
            }

        }.execute();
    }

    private Metadata downloadUserFollowedList(long userId, Long afterId) throws ServerException {
        // step 0: build uri
        HttpUrl url = HttpUrl.parse(getServiceUrl() + "/" + String.valueOf(userId) + "/followed");
        if (afterId != null) {
            url = url.newBuilder()
                    .addQueryParameter(Protocol.AFTER, String.valueOf(afterId))
                    .build();
        }
        // step 1: download everything
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Response response = null;
        Metadata metadata;
        List<BaseUser> userList;
        try {
            response = mServer.execute(request);
            String responseBody = response.body().string();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(responseBody);
                JSONArray jsonArray = jsonObject.getJSONArray(Protocol.DATA);
                userList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    userList.add(new BaseUser(jsonArray.getJSONObject(i)));
                }
                metadata = new Metadata(userList);
            } else {
                throw new ServerException(responseBody);
            }
        } catch (IOException | JSONException e) {
            throw new ServerException(e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
        // step 2: clear follower cache for user id only if this is the first request
        ICache cache = mServer.getCache();
        if (afterId == null) {
            cache.clearUserFollowedListCache(userId);
        }
        // step 3: update cache
        cache.storeUserFollowedList(userId, userList);
        // step 4: return metadata
        return metadata;
    }

    public void downloadUserFollowedListAsync(long userId, ServerCallback<Metadata> callback) {
        new AbstractNetworkAsyncTask<Long, Metadata>(userId, callback) {

            @Override
            protected Metadata doNetworkInBackground(Long userId) throws ServerException {
                return downloadUserFollowedList(userId, null);
            }

        }.execute();
    }

    public void downloadUserFollowedListAsync(long userId, long afterId, ServerCallback<Metadata> callback) {
        Long[] parameters = new Long[] {userId, afterId};
        new AbstractNetworkAsyncTask<Long[], Metadata>(parameters, callback) {

            @Override
            protected Metadata doNetworkInBackground(Long[] parameters) throws ServerException {
                return downloadUserFollowedList(parameters[0], parameters[1]);
            }

        }.execute();
    }
}