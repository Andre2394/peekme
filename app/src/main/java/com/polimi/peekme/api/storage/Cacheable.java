package com.polimi.peekme.api.storage;

import android.content.ContentValues;

/**
 * This interface identify an object that can be stored into the cache.
 */
public interface Cacheable {

    ContentValues getContentValues();
}