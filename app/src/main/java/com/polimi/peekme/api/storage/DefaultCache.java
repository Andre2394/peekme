package com.polimi.peekme.api.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.polimi.peekme.api.model.BaseUser;
import com.polimi.peekme.api.model.Comment;
import com.polimi.peekme.api.model.Post;
import com.polimi.peekme.api.model.Rating;

import java.util.List;

/**
 * Default implementation of a cache. This will never store anything about the server data on the
 * device. The only thing that will be cached is the current user id.
 */
public class DefaultCache implements ICache {

    private static final String CACHE_FILE = "cache.xml";
    private static final String CURRENT_USER = "current_user";

    private SharedPreferences mCache;

    public DefaultCache(Context context) {
        mCache = context.getSharedPreferences(CACHE_FILE, Context.MODE_PRIVATE);
    }

    @Override
    public void setCurrentUserId(long userId) {
        mCache.edit().putLong(CURRENT_USER, userId).apply();
    }

    @Override
    public long getCurrentUserId() {
        return mCache.getLong(CURRENT_USER, 0);
    }

    @Override
    public void storeUser(@NonNull BaseUser user) {

    }

    @Override
    public void storeUserList(List<BaseUser> users) {

    }

    @Override
    public void clearHomePostsCache() {

    }

    @Override
    public void clearExplorePostsCache() {

    }

    @Override
    public void clearTagPostsCache(String tag) {

    }

    @Override
    public void clearUserPostsCache(long userId) {

    }

    @Override
    public void storeCurrentUserPost(Post post) {

    }

    @Override
    public void storeStandardPost(Post post) {

    }

    @Override
    public void storeUserPosts(List<Post> posts) {

    }

    @Override
    public void storeHomePosts(List<Post> posts) {

    }

    @Override
    public void storeExplorePosts(List<Post> posts) {

    }

    @Override
    public void storeTagPosts(String tag, List<Post> posts) {

    }

    @Override
    public void clearPostCommentsCache(long postId) {

    }

    @Override
    public void storePostComment(Comment comment) {

    }

    @Override
    public void storePostComments(List<Comment> comments) {

    }

    @Override
    public void deletePostComment(long postId, long commentId) {

    }

    @Override
    public void clearUserFollowerListCache(long userId) {

    }

    @Override
    public void storeUserFollowerList(long userId, List<BaseUser> users) {

    }

    @Override
    public void clearUserFollowedListCache(long userId) {

    }

    @Override
    public void storeUserFollowedList(long userId, List<BaseUser> users) {

    }

    @Override
    public void storeFollowedUser(long userId, BaseUser user) {

    }

    @Override
    public void clearFollowedUser(long userId, BaseUser user) {

    }
}