package com.polimi.peekme.api.storage;

import android.support.annotation.NonNull;

import com.polimi.peekme.api.model.BaseUser;
import com.polimi.peekme.api.model.Comment;
import com.polimi.peekme.api.model.Post;
import com.polimi.peekme.api.model.Rating;

import java.util.List;

/**
 * This interface is used to hide internal operations to cache network data on the device.
 */
public interface ICache {

    /**
     * Constants that identify not a valid user id.
     */
    long NO_USER = 0;

    /**
     * Store the id of the current user.
     * @param userId of the current user.
     */
    void setCurrentUserId(long userId);

    /**
     * Get the id of the current user.
     * @return the id of the current user if found, {@link #NO_USER} if no user is currently logged in.
     */
    long getCurrentUserId();

    /**
     * Store a user into the cache.
     * @param user item to store in the cache.
     */
    void storeUser(@NonNull BaseUser user);

    /**
     * Store a list of users into the cache.
     * @param users list of users to store.
     */
    void storeUserList(List<BaseUser> users);

    /**
     * Remove a reference to all posts that should be displayed in the home section.
     */
    void clearHomePostsCache();

    /**
     * Remove a reference to all posts that should be displayed in the explore section.
     */
    void clearExplorePostsCache();

    /**
     * Remove all posts related to the provided tag.
     * @param tag that should be removed from cache.
     */
    void clearTagPostsCache(String tag);

    /**
     * Remove all posts that are linked to the provided user id.
     * @param userId id of the owner of the posts to remove.
     */
    void clearUserPostsCache(long userId);

    /**
     * Store a single post of the current user. This post will be shown in home section too.
     * @param post item to store.
     */
    void storeCurrentUserPost(Post post);

    /**
     * Store a single post.
     * @param post item to store.
     */
    void storeStandardPost(Post post);

    /**
     * Store all posts of the same user.
     * @param posts list of user's posts.
     */
    void storeUserPosts(List<Post> posts);

    /**
     * Store all posts to show in home section.
     * @param posts list of posts to store.
     */
    void storeHomePosts(List<Post> posts);

    /**
     * Store all posts to show in explore section.
     * @param posts list of posts to store.
     */
    void storeExplorePosts(List<Post> posts);

    /**
     * Store all posts to show in the provided tag section.
     * @param tag of the post.
     * @param posts list of posts to store.
     */
    void storeTagPosts(String tag, List<Post> posts);

    /**
     * Remove all comments related to the provided post id.
     * @param postId id of the post from which the comments should be removed.
     */
    void clearPostCommentsCache(long postId);

    /**
     * Store a post comment.
     * @param comment item to store.
     */
    void storePostComment(Comment comment);

    /**
     * Store a list of post comments.
     * @param comments list of comments to store.
     */
    void storePostComments(List<Comment> comments);

    /**
     * Remove the comment from the cache.
     * @param postId is of the post related to the comment.
     * @param commentId id of the comment to remove.
     */
    void deletePostComment(long postId, long commentId);

    /*
    void clearPostRatingsCache(long postId);

    void storePostRating(Rating rating);

    void storePostRatings(List<Rating> ratings);*/

    /**
     * Remove all references to the followers of the provided user.
     * @param userId id of the user.
     */
    void clearUserFollowerListCache(long userId);

    /**
     * Store all references to the followers of the provided user.
     * @param userId id of the user.
     * @param users list of users that are following the provided user.
     */
    void storeUserFollowerList(long userId, List<BaseUser> users);

    /**
     * Remove all references to followed users by the provided user.
     * @param userId id of the user.
     */
    void clearUserFollowedListCache(long userId);

    /**
     * Store all references to followed users by the provided user.
     * @param userId id of the user.
     * @param users list of users that are followed by the provided user.
     */
    void storeUserFollowedList(long userId, List<BaseUser> users);

    /**
     * Store a new followed user.
     * @param userId id of the user that is following the provided user.
     * @param user item that is followed by the userId
     */
    void storeFollowedUser(long userId, BaseUser user);

    /**
     * Remove the followed user.
     * @param userId id of the user that is following the provided user.
     * @param user item that is followed by the userId
     */
    void clearFollowedUser(long userId, BaseUser user);
}