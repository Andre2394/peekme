package com.polimi.peekme.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;

import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.polimi.peekme.R;
import com.polimi.peekme.api.PeekMeServer;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.BaseUser;
import com.polimi.peekme.api.model.Comment;
import com.polimi.peekme.api.model.Picture;
import com.polimi.peekme.api.model.Post;
import com.polimi.peekme.api.model.Rating;
import com.polimi.peekme.api.model.User;
import com.polimi.peekme.api.service.PostService;
import com.polimi.peekme.api.service.UserService;
import com.polimi.peekme.storage.Thumbnail;
import com.polimi.peekme.ui.activity.ShowPostActivity;
import com.polimi.peekme.ui.activity.ShowUserActivity;

import java.util.Map;
import java.util.concurrent.ExecutionException;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by andre on 25/10/2016.
 */
public class NotificationHandler extends FirebaseMessagingService {

    private static final String ACTION = "action";
    private static final String FOLLOWER_ID = "follower_id";
    private static final String RATING_ID = "rating_id";
    private static final String COMMENT_ID = "comment_id";
    private static final String POST_ID = "post_id";
    public static final int NEW_FOLLOWER = 1;
    public static final int NEW_RATING = 2;
    public static final int NEW_COMMENT = 3;
    public static final int TAG_POST = 4;
    public static final int TAG_COMMENT = 5;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        int action = Integer.parseInt(data.get(ACTION));
        switch (action) {
            case NEW_FOLLOWER:
                System.out.println("Notification new follower");
                onNewFollower(data.get(FOLLOWER_ID));
                break;
            case NEW_RATING:
                System.out.println("Notification new rating");
                // onNewRating(Long.parseLong(data.get(RATING_ID)));
                break;
            case NEW_COMMENT:
                System.out.println("Notification new comment");
                onNewComment(Long.parseLong(data.get(COMMENT_ID)));
                break;
            case TAG_POST:
                String postId = data.get(POST_ID);
                System.out.println("Notification tag post id: " + postId);
                onPostTag(Long.parseLong(postId));
                break;
            case TAG_COMMENT:
                String commentId = data.get(COMMENT_ID);
                System.out.println("Notification tag comment id: " + commentId);
                onCommentTag(Long.parseLong(commentId));
                break;
        }
    }

    private void onNewFollower(String followerId) {
        try {
            UserService userService = PeekMeServer.getUserService();
            User user = userService.getUserProfile(followerId);
            String message = getString(R.string.notification_new_follower, user.getUsername());
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(message)
                            .setAutoCancel(true);
            if (user.hasPicture()) {
                Picture picture = user.getPicture();
                try {
                    Bitmap bitmap = Glide.with(getApplicationContext())
                            .load(picture.getFileUrl())
                            .asBitmap()
                            .transform(new CropCircleTransformation(getApplicationContext()))
                            .into(250, 250)
                            .get();
                    builder.setLargeIcon(bitmap);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            } else {
                // TODO set default user icon
            }
            Intent intent = new Intent(this, ShowUserActivity.class);
            intent.putExtra(ShowUserActivity.USER, user.getId());
            int requestId = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(this, requestId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(1, builder.build());
        } catch (ServerException e) {
            e.printStackTrace();
        }
    }

    /*
    private void onNewRating(long ratingId) {
        try {
            PostService postService = PeekMeServer.getPostService();
            Rating rating = postService.getRating(ratingId);
            BaseUser user = rating.getUser();
            String message = getString(R.string.notification_new_rating, user.getUsername());
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setAutoCancel(true);
            if (user.hasPicture()) {
                Picture picture = user.getPicture();
                try {
                    Bitmap bitmap = Glide.with(getApplicationContext())
                            .load(picture.getFileUrl())
                            .asBitmap()
                            .transform(new CropCircleTransformation(getApplicationContext()))
                            .into(250, 250)
                            .get();
                    builder.setLargeIcon(bitmap);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            } else {
                // TODO set default user icon
            }
            Intent intent = new Intent(this, ShowPostActivity.class);
            intent.putExtra(ShowPostActivity.POST, rating.getPostId());
            int requestId = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(this, requestId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(1, builder.build());
        } catch (ServerException e) {
            e.printStackTrace();
        }
    }*/

    private void onNewComment(long commentId) {
        try {
            PostService postService = PeekMeServer.getPostService();
            Comment comment = postService.getComment(commentId);
            BaseUser user = comment.getUser();
            String message = getString(R.string.notification_new_comment, user.getUsername());
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setAutoCancel(true);
            if (user.hasPicture()) {
                Picture picture = user.getPicture();
                try {
                    Bitmap bitmap = Glide.with(getApplicationContext())
                            .load(picture.getFileUrl())
                            .asBitmap()
                            .transform(new CropCircleTransformation(getApplicationContext()))
                            .into(250, 250)
                            .get();
                    builder.setLargeIcon(bitmap);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            } else {
                // TODO set default user icon
            }
            Intent intent = new Intent(this, ShowPostActivity.class);
            intent.putExtra(ShowPostActivity.POST, comment.getPostId());
            int requestId = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(this, requestId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(1, builder.build());
        } catch (ServerException e) {
            e.printStackTrace();
        }
    }

    private void onPostTag(long postId) {
        try {
            PostService postService = PeekMeServer.getPostService();
            Post post = postService.getPost(postId);
            BaseUser user = post.getUser();
            String message = getString(R.string.notification_tag_post, user.getUsername());
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setAutoCancel(true);
            if (user.hasPicture()) {
                Picture picture = user.getPicture();
                try {
                    Bitmap bitmap = Glide.with(getApplicationContext())
                            .load(picture.getFileUrl())
                            .asBitmap()
                            .transform(new CropCircleTransformation(getApplicationContext()))
                            .into(250, 250)
                            .get();
                    builder.setLargeIcon(bitmap);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            } else {
                // TODO set default user icon
            }
            Intent intent = new Intent(this, ShowPostActivity.class);
            intent.putExtra(ShowPostActivity.POST, postId);
            int requestId = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(this, requestId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(1, builder.build());
        } catch (ServerException e) {
            e.printStackTrace();
        }
    }

    private void onCommentTag(long commentId) {
        try {
            PostService postService = PeekMeServer.getPostService();
            Comment comment = postService.getComment(commentId);
            BaseUser user = comment.getUser();
            String message = getString(R.string.notification_tag_comment, user.getUsername());
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setAutoCancel(true);
            if (user.hasPicture()) {
                Picture picture = user.getPicture();
                try {
                    Bitmap bitmap = Glide.with(getApplicationContext())
                            .load(picture.getFileUrl())
                            .asBitmap()
                            .transform(new CropCircleTransformation(getApplicationContext()))
                            .into(250, 250)
                            .get();
                    builder.setLargeIcon(bitmap);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            } else {
                // TODO set default user icon
            }
            Intent intent = new Intent(this, ShowPostActivity.class);
            intent.putExtra(ShowPostActivity.POST, comment.getPostId());
            int requestId = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(this, requestId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(1, builder.build());
        } catch (ServerException e) {
            e.printStackTrace();
        }
    }
}