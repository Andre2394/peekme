package com.polimi.peekme.service;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;

import com.polimi.peekme.api.PeekMeServer;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.Post;
import com.polimi.peekme.api.service.PostService;
import com.polimi.peekme.storage.CacheDataProvider;

/**
 * Created by andre on 18/10/2016.
 */
public class UploadPostService extends IntentService {

    public static final String POST_MEDIA = "media";
    public static final String POST_DESCRIPTION = "description";

    public UploadPostService() {
        super("UploadPostService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Uri uri = intent.getParcelableExtra(POST_MEDIA);
        String description = intent.getStringExtra(POST_DESCRIPTION);
        PostService postService = PeekMeServer.getPostService();
        try {
            Post post = postService.uploadPost(uri, description);
            ContentResolver contentResolver = getContentResolver();
            contentResolver.notifyChange(CacheDataProvider.POST_URI, null);
            contentResolver.notifyChange(Uri.withAppendedPath(CacheDataProvider.USER_URI, String.valueOf(post.getUser().getId()) + "/posts"), null);
        } catch (ServerException e) {
            // TODO maybe show a notification or send an intent to the system.
            e.printStackTrace();
        }
    }
}