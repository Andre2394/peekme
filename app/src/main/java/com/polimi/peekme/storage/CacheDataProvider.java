package com.polimi.peekme.storage;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * This class is an implementation of android content provider that manages the data inside the
 * database. It is responsible to retrieve and insert data when something from the server is coming
 * and to clean the table whenever is required.
 */
public class CacheDataProvider extends ContentProvider {

    public static final String PROVIDER_NAME = "com.polimi.peekme.database";

    private static final UriMatcher mUriMatcher = createUriMatcher();

    public static final Uri USER_URI = Uri.parse("content://" + PROVIDER_NAME + "/users");
    public static final Uri PICTURE_URI = Uri.parse("content://" + PROVIDER_NAME + "/pictures");
    public static final Uri POST_URI = Uri.parse("content://" + PROVIDER_NAME + "/posts");

    public static final Uri TAG_POST_URI = Uri.parse("content://" + PROVIDER_NAME + "/posts/tags");
    public static final Uri HOME_POST_URI = Uri.parse("content://" + PROVIDER_NAME + "/home/posts");
    public static final Uri EXPLORE_POST_URI = Uri.parse("content://" + PROVIDER_NAME + "/explore/posts");
    public static final Uri SEARCH_USER_URI = Uri.parse("content://" + PROVIDER_NAME + "/search/user");

    public static final String KEYWORD_PARAMETER = "keyword";

    private static final int USER_LIST = 1;
    private static final int USER_ID = 2;
    private static final int PICTURE_LIST = 3;
    private static final int POST_LIST = 4;
    private static final int POST_ID = 5;
    private static final int POST_COMMENT_LIST = 6;
    private static final int POST_COMMENT_ID = 7;
    // private static final int POST_RATING_LIST = 8;
    private static final int USER_FOLLOWER_LIST = 9;
    private static final int USER_FOLLOWED_LIST = 10;
    private static final int USER_POST_LIST = 11;
    private static final int TAG_LIST = 12;
    private static final int TAG_POST_LIST = 13;
    private static final int HOME_POST_LIST = 14;
    private static final int EXPLORE_POST_LIST = 15;
    private static final int USER_FOLLOWER_ID = 16;
    private static final int USER_FOLLOWED_ID = 17;
    private static final int SEARCH_USER_LIST = 20;

    private static UriMatcher createUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(PROVIDER_NAME, "users", USER_LIST);
        matcher.addURI(PROVIDER_NAME, "users/#", USER_ID);
        matcher.addURI(PROVIDER_NAME, "pictures", PICTURE_LIST);
        matcher.addURI(PROVIDER_NAME, "posts", POST_LIST);
        matcher.addURI(PROVIDER_NAME, "posts/#", POST_ID);
        matcher.addURI(PROVIDER_NAME, "posts/tags", TAG_LIST);
        matcher.addURI(PROVIDER_NAME, "posts/tags/*", TAG_POST_LIST);
        matcher.addURI(PROVIDER_NAME, "posts/#/comments", POST_COMMENT_LIST);
        matcher.addURI(PROVIDER_NAME, "posts/#/comments/#", POST_COMMENT_ID);
        // matcher.addURI(PROVIDER_NAME, "posts/#/ratings", POST_RATING_LIST);
        matcher.addURI(PROVIDER_NAME, "users/#/follower", USER_FOLLOWER_LIST);
        matcher.addURI(PROVIDER_NAME, "users/#/followed", USER_FOLLOWED_LIST);
        matcher.addURI(PROVIDER_NAME, "users/#/follower/#", USER_FOLLOWER_ID);
        matcher.addURI(PROVIDER_NAME, "users/#/followed/#", USER_FOLLOWED_ID);
        matcher.addURI(PROVIDER_NAME, "users/#/posts", USER_POST_LIST);
        matcher.addURI(PROVIDER_NAME, "home/posts", HOME_POST_LIST);
        matcher.addURI(PROVIDER_NAME, "explore/posts", EXPLORE_POST_LIST);
        matcher.addURI(PROVIDER_NAME, "search/user", SEARCH_USER_LIST);
        return matcher;
    }

    private Database mDatabase;

    @Override
    public boolean onCreate() {
        mDatabase = new Database(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Context context = getContext();
        if (context == null) {
            return null;
        }
        Cursor cursor = null;
        switch (mUriMatcher.match(uri)) {
            case USER_ID:
                cursor = mDatabase.getUsers(projection, Schema.User.USER_ID + " = ?", new String[] {uri.getLastPathSegment()}, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                break;
            case POST_ID:
                cursor = mDatabase.getPosts(projection, Schema.Post.POST_ID + " = ?", new String[] {uri.getLastPathSegment()}, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                break;
            case TAG_POST_LIST:
                cursor = mDatabase.getTagPosts(projection, Schema.Tag.TAG_NAME + " = ?", new String[] {uri.getLastPathSegment()}, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                break;
            case POST_COMMENT_LIST:
                cursor = mDatabase.getPostComments(projection, Schema.Comment.COMMENT_POST_ID + " = ?", new String[] {uri.getPathSegments().get(1)}, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                break;
            /*
            case POST_RATING_LIST:
                cursor = mDatabase.getPostRatings(getPostId(uri), projection, selection, selectionArgs, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                break;*/
            case USER_FOLLOWER_LIST:
                cursor = mDatabase.getUserFollowerList(projection, Schema.Follower.FOLLOWER_TO_USER_ID + " = ?", new String[] {uri.getPathSegments().get(1)}, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                break;
            case USER_FOLLOWED_LIST:
                cursor = mDatabase.getUserFollowedList(projection, Schema.Follower.FOLLOWER_FROM_USER_ID + " = ?", new String[] {uri.getPathSegments().get(1)}, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                break;
            case USER_POST_LIST: // used
                cursor = mDatabase.getPosts(projection, Schema.Post.POST_USER_ID + " = ?", new String[] {uri.getPathSegments().get(1)}, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), USER_URI);
                break;
            case HOME_POST_LIST: // used
                cursor = new MultiUriCursorWrapper(mDatabase.getHomePosts(projection, selection, selectionArgs, sortOrder))
                        .withNotificationUri(context.getContentResolver(), HOME_POST_URI)
                        .withNotificationUri(context.getContentResolver(), POST_URI);
                break;
            case EXPLORE_POST_LIST: // used
                cursor = new MultiUriCursorWrapper(mDatabase.getExplorePosts(projection, selection, selectionArgs, sortOrder))
                        .withNotificationUri(context.getContentResolver(), EXPLORE_POST_URI)
                        .withNotificationUri(context.getContentResolver(), POST_URI);
                break;
            case SEARCH_USER_LIST:
                String keyword = uri.getQueryParameter(KEYWORD_PARAMETER);
                cursor = mDatabase.getUsers(projection, Schema.User.USER_USERNAME + " LIKE '" + keyword + "%'", selectionArgs, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                break;
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (mUriMatcher.match(uri)) {
            case USER_LIST:
                return "vnd.android.cursor.dir/vnd." + PROVIDER_NAME + ".users";
            case USER_ID:
                return "vnd.android.cursor.item/vnd." + PROVIDER_NAME + ".users";
            case PICTURE_LIST:
                return "vnd.android.cursor.dir/vnd." + PROVIDER_NAME + ".pictures";
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues contentValues) {
        switch (mUriMatcher.match(uri)) {
            case USER_LIST: // used
                return mDatabase.insertUser(contentValues);
            case PICTURE_LIST: // used
                return mDatabase.insertPicture(contentValues);
            case POST_COMMENT_LIST: // used
                return mDatabase.insertPostComment(contentValues);
            /*
            case POST_RATING_LIST:
                return mDatabase.insertPostRating(contentValues);*/
            case USER_FOLLOWER_LIST: // used
                contentValues.put(Schema.Follower.FOLLOWER_TO_USER_ID, uri.getPathSegments().get(1));
                return mDatabase.insertUserFollower(contentValues);
            case USER_FOLLOWED_LIST: // used
                contentValues.put(Schema.Follower.FOLLOWER_FROM_USER_ID, uri.getPathSegments().get(1));
                return mDatabase.insertUserFollowed(contentValues);
            case POST_LIST: // used
                return mDatabase.insertPost(contentValues);
            case HOME_POST_LIST: // used
                return mDatabase.insertHomePost(contentValues);
            case EXPLORE_POST_LIST: // used
                return mDatabase.insertExplorePost(contentValues);
            case TAG_LIST: // used
                return mDatabase.insertTagPost(contentValues);
        }
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        switch (mUriMatcher.match(uri)) {
            case POST_COMMENT_ID: // used
                return mDatabase.deleteComments(Schema.Comment.COMMENT_POST_ID + " = ? AND " +
                        Schema.Comment.COMMENT_ID + " = ?", new String[] {uri.getPathSegments().get(1),
                        uri.getLastPathSegment()});
            /*
            case POST_RATING_LIST:
                return mDatabase.deletePostRatings(getPostId(uri), selection, selectionArgs);*/
            case USER_FOLLOWER_LIST: // used
                return mDatabase.deleteUserFollowerList(selection, selectionArgs);
            case USER_FOLLOWED_LIST: // used
                return mDatabase.deleteUserFollowedList(selection, selectionArgs);
            case USER_FOLLOWER_ID: // used
                return mDatabase.deleteUserFollowerList(Schema.Follower.FOLLOWER_TO_USER_ID +
                        " = ? AND " + Schema.Follower.FOLLOWER_FROM_USER_ID + " = ?",
                        new String[] {uri.getPathSegments().get(1), uri.getLastPathSegment()});
            case USER_FOLLOWED_ID: // used
                return mDatabase.deleteUserFollowedList(Schema.Follower.FOLLOWER_FROM_USER_ID +
                                " = ? AND " + Schema.Follower.FOLLOWER_TO_USER_ID + " = ?",
                        new String[] {uri.getPathSegments().get(1), uri.getLastPathSegment()});
            case POST_LIST: // used
                return mDatabase.deletePosts(selection, selectionArgs);
            case HOME_POST_LIST: // used
                return mDatabase.deleteHomePosts(selection, selectionArgs);
            case EXPLORE_POST_LIST: // used
                return mDatabase.deleteExplorePosts(selection, selectionArgs);
            case TAG_LIST: // used
                return mDatabase.deleteTagPosts(selection, selectionArgs);
            case POST_COMMENT_LIST: // used
                return mDatabase.deleteComments(Schema.Comment.COMMENT_POST_ID + " = ?",
                        new String[] {uri.getPathSegments().get(1)});
        }
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        switch (mUriMatcher.match(uri)) {
            case USER_LIST: // used
                return mDatabase.updateUser(values, selection, selectionArgs);
        }
        return 0;
    }
}