package com.polimi.peekme.storage;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

/**
 * This class manage the caching jobs of the data from the server. The cache is cleaned every time
 * the user (or a background process) requires to update data of a table.
 */
public class Database extends SQLiteOpenHelper {

    private static final String NAME = "database.db";
    private static final int VERSION = 1;

    public Database(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void createTables(SQLiteDatabase db) {
        new TableBuilder(db)
                .withColumn(Schema.Picture.PICTURE_ID, TableBuilder.Type.INTEGER_PRIMARY_KEY_AUTOINCREMENT)
                .withColumn(Schema.Picture.PICTURE_URL, TableBuilder.Type.TEXT_NOT_NULL)
                .withColumn(Schema.Picture.PICTURE_MIME_TYPE, TableBuilder.Type.TEXT_NOT_NULL)
                .createTable(Schema.Picture.TABLE_NAME);
        new TableBuilder(db)
                .withColumn(Schema.User.USER_ID, TableBuilder.Type.INTEGER_PRIMARY_KEY_AUTOINCREMENT)
                .withColumn(Schema.User.USER_USERNAME, TableBuilder.Type.TEXT_NOT_NULL)
                .withColumn(Schema.User.USER_PICTURE, TableBuilder.Type.INTEGER)
                .withColumn(Schema.User.USER_NAME, TableBuilder.Type.TEXT)
                .withColumn(Schema.User.USER_SURNAME, TableBuilder.Type.TEXT)
                .withColumn(Schema.User.USER_EMAIL, TableBuilder.Type.TEXT)
                .withColumn(Schema.User.USER_SEX, TableBuilder.Type.TEXT)
                .withColumn(Schema.User.USER_BIRTHDAY, TableBuilder.Type.TEXT)
                .withColumn(Schema.User.USER_FOLLOWED_COUNT, TableBuilder.Type.INTEGER)
                .withColumn(Schema.User.USER_FOLLOWER_COUNT, TableBuilder.Type.INTEGER)
                .withColumn(Schema.User.USER_FOLLOWED_BY_YOU, TableBuilder.Type.BOOLEAN_NOT_NULL)
                .withForeignKey(Schema.User.USER_PICTURE, Schema.Picture.TABLE_NAME, Schema.Picture.PICTURE_ID)
                .createTable(Schema.User.TABLE_NAME);
        new TableBuilder(db)
                .withColumn(Schema.Post.POST_ID, TableBuilder.Type.INTEGER_PRIMARY_KEY_AUTOINCREMENT)
                .withColumn(Schema.Post.POST_USER_ID, TableBuilder.Type.INTEGER_NOT_NULL)
                .withColumn(Schema.Post.POST_DESCRIPTION, TableBuilder.Type.TEXT)
                .withColumn(Schema.Post.POST_MEDIA_URL, TableBuilder.Type.TEXT_NOT_NULL)
                .withColumn(Schema.Post.POST_MEDIA_MIME_TYPE, TableBuilder.Type.TEXT_NOT_NULL)
                .withColumn(Schema.Post.POST_RATING_TOTAL, TableBuilder.Type.INTEGER_NOT_NULL)
                .withColumn(Schema.Post.POST_RATING_COUNT, TableBuilder.Type.INTEGER_NOT_NULL)
                .withColumn(Schema.Post.POST_USER_RATING, TableBuilder.Type.INTEGER)
                .withColumn(Schema.Post.POST_COMMENT_COUNT, TableBuilder.Type.INTEGER_NOT_NULL)
                .withColumn(Schema.Post.POST_CREATED_AT, TableBuilder.Type.INTEGER_NOT_NULL)
                .withColumn(Schema.Post.POST_UPDATED_AT, TableBuilder.Type.INTEGER_NOT_NULL)
                .createTable(Schema.Post.TABLE_NAME);
        /*
        new TableBuilder(db)
                .withColumn(Schema.Rating.RATING_ID, TableBuilder.Type.INTEGER_PRIMARY_KEY_AUTOINCREMENT)
                .withColumn(Schema.Rating.RATING_USER_ID, TableBuilder.Type.INTEGER_NOT_NULL)
                .withColumn(Schema.Rating.RATING_POST_ID, TableBuilder.Type.INTEGER_NOT_NULL)
                .withColumn(Schema.Rating.RATING_VALUE, TableBuilder.Type.INTEGER_NOT_NULL)
                .withForeignKey(Schema.Rating.RATING_USER_ID, Schema.User.TABLE_NAME, Schema.User.USER_ID)
                .createTable(Schema.Rating.TABLE_NAME);*/
        new TableBuilder(db)
                .withColumn(Schema.Follower.FOLLOWER_FROM_USER_ID, TableBuilder.Type.INTEGER_NOT_NULL)
                .withColumn(Schema.Follower.FOLLOWER_TO_USER_ID, TableBuilder.Type.INTEGER_NOT_NULL)
                .withForeignKey(Schema.Follower.FOLLOWER_FROM_USER_ID, Schema.User.TABLE_NAME, Schema.User.USER_ID)
                .withForeignKey(Schema.Follower.FOLLOWER_TO_USER_ID, Schema.User.TABLE_NAME, Schema.User.USER_ID)
                .createTable(Schema.Follower.TABLE_NAME);
        new TableBuilder(db)
                .withColumn(Schema.Comment.COMMENT_ID, TableBuilder.Type.INTEGER_PRIMARY_KEY_AUTOINCREMENT)
                .withColumn(Schema.Comment.COMMENT_USER_ID, TableBuilder.Type.INTEGER_NOT_NULL)
                .withColumn(Schema.Comment.COMMENT_POST_ID, TableBuilder.Type.INTEGER_NOT_NULL)
                .withColumn(Schema.Comment.COMMENT_TEXT, TableBuilder.Type.TEXT_NOT_NULL)
                .withColumn(Schema.Comment.COMMENT_CREATED_AT, TableBuilder.Type.INTEGER_NOT_NULL)
                .withForeignKey(Schema.Comment.COMMENT_USER_ID, Schema.User.TABLE_NAME, Schema.User.USER_ID)
                .withForeignKey(Schema.Comment.COMMENT_POST_ID, Schema.Post.TABLE_NAME, Schema.Post.POST_ID)
                .createTable(Schema.Comment.TABLE_NAME);
        new TableBuilder(db)
                .withColumn(Schema.Tag.TAG_NAME, TableBuilder.Type.TEXT_NOT_NULL)
                .withColumn(Schema.Tag.TAG_POST_ID, TableBuilder.Type.INTEGER_NOT_NULL)
                .createTable(Schema.Tag.TABLE_NAME);
        new TableBuilder(db)
                .withColumn(Schema.HomePost.POST_ID, TableBuilder.Type.INTEGER_PRIMARY_KEY)
                .createTable(Schema.HomePost.TABLE_NAME);
        new TableBuilder(db)
                .withColumn(Schema.ExplorePost.POST_ID, TableBuilder.Type.INTEGER_PRIMARY_KEY)
                .createTable(Schema.ExplorePost.TABLE_NAME);
    }

    public Uri insertUser(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        long id = database.insertWithOnConflict(Schema.User.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        return ContentUris.withAppendedId(CacheDataProvider.USER_URI, id);
    }

    public Cursor getUsers(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String query = "SELECT * FROM " + Schema.User.TABLE_NAME + " LEFT JOIN " +
                Schema.Picture.TABLE_NAME + " ON " + Schema.User.USER_PICTURE + " = " +
                Schema.Picture.PICTURE_ID;
        return getReadableDatabase().query("(" + query + ")", projection, selection, selectionArgs, null, null, sortOrder);
    }

    public int updateUser(ContentValues contentValues, String selection, String[] selectionArgs) {
        return getWritableDatabase().update(Schema.User.TABLE_NAME, contentValues, selection, selectionArgs);
    }

    public Uri insertPicture(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        long id = database.insertWithOnConflict(Schema.Picture.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        return ContentUris.withAppendedId(CacheDataProvider.PICTURE_URI, id);
    }

    public Uri insertPost(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        long id = database.insertWithOnConflict(Schema.Post.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        return ContentUris.withAppendedId(CacheDataProvider.POST_URI, id);
    }

    public Uri insertHomePost(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        long id = database.insertWithOnConflict(Schema.HomePost.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        return ContentUris.withAppendedId(CacheDataProvider.POST_URI, id);
    }

    public Uri insertExplorePost(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        long id = database.insertWithOnConflict(Schema.ExplorePost.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        return ContentUris.withAppendedId(CacheDataProvider.POST_URI, id);
    }

    public Uri insertTagPost(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        database.insertWithOnConflict(Schema.Tag.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        return ContentUris.withAppendedId(CacheDataProvider.POST_URI, contentValues.getAsLong(Schema.Tag.TAG_POST_ID));
    }

    public Cursor getPosts(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String query = "SELECT * FROM " + Schema.Post.TABLE_NAME + " JOIN " +
                Schema.User.TABLE_NAME + " ON " + Schema.Post.POST_USER_ID + " = " +
                Schema.User.USER_ID + " LEFT JOIN " + Schema.Picture.TABLE_NAME + " ON " +
                Schema.User.USER_PICTURE + " = " + Schema.Picture.PICTURE_ID;
        return getReadableDatabase().query("(" + query + ")", projection, selection, selectionArgs, null, null, sortOrder);
    }

    public Cursor getTagPosts(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String query = "SELECT * FROM " + Schema.Post.TABLE_NAME + " JOIN " +
                Schema.Tag.TABLE_NAME + " ON " + Schema.Post.POST_ID + " = " +
                Schema.Tag.TAG_POST_ID + " JOIN " + Schema.User.TABLE_NAME + " ON " +
                Schema.Post.POST_USER_ID + " = " + Schema.User.USER_ID + " LEFT JOIN " +
                Schema.Picture.TABLE_NAME + " ON " + Schema.User.USER_PICTURE + " = " +
                Schema.Picture.PICTURE_ID;
        return getReadableDatabase().query("(" + query + ")", projection, selection, selectionArgs, null, null, sortOrder);
    }

    public Cursor getHomePosts(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String query = "SELECT * FROM " + Schema.Post.TABLE_NAME + " JOIN " +
                Schema.HomePost.TABLE_NAME + " ON " + Schema.Post.POST_ID + " = " +
                Schema.HomePost.POST_ID + " JOIN " + Schema.User.TABLE_NAME + " ON " +
                Schema.Post.POST_USER_ID + " = " + Schema.User.USER_ID + " LEFT JOIN " +
                Schema.Picture.TABLE_NAME + " ON " + Schema.User.USER_PICTURE + " = " +
                Schema.Picture.PICTURE_ID;
        return getReadableDatabase().query("(" + query + ")", projection, selection, selectionArgs, null, null, sortOrder);
    }

    public Cursor getExplorePosts(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String query = "SELECT * FROM " + Schema.Post.TABLE_NAME + " JOIN " +
                Schema.ExplorePost.TABLE_NAME + " ON " + Schema.Post.POST_ID + " = " +
                Schema.ExplorePost.POST_ID + " JOIN " + Schema.User.TABLE_NAME + " ON " +
                Schema.Post.POST_USER_ID + " = " + Schema.User.USER_ID + " LEFT JOIN " +
                Schema.Picture.TABLE_NAME + " ON " + Schema.User.USER_PICTURE + " = " +
                Schema.Picture.PICTURE_ID;
        return getReadableDatabase().query("(" + query + ")", projection, selection, selectionArgs, null, null, sortOrder);
    }

    public int deletePosts(String whereClause, String[] whereArgs) {
        SQLiteDatabase database = getWritableDatabase();
        return database.delete(Schema.Post.TABLE_NAME, whereClause, whereArgs);
    }

    public int deleteHomePosts(String whereClause, String[] whereArgs) {
        SQLiteDatabase database = getWritableDatabase();
        return database.delete(Schema.HomePost.TABLE_NAME, whereClause, whereArgs);
    }

    public int deleteExplorePosts(String whereClause, String[] whereArgs) {
        SQLiteDatabase database = getWritableDatabase();
        return database.delete(Schema.ExplorePost.TABLE_NAME, whereClause, whereArgs);
    }

    public int deleteTagPosts(String whereClause, String[] whereArgs) {
        SQLiteDatabase database = getWritableDatabase();
        return database.delete(Schema.Tag.TABLE_NAME, whereClause, whereArgs);
    }

    public Cursor getPostComments(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String query = "SELECT * FROM " + Schema.Comment.TABLE_NAME + " JOIN " + Schema.User.TABLE_NAME +
                " ON " + Schema.Comment.COMMENT_USER_ID + " = " + Schema.User.USER_ID + " LEFT JOIN " +
                Schema.Picture.TABLE_NAME + " ON " + Schema.User.USER_PICTURE + " = " +
                Schema.Picture.PICTURE_ID;
        return getReadableDatabase().query("(" + query + ")", projection, selection, selectionArgs, null, null, sortOrder);
    }

    public Uri insertPostComment(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        long id = database.insertWithOnConflict(Schema.Comment.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        return Uri.withAppendedPath(CacheDataProvider.POST_URI, String.valueOf(id) + "/comments");
    }

    public int deleteComments(String whereClause, String[] whereArgs) {
        return getWritableDatabase().delete(Schema.Comment.TABLE_NAME, whereClause, whereArgs);
    }

    /*
    public Cursor getPostRatings(long postId, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return Query.Builder()
                .selectDistinct(projection)
                .fromTable(Schema.Rating.TABLE_NAME)
                    .leftJoin(Schema.User.TABLE_NAME)
                    .on(Schema.Rating.TABLE_NAME, Schema.Rating.RATING_USER_ID, Schema.User.TABLE_NAME, Schema.User.USER_ID)
                    .leftJoin(Schema.Picture.TABLE_NAME)
                    .on(Schema.User.TABLE_NAME, Schema.User.USER_PICTURE, Schema.Picture.TABLE_NAME, Schema.Picture.PICTURE_ID)
                .where()
                    .columnEquals(Schema.Rating.RATING_POST_ID, postId)
                    .addCondition(selection, selectionArgs)
                .orderBy(Schema.User.USER_USERNAME, OrderByBuilder.Mode.ASC)
                .orderBy(sortOrder)
                .query(getReadableDatabase());
    }

    public Uri insertPostRating(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        long id = database.insert(Schema.Rating.TABLE_NAME, null, contentValues);
        String pathSegment = String.valueOf(id) + "/ratings";
        return Uri.withAppendedPath(CacheDataProvider.POST_URI, pathSegment);
    }

    public int deletePostRatings(long postId, String whereClause, String[] whereArgs) {
        WhereBuilder builder = new WhereBuilder(null)
                .columnEquals(Schema.Rating.RATING_POST_ID, postId)
                .addCondition(whereClause, whereArgs);
        SQLiteDatabase database = getWritableDatabase();
        return database.delete(Schema.Rating.TABLE_NAME, builder.getSql(), builder.getCompiledArguments());
    }*/

    public Cursor getUserFollowerList(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String query = "SELECT * FROM " + Schema.Follower.TABLE_NAME + " JOIN " + Schema.User.TABLE_NAME +
                " ON " + Schema.Follower.FOLLOWER_FROM_USER_ID + " = " + Schema.User.USER_ID + " LEFT JOIN " +
                Schema.Picture.TABLE_NAME + " ON " + Schema.User.USER_PICTURE + " = " +
                Schema.Picture.PICTURE_ID;
        return getReadableDatabase().query("(" + query + ")", projection, selection, selectionArgs, null, null, sortOrder);
    }

    public Uri insertUserFollower(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        long id = database.insert(Schema.Follower.TABLE_NAME, null, contentValues);
        return Uri.withAppendedPath(CacheDataProvider.USER_URI, String.valueOf(id) + "/follower");
    }

    public int deleteUserFollowerList(String whereClause, String[] whereArgs) {
        return getWritableDatabase().delete(Schema.Follower.TABLE_NAME, whereClause, whereArgs);
    }

    public Cursor getUserFollowedList(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String query = "SELECT * FROM " + Schema.Follower.TABLE_NAME + " JOIN " + Schema.User.TABLE_NAME +
                " ON " + Schema.Follower.FOLLOWER_TO_USER_ID + " = " + Schema.User.USER_ID + " LEFT JOIN " +
                Schema.Picture.TABLE_NAME + " ON " + Schema.User.USER_PICTURE + " = " +
                Schema.Picture.PICTURE_ID;
        return getReadableDatabase().query("(" + query + ")", projection, selection, selectionArgs, null, null, sortOrder);
    }

    public Uri insertUserFollowed(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        long id = database.insert(Schema.Follower.TABLE_NAME, null, contentValues);
        return Uri.withAppendedPath(CacheDataProvider.USER_URI, String.valueOf(id) + "/followed");
    }

    public int deleteUserFollowedList(String whereClause, String[] whereArgs) {
        return getWritableDatabase().delete(Schema.Follower.TABLE_NAME, whereClause, whereArgs);
    }
}