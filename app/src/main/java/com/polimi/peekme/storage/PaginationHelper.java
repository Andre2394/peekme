package com.polimi.peekme.storage;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.polimi.peekme.api.model.Metadata;

/**
 * Created by andre on 27/12/2016.
 */
public class PaginationHelper extends RecyclerView.OnScrollListener {

    private static final String SAVED_SIZE = "PaginationHelper::itemSize";
    private static final String SAVED_CAN_LOAD_MORE = "PaginationHelper::canLoadMore";
    private static final String SAVED_LAST_ID = "PaginationHelper::lastId";

    private static final int OFFSET = 20;

    private int mSize;
    private boolean mCanLoadMore;
    private boolean mLoading;
    private Long mLastId;

    private final RecyclerView mRecyclerView;
    private final LinearLayoutManager mLayoutManager;
    private final LoaderCallback mLoaderCallback;

    public PaginationHelper(RecyclerView recyclerView, Bundle savedState, LoaderCallback loaderCallback) {
        mRecyclerView = recyclerView;
        mLoaderCallback = loaderCallback;
        // initialize or restore the state
        if (savedState != null) {
            mLoading = false;
            mSize = savedState.getInt(SAVED_SIZE);
            mCanLoadMore = savedState.getBoolean(SAVED_CAN_LOAD_MORE);
            if (savedState.containsKey(SAVED_LAST_ID)) {
                mLastId = savedState.getLong(SAVED_LAST_ID);
            } else {
                mLastId = null;
            }
        } else {
            mLoading = true;
            mSize = 0;
            mCanLoadMore = false;
            mLastId = null;
        }
        // prepare recycler view
        mLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(this);
    }

    public void onRefresh() {
        mLoading = true;
        mSize = 0;
        mCanLoadMore = false;
        mLastId = null;
    }

    public void onNewItems(Metadata metadata) {
        mLoading = false;
        if (!mCanLoadMore) {
            // mLayoutManager.scrollToPosition(0); TODO [TEST REQUIRED] find a better way to handle this
        }
        mSize += metadata.size();
        mCanLoadMore = metadata.size() != 0;
        mLastId = metadata.getLastId();
        System.out.println("Metadata loaded, size: " + mSize + ", canLoadMore: " + mCanLoadMore + ", lastId: " + mLastId);
    }

    public Bundle getSavedState() {
        Bundle outState = new Bundle();
        outState.putInt(SAVED_SIZE, mSize);
        outState.putBoolean(SAVED_CAN_LOAD_MORE, mCanLoadMore);
        if (mLastId != null) {
            outState.putLong(SAVED_LAST_ID, mLastId);
        }
        return outState;
    }

    public void release() {
        mRecyclerView.removeOnScrollListener(this);
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (!mLoading && mCanLoadMore) {
            int lastItem = mLayoutManager.findLastVisibleItemPosition() + 1;
            if (mSize <= (lastItem + OFFSET)) {
                // it's time to request more data
                mLoading = true;
                if (mLoaderCallback != null) {
                    System.out.println("Loading more items!");
                    mLoaderCallback.onLoadMore(mLastId);
                }
            }
        }
    }

    public interface LoaderCallback {

        void onLoadMore(long lastId);
    }
}