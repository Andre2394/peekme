package com.polimi.peekme.storage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.polimi.peekme.api.model.BaseUser;
import com.polimi.peekme.api.model.Comment;
import com.polimi.peekme.api.model.Picture;
import com.polimi.peekme.api.model.Post;
import com.polimi.peekme.api.model.User;
import com.polimi.peekme.api.storage.ICache;

import java.util.List;
import java.util.Locale;

/**
 * This class is an implementation of a static storage to use to cache network data on device.
 * It uses an internal SQLite database to handle the storage of the provided items. It will use
 * a {@link SharedPreferences} file to store the current logged item and retrieve it when required.
 */
public class SQLiteCache implements ICache {

    /**
     * SharedPreference file constants.
     */
    private static final String CACHE_FILE = "cache.xml";
    private static final String CURRENT_USER = "current_user";

    /**
     * SharedPreference file reference.
     */
    private final SharedPreferences mCache;

    /**
     * Content Resolver reference to handle SQLite requests.
     */
    private final ContentResolver mContentResolver;

    /**
     * Public constructor.
     * @param context of the application in order to access private files.
     */
    public SQLiteCache(Context context) {
        mCache = context.getSharedPreferences(CACHE_FILE, Context.MODE_PRIVATE);
        mContentResolver = context.getContentResolver();
    }

    /**
     * Store the id of the current user.
     * @param userId of the current user.
     */
    @Override
    public void setCurrentUserId(long userId) {
        mCache.edit().putLong(CURRENT_USER, userId).apply();
    }

    /**
     * Get the id of the current user.
     * @return the id of the current user if found, {@link #NO_USER} if no user is currently logged in.
     */
    @Override
    public long getCurrentUserId() {
        return mCache.getLong(CURRENT_USER, NO_USER);
    }

    /**
     * Store a user into the cache. If user is an instance of {@link BaseUser} and the same user
     * already exists in database it will not be replaced. If user is an instance of {@link User}
     * than it will be stored or replaced even if it already exists because it can contains updated
     * information like follower / followed count.
     * @param user item to store in the cache.
     */
    @Override
    public void storeUser(@NonNull BaseUser user) {
        if (user.hasPicture()) {
            Picture picture = user.getPicture();
            ContentValues contentValues = picture.getContentValues();
            mContentResolver.insert(CacheDataProvider.PICTURE_URI, contentValues);
        }
        ContentValues contentValues = user.getContentValues();
        if (!(user instanceof User)) {
            Uri uri = CacheDataProvider.USER_URI;
            String where = Schema.User.USER_ID + " = ?";
            if (mContentResolver.update(uri, contentValues, where, new String[] {String.valueOf(user.getId())}) > 0) {
                // user already exists in cache, so it has been only updated. Skipping replacement.
                return;
            }
        }
        mContentResolver.insert(CacheDataProvider.USER_URI, contentValues);
    }

    /**
     * Store a list of users into the cache. If user is an instance of {@link BaseUser} and the same user
     * already exists in database it will not be replaced. If user is an instance of {@link User}
     * than it will be stored or replaced even if it already exists because it can contains updated
     * information like follower / followed count.
     * @param users list of users to store.
     */
    @Override
    public void storeUserList(List<BaseUser> users) {
        for (BaseUser user : users) {
            storeUser(user);
        }
    }

    /**
     * Remove a reference to all posts that should be displayed in the home section.
     * This method will remove everything from {@link Schema.HomePost} table.
     */
    @Override
    public void clearHomePostsCache() {
        Uri uri = CacheDataProvider.HOME_POST_URI;
        mContentResolver.delete(uri, null, null);
    }

    /**
     * Remove a reference to all posts that should be displayed in the explore section.
     * This method will remove everything from {@link Schema.ExplorePost} table.
     */
    @Override
    public void clearExplorePostsCache() {
        Uri uri = CacheDataProvider.EXPLORE_POST_URI;
        mContentResolver.delete(uri, null, null);
    }

    /**
     * Remove all posts related to the provided tag.
     * @param tag that should be removed from the cache.
     */
    @Override
    public void clearTagPostsCache(String tag) {
        Uri uri = CacheDataProvider.TAG_POST_URI;
        String where = Schema.Tag.TAG_NAME + " = ?";
        mContentResolver.delete(uri, where, new String[] {tag});
    }

    /**
     * Remove all posts that are linked to the provided user id.
     * @param userId id of the owner of the posts to remove.
     */
    @Override
    public void clearUserPostsCache(long userId) {
        Uri uri = CacheDataProvider.POST_URI;
        String where = Schema.Post.POST_USER_ID + " = ?";
        mContentResolver.delete(uri, where, new String[] {String.valueOf(userId)});
    }

    /**
     * Store a single post of the current user. This post will be shown in home section too.
     * @param post item to store.
     */
    @Override
    public void storeCurrentUserPost(Post post) {
        storePost(post, false, true, false);
    }

    /**
     * Store a single post.
     * @param post item to store.
     */
    @Override
    public void storeStandardPost(Post post) {
        storePost(post, false, false, false);
    }

    /**
     * Store all posts of the same user.
     * @param posts list of user's posts.
     */
    @Override
    public void storeUserPosts(List<Post> posts) {
        for (Post post : posts) {
            storePost(post, true, false, false);
        }
    }

    /**
     * Store all posts to show in home section.
     * @param posts list of posts to store.
     */
    @Override
    public void storeHomePosts(List<Post> posts) {
        for (Post post : posts) {
            storePost(post, false, true, false);
        }
    }

    /**
     * Store all posts to show in explore section.
     * @param posts list of posts to store.
     */
    @Override
    public void storeExplorePosts(List<Post> posts) {
        for (Post post : posts) {
            storePost(post, false, false, true);
        }
    }

    /**
     * Store all posts to show in the provided tag section.
     * @param tag of the post.
     * @param posts list of posts to store.
     */
    @Override
    public void storeTagPosts(String tag, List<Post> posts) {
        for (Post post : posts) {
            storePost(post, false, false, false);
            Uri uri = CacheDataProvider.TAG_POST_URI;
            ContentValues contentValues = new ContentValues();
            contentValues.put(Schema.Tag.TAG_NAME, tag);
            contentValues.put(Schema.Tag.TAG_POST_ID, post.getId());
            mContentResolver.insert(uri, contentValues);
        }
    }

    /**
     * Store a post in the database. Use the skipUserCaching optimization only if you are sure that
     * the same user is already stored in the {@link Schema.User} table.
     * @param post item to store.
     * @param skipUserCaching if true the user caching is disabled for this post.
     * @param showHome if true the post will be referenced into {@link Schema.HomePost} table.
     * @param showExplore if true the post will be referenced into {@link Schema.ExplorePost} table.
     */
    private void storePost(Post post, boolean skipUserCaching, boolean showHome, boolean showExplore) {
        storePost(post, skipUserCaching);
        if (showHome) {
            Uri uri = CacheDataProvider.HOME_POST_URI;
            ContentValues contentValues = new ContentValues();
            contentValues.put(Schema.HomePost.POST_ID, post.getId());
            mContentResolver.insert(uri, contentValues);
        }
        if (showExplore) {
            Uri uri = CacheDataProvider.EXPLORE_POST_URI;
            ContentValues contentValues = new ContentValues();
            contentValues.put(Schema.ExplorePost.POST_ID, post.getId());
            mContentResolver.insert(uri, contentValues);
        }
    }

    /**
     * Store a post in the database. Use the skipUserCaching optimization only if you are sure that
     * the same user is already stored in the {@link Schema.User} table.
     * @param post item to store.
     * @param skipUserCaching if true the user caching is disabled for this post.
     */
    private void storePost(Post post, boolean skipUserCaching) {
        Uri uri = CacheDataProvider.POST_URI;
        if (!skipUserCaching) {
            BaseUser user = post.getUser();
            storeUser(user);
        }
        mContentResolver.insert(uri, post.getContentValues());
    }

    /**
     * Remove all comments related to the provided post id.
     * @param postId id of the post from which the comments should be removed.
     */
    @Override
    public void clearPostCommentsCache(long postId) {
        String pathSegment = String.valueOf(postId) + "/comments";
        Uri uri = Uri.withAppendedPath(CacheDataProvider.POST_URI, pathSegment);
        mContentResolver.delete(uri, null, null);
    }

    /**
     * Store a post comment.
     * @param comment item to store.
     */
    @Override
    public void storePostComment(Comment comment) {
        String pathSegment = String.valueOf(comment.getPostId()) + "/comments";
        Uri uri = Uri.withAppendedPath(CacheDataProvider.POST_URI, pathSegment);
        storeUser(comment.getUser());
        mContentResolver.insert(uri, comment.getContentValues());
    }

    /**
     * Store a list of post comments.
     * @param comments list of comments to store.
     */
    @Override
    public void storePostComments(List<Comment> comments) {
        for (Comment comment : comments) {
            storePostComment(comment);
        }
    }

    /**
     * Remove the comment from the cache.
     * @param postId is of the post related to the comment.
     * @param commentId id of the comment to remove.
     */
    @Override
    public void deletePostComment(long postId, long commentId) {
        String pathSegment = String.format(Locale.ENGLISH, "%d/comments/%d", postId, commentId);
        Uri uri = Uri.withAppendedPath(CacheDataProvider.POST_URI, pathSegment);
        mContentResolver.delete(uri, null, null);
        // TODO decrement comment counter of the post
    }

    /*
    @Override
    public void clearPostRatingsCache(long postId) {
        String pathSegment = String.valueOf(postId) + "/ratings";
        Uri uri = Uri.withAppendedPath(CacheDataProvider.POST_URI, pathSegment);
        mContentResolver.delete(uri, null, null);
    }

    @Override
    public void storePostRating(Rating rating) {
        String pathSegment = String.valueOf(rating.getPostId()) + "/ratings";
        Uri uri = Uri.withAppendedPath(CacheDataProvider.POST_URI, pathSegment);
        BaseUser user = rating.getUser();
        storeUser(user);
        ContentValues contentValues = rating.getContentValues();
        mContentResolver.insert(uri, contentValues);
    }

    @Override
    public void storePostRatings(List<Rating> ratings) {
        for (Rating rating : ratings) {
            storePostRating(rating);
        }
    }*/

    /**
     * Remove all references to the followers of the provided user.
     * @param userId id of the user.
     */
    @Override
    public void clearUserFollowerListCache(long userId) {
        String pathSegment = String.valueOf(userId) + "/follower";
        Uri uri = Uri.withAppendedPath(CacheDataProvider.USER_URI, pathSegment);
        mContentResolver.delete(uri, null, null);
    }

    /**
     * Store all references to the followers of the provided user.
     * @param userId id of the user.
     * @param users list of users that are following the provided user.
     */
    @Override
    public void storeUserFollowerList(long userId, List<BaseUser> users) {
        String pathSegment = String.valueOf(userId) + "/follower";
        Uri uri = Uri.withAppendedPath(CacheDataProvider.USER_URI, pathSegment);
        for (BaseUser user : users) {
            storeUser(user);
            ContentValues contentValues = new ContentValues();
            contentValues.put(Schema.Follower.FOLLOWER_FROM_USER_ID, user.getId());
            mContentResolver.insert(uri, contentValues);
        }
    }

    /**
     * Remove all references to followed users by the provided user.
     * @param userId id of the user.
     */
    @Override
    public void clearUserFollowedListCache(long userId) {
        String pathSegment = String.valueOf(userId) + "/followed";
        Uri uri = Uri.withAppendedPath(CacheDataProvider.USER_URI, pathSegment);
        mContentResolver.delete(uri, null, null);
    }

    /**
     * Store all references to followed users by the provided user.
     * @param userId id of the user.
     * @param users list of users that are followed by the provided user.
     */
    @Override
    public void storeUserFollowedList(long userId, List<BaseUser> users) {
        String pathSegment = String.valueOf(userId) + "/followed";
        Uri uri = Uri.withAppendedPath(CacheDataProvider.USER_URI, pathSegment);
        for (BaseUser user : users) {
            storeUser(user);
            ContentValues contentValues = new ContentValues();
            contentValues.put(Schema.Follower.FOLLOWER_TO_USER_ID, user.getId());
            mContentResolver.insert(uri, contentValues);
        }
    }

    /**
     * Store a new followed user. Store a reference to all user's posts in the
     * {@link Schema.HomePost} table.
     * @param userId id of the user that is following the provided user.
     * @param user item that is followed by the userId
     */
    @Override
    public void storeFollowedUser(long userId, BaseUser user) {
        storeUser(user);
        String pathSegment = String.valueOf(userId) + "/followed";
        Uri uri = Uri.withAppendedPath(CacheDataProvider.USER_URI, pathSegment);
        ContentValues contentValues = new ContentValues();
        contentValues.put(Schema.Follower.FOLLOWER_TO_USER_ID, user.getId());
        mContentResolver.insert(uri, contentValues);
        // make all cached post of the user visible in the home
        // TODO: [TEST REQUIRED] make test on large collection. Is this necessary for user experience?
        uri = CacheDataProvider.POST_URI;
        String[] projections = new String[] {
                Schema.Post.POST_ID
        };
        String where = Schema.Post.POST_USER_ID + " = ?";
        String[] whereArgs = new String[] {String.valueOf(user.getId())};
        Cursor cursor = mContentResolver.query(uri, projections, where, whereArgs, null);
        if (cursor != null) {
            int index = cursor.getColumnIndex(Schema.Post.POST_ID);
            while (cursor.moveToNext()) {
                contentValues = new ContentValues();
                contentValues.put(Schema.HomePost.POST_ID, cursor.getLong(index));
                mContentResolver.insert(CacheDataProvider.HOME_POST_URI, contentValues);
            }
            cursor.close();
        }
    }

    /**
     * Remove the followed user. Remove all references to all user's posts from the
     * {@link Schema.HomePost} table.
     * @param userId id of the user that is following the provided user.
     * @param user item that is followed by the userId
     */
    @Override
    public void clearFollowedUser(long userId, BaseUser user) {
        storeUser(user);
        String pathSegment = String.valueOf(user.getId()) + "/followed/" + String.valueOf(userId);
        Uri uri = Uri.withAppendedPath(CacheDataProvider.USER_URI, pathSegment);
        mContentResolver.delete(uri, null, null);
        // make all cached post of the user not more visible in the home
        String where = Schema.HomePost.POST_ID + " IN (SELECT " + Schema.Post.POST_ID + " FROM " +
                Schema.Post.TABLE_NAME + " WHERE " + Schema.Post.POST_USER_ID + " = ?)";
        mContentResolver.delete(CacheDataProvider.HOME_POST_URI, where, new String[] {String.valueOf(user.getId())});
    }
}