package com.polimi.peekme.storage;

/**
 * Created by andre on 26/10/2016.
 */
public class Schema {

    public static class User {
        public static final String TABLE_NAME = "users";
        public static final String USER_ID = "user_id";
        public static final String USER_USERNAME = "user_username";
        public static final String USER_EMAIL = "user_email";
        public static final String USER_NAME = "user_name";
        public static final String USER_SURNAME = "user_surname";
        public static final String USER_SEX = "user_sex";
        public static final String USER_BIRTHDAY = "user_birthday";
        public static final String USER_PICTURE = "user_picture_id";
        public static final String USER_FOLLOWER_COUNT = "user_follower_count";
        public static final String USER_FOLLOWED_COUNT = "user_followed_count";
        public static final String USER_FOLLOWED_BY_YOU = "user_followed_by_you";
    }

    public static class Picture {
        public static final String TABLE_NAME = "pictures";
        public static final String PICTURE_ID = "picture_id";
        public static final String PICTURE_URL = "picture_file_url";
        public static final String PICTURE_MIME_TYPE = "picture_mime_type";
    }

    public static class Post {
        public static final String TABLE_NAME = "posts";
        public static final String POST_ID = "post_id";
        public static final String POST_USER_ID = "post_user_id";
        public static final String POST_MEDIA_URL = "post_media_file_url";
        public static final String POST_MEDIA_MIME_TYPE = "post_media_mime_type";
        public static final String POST_DESCRIPTION = "post_description";
        public static final String POST_RATING_TOTAL = "post_rating_total";
        public static final String POST_RATING_COUNT = "post_rating_count";
        public static final String POST_USER_RATING = "post_user_rating";
        public static final String POST_COMMENT_COUNT = "post_comment_count";
        public static final String POST_CREATED_AT = "post_created_at";
        public static final String POST_UPDATED_AT = "post_updated_at";
        public static final String POST_SHOW_HOME = "post_show_in_home";
        public static final String POST_SHOW_EXPLORE = "post_show_in_explore";
    }

    public static class Rating {
        public static final String TABLE_NAME = "ratings";
        public static final String RATING_ID = "rating_id";
        public static final String RATING_USER_ID = "rating_user_id";
        public static final String RATING_POST_ID = "rating_post_id";
        public static final String RATING_VALUE = "rating_value";
    }

    public static class Follower {
        public static final String TABLE_NAME = "followers";
        public static final String FOLLOWER_FROM_USER_ID = "follower_from_user_id";
        public static final String FOLLOWER_TO_USER_ID = "follower_to_user_id";
    }

    public static class Comment {
        public static final String TABLE_NAME = "comments";
        public static final String COMMENT_ID = "comment_id";
        public static final String COMMENT_USER_ID = "comment_user_id";
        public static final String COMMENT_POST_ID = "comment_post_id";
        public static final String COMMENT_TEXT = "comment_text";
        public static final String COMMENT_CREATED_AT = "comment_created_at";
    }

    public static class Tag {
        public static final String TABLE_NAME = "tags";
        public static final String TAG_NAME = "tag_name";
        public static final String TAG_POST_ID = "tag_post_id";
    }

    public static class HomePost {
        public static final String TABLE_NAME = "home_posts";
        public static final String POST_ID = "_post_id";
    }

    public static class ExplorePost {
        public static final String TABLE_NAME = "explore_posts";
        public static final String POST_ID = "_post_id";
    }
}