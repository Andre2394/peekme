package com.polimi.peekme.storage;

import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import java.util.ArrayList;

/**
 * Created by andre.
 */
public class TableBuilder {

    private SQLiteDatabase mDatabase;
    private ArrayList<String> mColumns;

    public TableBuilder(SQLiteDatabase database) {
        mDatabase = database;
        mColumns = new ArrayList<>();
    }

    public TableBuilder withColumn(String name, Type type) {
        return withColumn(name, type, null);
    }

    public TableBuilder withColumn(String name, Type type, String defaultValue) {
        StringBuilder builder = new StringBuilder()
                .append(name)
                .append(" ")
                .append(type.toString());
        if (!TextUtils.isEmpty(defaultValue)) {
            builder.append(" DEFAULT ");
            builder.append(defaultValue);
        }
        mColumns.add(builder.toString());
        return this;
    }

    public TableBuilder withForeignKey(String column, String tableRef, String columnRef) {
        mColumns.add("FOREIGN KEY(" + column + ") REFERENCES " + tableRef + "(" + columnRef + ")");
        return this;
    }

    public void createTable(String name) {
        if (!TextUtils.isEmpty(name)) {
            StringBuilder builder = new StringBuilder();
            builder.append("CREATE TABLE IF NOT EXISTS ");
            builder.append(name);
            builder.append(" (");
            for (int i = 0; i < mColumns.size(); i++) {
                builder.append(mColumns.get(i));
                if (i != mColumns.size() - 1) {
                    builder.append(",");
                }
            }
            builder.append(")");
            mDatabase.execSQL(builder.toString());
        }
    }

    public enum Type {
        INTEGER,
        INTEGER_PRIMARY_KEY,
        INTEGER_PRIMARY_KEY_AUTOINCREMENT,
        INTEGER_NOT_NULL,
        TEXT,
        TEXT_NOT_NULL,
        BOOLEAN,
        BOOLEAN_NOT_NULL;

        @Override
        public String toString() {
            switch (this) {
                case INTEGER_PRIMARY_KEY:
                    return "INTEGER PRIMARY KEY";
                case INTEGER_PRIMARY_KEY_AUTOINCREMENT:
                    return "INTEGER PRIMARY KEY AUTOINCREMENT";
                case INTEGER:
                    return "INTEGER";
                case INTEGER_NOT_NULL:
                    return "INTEGER NOT NULL";
                case TEXT:
                    return "TEXT";
                case TEXT_NOT_NULL:
                    return "TEXT NOT NULL";
                case BOOLEAN:
                    return "BOOLEAN";
                case BOOLEAN_NOT_NULL:
                    return "BOOLEAN NOT NULL";
            }
            return null;
        }
    }
}