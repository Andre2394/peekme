package com.polimi.peekme.storage;

/**
 * Created by andre on 26/10/2016.
 */
public class Thumbnail {

    private static final String MIME_TYPE_GIF = "image/gif";

    public static boolean isGif(String mimeType) {
        return MIME_TYPE_GIF.equals(mimeType);
    }
}