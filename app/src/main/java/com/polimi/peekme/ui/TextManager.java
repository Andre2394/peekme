package com.polimi.peekme.ui;

import android.graphics.Color;
import android.text.Selection;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by andre on 04/11/2016.
 */
public class TextManager {

    private static final Pattern PATTERN_HASH_TAG = Pattern.compile("#\\w+");
    private static final Pattern PATTERN_USER_TAG = Pattern.compile("@[a-zA-Z1-9._]+");

    public static void applyHashTagRecognizer(TextView textView, String text, final TagClickListener listener) {
        SpannableString spannableString = new SpannableString(text);
        List<int[]> hashTagIndices = getSpans(text, PATTERN_HASH_TAG);
        List<int[]> userTagIndices = getSpans(text, PATTERN_USER_TAG);
        for (int[] indices : hashTagIndices) {
            spannableString.setSpan(new ClickableSpan() {

                @Override
                public void updateDrawState(TextPaint textPaint) {
                    textPaint.setColor(Color.BLUE);
                }

                @Override
                public void onClick(View view) {
                    TextView textView = (TextView) view;
                    Spanned spanned = (Spanned) textView.getText();
                    int start = spanned.getSpanStart(this);
                    int end = spanned.getSpanEnd(this);
                    listener.onHashTagClick(spanned.subSequence(start + 1, end).toString());
                }

            }, indices[0], indices[1], 0);
        }
        for (int[] indices : userTagIndices) {
            spannableString.setSpan(new ClickableSpan() {

                @Override
                public void updateDrawState(TextPaint textPaint) {
                    textPaint.setColor(Color.BLUE);
                }

                @Override
                public void onClick(View view) {
                    TextView textView = (TextView) view;
                    Spanned spanned = (Spanned) textView.getText();
                    int start = spanned.getSpanStart(this);
                    int end = spanned.getSpanEnd(this);
                    listener.onUserTagClick(spanned.subSequence(start + 1, end).toString());
                }

            }, indices[0], indices[1], 0);
        }
        textView.setHighlightColor(Color.TRANSPARENT);
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public static List<int[]> getSpans(String body, Pattern pattern) {
        Matcher matcher = pattern.matcher(body);
        List<int[]> spans = new ArrayList<>();
        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }
        return spans;
    }

    public interface TagClickListener {

        void onHashTagClick(String hashTag);

        void onUserTagClick(String userTag);
    }
}