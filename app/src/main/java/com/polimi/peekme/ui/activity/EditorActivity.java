package com.polimi.peekme.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.polimi.peekme.R;

public class EditorActivity extends AppCompatActivity {

    public static final String MEDIA_URI = "media_uri";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        Uri uri = getIntent().getParcelableExtra(MEDIA_URI);
        startUploadActivity(uri);
    }

    private void startUploadActivity(Uri uri) {
        Intent intent = new Intent(this, UploadPostActivity.class);
        intent.putExtra(UploadPostActivity.MEDIA_URI, uri);
        startActivity(intent);
        finish();
    }
}