package com.polimi.peekme.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.polimi.peekme.R;
import com.polimi.peekme.api.PeekMeServer;
import com.polimi.peekme.api.Protocol;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.service.UserService;

/**
 * Created by andre on 14/10/2016.
 */
public class LoginRegisterActivity extends AppCompatActivity {

    private EditText mUsernameField;
    private EditText mEmailField;
    private EditText mPasswordField;
    private EditText mPasswordCheckField;
    private Button mActionButton;
    private TextView mHelpField;

    private boolean mLoginMode = false;
    private boolean mTaskRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkState();
        initializeUi();
        initializeListeners();
        loadUi();
    }

    private void checkState() {
        UserService userService = PeekMeServer.getUserService();
        if (userService.isUserLoggedIn()) {
            System.out.println("User is logged in");
            startMainActivity();
        } else {
            System.out.println("No User is logged in");
        }
    }

    private void initializeUi() {
        setContentView(R.layout.activity_login_register);
        mUsernameField = (EditText) findViewById(R.id.field_username);
        mEmailField = (EditText) findViewById(R.id.field_email);
        mPasswordField = (EditText) findViewById(R.id.field_password);
        mPasswordCheckField = (EditText) findViewById(R.id.field_password_check);
        mActionButton = (Button) findViewById(R.id.action_button);
        mHelpField = (TextView) findViewById(R.id.field_help);
    }

    private void initializeListeners() {
        mActionButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (!mTaskRunning) {
                    if (mLoginMode) {
                        doLogin();
                    } else {
                        doRegister();
                    }
                }
            }

        });
        mHelpField.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (!mTaskRunning) {
                    mLoginMode = !mLoginMode;
                    loadUi();
                }
            }

        });
    }

    private void loadUi() {
        mEmailField.setVisibility(mLoginMode ? View.GONE : View.VISIBLE);
        mPasswordCheckField.setVisibility(mLoginMode ? View.GONE : View.VISIBLE);
        mActionButton.setText(mLoginMode ? R.string.button_text_login : R.string.button_text_register);
        mHelpField.setText(mLoginMode ? R.string.help_user_not_yet_registered : R.string.help_user_already_registered);
    }

    private void doLogin() {
        String username = mUsernameField.getText().toString();
        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Username or password is empty", Toast.LENGTH_SHORT).show();
            return;
        }
        mTaskRunning = true;
        UserService userService = PeekMeServer.getUserService();
        userService.loginAsync(username, password, new ServerCallback<Void>() {

            @Override
            public void onRequestDone(Void item, ServerException e) {
                if (e != null) {
                    // an error is occurred
                    handleServerException(e);
                } else {
                    // everything is gone ok, simply end activity
                    startMainActivity();
                }
                mTaskRunning = false;
            }

        });
    }

    private void doRegister() {
        String username = mUsernameField.getText().toString();
        String email = mEmailField.getText().toString();
        String password = mPasswordField.getText().toString();
        String passwordCheck = mPasswordCheckField.getText().toString();
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Username, email or password is empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!TextUtils.equals(password, passwordCheck)) {
            Toast.makeText(this, "Password mismatch", Toast.LENGTH_SHORT).show();
            return;
        }
        mTaskRunning = true;
        UserService userService = PeekMeServer.getUserService();
        userService.registerAsync(username, email, password, new ServerCallback<Void>() {

            @Override
            public void onRequestDone(Void item, ServerException e) {
                if (e != null) {
                    // an error is occurred
                    handleServerException(e);
                } else {
                    // everything is gone ok, switch to login activity
                    mEmailField.getText().clear();
                    mPasswordField.getText().clear();
                    mPasswordCheckField.getText().clear();
                    mLoginMode = true;
                    loadUi();
                }
                mTaskRunning = false;
            }

        });
    }

    private void handleServerException(ServerException exception) {
        String error = exception.getError();
        switch (error) {
            case Protocol.INVALID_PARAMETER:
                // TODO parse which parameter is not valid
                break;
            case Protocol.MISSING_PARAMETER:
                // TODO parse which parameter is missing => sdk bugs?
                break;
            default:
                Toast.makeText(this, exception.getErrorDescription(), Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void showMessageDialog(@StringRes int title, @StringRes int message) {
        new MaterialDialog.Builder(this)
                .title(title)
                .content(message)
                .positiveText(android.R.string.ok)
                .negativeText(android.R.string.cancel)
                .show();
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        setResult(RESULT_OK);
        finish();
    }
}