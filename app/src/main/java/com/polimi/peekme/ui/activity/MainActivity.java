package com.polimi.peekme.ui.activity;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.StringHolder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.polimi.peekme.R;
import com.polimi.peekme.api.PeekMeServer;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.User;
import com.polimi.peekme.api.service.UserService;
import com.polimi.peekme.storage.CacheDataProvider;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.storage.Thumbnail;
import com.polimi.peekme.ui.fragment.ExploreFragment;
import com.polimi.peekme.ui.fragment.HomeFragment;

/**
 * This class is built on top of the android activity and it works as the base of the peekme app.
 * It will create a left navigation drawer and load the current user in the account header of the
 * drawer. It will even handle the lifecycle of the fragments which are replaced when a new drawer
 * section is selected.
 */
public class MainActivity extends AppCompatActivity implements Drawer.OnDrawerItemClickListener,
        Toolbar.OnMenuItemClickListener, LoaderManager.LoaderCallbacks<Cursor>,ServerCallback<User> {

    /**
     * Saved state constants.
     */
    private static final String SAVED_STATE_CURRENT_SECTION = "current_section";

    /**
     * Id of the loader that will retrieve the current user profile from cache.
     */
    private static final int LOADER_ID = 1;

    /**
     * Identifiers for the sections of the navigation drawer.
     */
    private static final long SECTION_HOME = 1;
    private static final long SECTION_EXPLORE = 2;

    /**
     * View references of the activity.
     */
    private Toolbar mToolbar;
    private Drawer mDrawer;
    private AccountHeader mAccountHeader;

    /**
     * Keep a reference to the current item of the drawer in order to reset it when configuration
     * changes (for example when device changes the screen orientation).
     */
    private long mCurrentSection;

    /**
     * Keep a references to the cursor user to load data from cache in order to close it when is
     * not more necessary. (You should not close it after loading data because when data of the uri
     * changes the loader will reuse this cursor and it will not create a new one!).
     */
    private Cursor mCursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUi();
        loadUi(savedInstanceState);
    }

    private void initializeUi() {
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_main);
        // setup
        // setSupportActionBar(mToolbar);
        // setTitle(R.string.app_name);
        mToolbar.setOnMenuItemClickListener(this);
        // initialize navigation drawer
        mAccountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.colorPrimary)
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {

                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                        Intent intent = new Intent(MainActivity.this, ShowUserActivity.class);
                        intent.putExtra(ShowUserActivity.USER, profile.getIdentifier());
                        startActivity(intent);
                        return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        return false;
                    }

                })
                .build();
        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(mToolbar)
                .withAccountHeader(mAccountHeader)
                .addDrawerItems(
                        addSection(SECTION_HOME, R.drawable.ic_dashboard_black_24dp, R.string.section_home),
                        addSection(SECTION_EXPLORE, R.mipmap.ic_launcher, R.string.section_explore)
                )
                .withOnDrawerItemClickListener(this)
                .build();
    }

    private void loadUi(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mCurrentSection = savedInstanceState.getLong(SAVED_STATE_CURRENT_SECTION, SECTION_HOME);
        } else {
            mCurrentSection = SECTION_HOME;
        }
        mDrawer.setSelection(mCurrentSection);
        getSupportLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(SAVED_STATE_CURRENT_SECTION, mCurrentSection);
    }

    private IDrawerItem addSection(long id, @DrawableRes int icon, @StringRes int name) {
        return new PrimaryDrawerItem()
                .withIdentifier(id)
                .withIcon(icon)
                .withName(name)
                .withIconTintingEnabled(true)
                .withSelectedIconColorRes(R.color.colorPrimary)
                .withSelectedTextColorRes(R.color.colorPrimary);
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        mCurrentSection = drawerItem.getIdentifier();
        loadFragment(mCurrentSection);
        if (drawerItem instanceof PrimaryDrawerItem) {
            StringHolder stringHolder = ((PrimaryDrawerItem) drawerItem).getName();
            mToolbar.setTitle(stringHolder.getText(this));
        }
        return false;
    }

    private void loadFragment(long identifier) {
        mToolbar.getMenu().clear();
        Fragment fragment;
        if (identifier == SECTION_HOME) {
            fragment = new HomeFragment();
        } else if (identifier == SECTION_EXPLORE) {
            fragment = new ExploreFragment();
            mToolbar.inflateMenu(R.menu.menu_fragment_explore);
        } else {
            throw new IllegalStateException("Invalid section");
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                startActivity(new Intent(this, SearchActivity.class));
                break;
        }
        return false;
    }

    /***********************************************************************************************
     ************************************ SQLite cache *********************************************
     **********************************************************************************************/

    /**
     * This method is called whenever a loader is going to be created. The single loader of this
     * activity should look into the application cache to load the current item profile into the
     * navigation drawer header.
     * @param id of the loader that is going to be created.
     * @param args bundle of arguments passed during the creation.
     * @return a cursor loader that will look into the database asynchronously.
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        UserService userService = PeekMeServer.getUserService();
        long userId = userService.getCurrentUserId();
        userService.getUserProfileAsync(userId, this);
        String[] projections = new String[] {
                Schema.User.USER_ID,
                Schema.User.USER_USERNAME,
                Schema.User.USER_EMAIL,
                Schema.Picture.PICTURE_URL
        };
        Uri uri = ContentUris.withAppendedId(CacheDataProvider.USER_URI, userId);
        return new CursorLoader(this, uri, projections, null, null, null);
    }

    /**
     * This method is called whenever a loader finish to do his job and return the result as cursor.
     * The account header of the navigation drawer is cleared and then repopulated with a fresh
     * instance of the current user if the cursor is not empty (or null).
     * @param loader that has just finished the hard work.
     * @param cursor that the loader has returned as result.
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mCursor = cursor;
        mAccountHeader.clear();
        if (mCursor != null && mCursor.moveToFirst()) {
            ProfileDrawerItem profileDrawerItem = new ProfileDrawerItem()
                    .withIdentifier(mCursor.getLong(mCursor.getColumnIndex(Schema.User.USER_ID)))
                    .withName(mCursor.getString(mCursor.getColumnIndex(Schema.User.USER_USERNAME)))
                    .withEmail(mCursor.getString(mCursor.getColumnIndex(Schema.User.USER_EMAIL)));
            String pictureFile = mCursor.getString(mCursor.getColumnIndex(Schema.Picture.PICTURE_URL));
            if (TextUtils.isEmpty(pictureFile)) {
                profileDrawerItem.withIcon(R.mipmap.ic_launcher);
            } else {
                profileDrawerItem.withIcon(pictureFile);
            }
            mAccountHeader.addProfiles(profileDrawerItem);
        }
    }

    /**
     * Called when a previously created loader is being reset, and thus
     * making its data unavailable.  The application should at this point
     * remove any references it has to the Loader's data.
     * @param loader The Loader that is being reset.
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAccountHeader.clear();
        if (mCursor != null) {
            if (!mCursor.isClosed()) {
                mCursor.close();
                mCursor = null;
            }
        }
    }

    /***********************************************************************************************
     *********************************** Server callback *******************************************
     **********************************************************************************************/

    /**
     * Api callback called whenever a user update comes from server.
     * @param user instance of the user retrieved from server.
     * @param e if not null it contains the error encountered.
     */
    @Override
    public void onRequestDone(User user, ServerException e) {
        if (e != null) {
            // TODO handle error, maybe show a connection error but pay attention: if you handle
            // TODO connection errors in every fragment when loading data, here you should not handle
            // TODO it or the user will be notified multiple times on the same error.
        } else {
            Uri uri = ContentUris.withAppendedId(CacheDataProvider.USER_URI, user.getId());
            getContentResolver().notifyChange(uri, null);
        }
    }
}