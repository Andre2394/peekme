package com.polimi.peekme.ui.activity;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.polimi.peekme.R;
import com.polimi.peekme.api.PeekMeServer;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.Metadata;
import com.polimi.peekme.api.service.SearchService;
import com.polimi.peekme.storage.CacheDataProvider;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.ui.adapter.BaseUserAdapter;

public class SearchActivity extends AppCompatActivity implements BaseUserAdapter.UserClickListener, SwipeRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {

    private static final int LOADER_ID = 1;
    private static final String KEYWORD = "keyword";

    private EditText mSearchView;
    private TextView mHelpTextView;
    private ProgressWheel mProgressWheel;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;

    private BaseUserAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUi();
        setupUi();
    }

    private void initializeUi() {
        setContentView(R.layout.activity_search);
        mSearchView = (EditText) findViewById(R.id.search_view);
        mHelpTextView = (TextView) findViewById(R.id.error_text_view);
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    }

    private void setupUi() {
        mSearchView.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                searchAsync(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

        });
        // TODO setup search view
        mAdapter = new BaseUserAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mProgressWheel.setVisibility(View.GONE);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        mHelpTextView.setText(R.string.help_no_user_found);
    }

    private void searchAsync(String keyword) {
        if (!TextUtils.isEmpty(keyword)) {
            Bundle bundle = new Bundle();
            bundle.putString(KEYWORD, keyword);
            getLoaderManager().restartLoader(LOADER_ID, bundle, this);
        } else {
            mAdapter.swapCursor(null);
            mHelpTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onUserClick(long userId) {
        Intent intent = new Intent(this, ShowUserActivity.class);
        intent.putExtra(ShowUserActivity.USER, userId);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        searchAsync(mSearchView.getText().toString());
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        String keyword = bundle.getString(KEYWORD);
        SearchService searchService = PeekMeServer.getSearchService();
        searchService.findUsersAsync(keyword, new ServerCallback<Metadata>() {

            @Override
            public void onRequestDone(Metadata item, ServerException e) {
                if (e != null) {
                    // TODO show error
                } else {
                    // TODO store metadata for pagination
                    getContentResolver().notifyChange(CacheDataProvider.SEARCH_USER_URI, null);
                }
            }

        });
        String[] projections = new String[] {
                Schema.User.USER_ID,
                Schema.User.USER_USERNAME,
                Schema.Picture.PICTURE_URL,
                Schema.Picture.PICTURE_MIME_TYPE
        };
        Uri uri = CacheDataProvider.SEARCH_USER_URI
                .buildUpon()
                .appendQueryParameter(CacheDataProvider.KEYWORD_PARAMETER, keyword)
                .build();
        return new CursorLoader(this, uri, projections, null, null, Schema.User.USER_USERNAME + " ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
        mProgressWheel.setVisibility(View.GONE);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        mHelpTextView.setVisibility(mAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }
}