package com.polimi.peekme.ui.activity;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.polimi.peekme.R;
import com.polimi.peekme.api.PeekMeServer;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.Comment;
import com.polimi.peekme.api.model.Metadata;
import com.polimi.peekme.api.model.Post;
import com.polimi.peekme.api.model.User;
import com.polimi.peekme.api.service.PostService;
import com.polimi.peekme.api.service.UserService;
import com.polimi.peekme.storage.CacheDataProvider;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.storage.Thumbnail;
import com.polimi.peekme.ui.TextManager;
import com.polimi.peekme.ui.adapter.CommentAdapter;
import com.polimi.peekme.ui.decorator.IconListDecorator;

import java.util.Locale;

public class ShowPostActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, TextManager.TagClickListener, CommentAdapter.CommentClickListener {

    public static final String POST = "post";

    private static final int LOADER_POST = 1;
    private static final int LOADER_COMMENT_LIST = 2;

    private ImageView mImageView;
    private ImageView mUserIcon;
    private TextView mUsername;
    private TextView mDescription;
    private TextView mRating;
    private TextView mComment;
    private TextView mTotal;
    private EditText mFieldComment;
    private ImageButton mSendComment;
    private RecyclerView mRecyclerView;

    private CommentAdapter mAdapter;

    private View mRatingLayout;
    private View mRatingListLayout;

    private long mPostId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUi();
        setupUi();
        loadUi(getIntent());
    }

    private void initializeUi() {
        setStatusBarColorCompat(Color.TRANSPARENT);
        setContentView(R.layout.activity_show_post);
        mImageView = (ImageView) findViewById(R.id.post_media);
        mUserIcon = (ImageView) findViewById(R.id.post_author_icon);
        mUsername = (TextView) findViewById(R.id.post_author_name);
        mDescription = (TextView) findViewById(R.id.post_description);
        mRating = (TextView) findViewById(R.id.post_rating);
        mComment = (TextView) findViewById(R.id.post_comment);
        mTotal = (TextView) findViewById(R.id.post_total);
        mRatingLayout = findViewById(R.id.layout_rating);
        mRatingListLayout = findViewById(R.id.layout_rating_list);
        mFieldComment = (EditText) findViewById(R.id.field_comment);
        mSendComment = (ImageButton) findViewById(R.id.button_send_comment);
        mRecyclerView = (RecyclerView) findViewById(R.id.post_comment_list);
    }

    private void setStatusBarColorCompat(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(color);
        }
    }

    private void setupUi() {
        mRatingLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                showPostRater();
            }

        });
        mRatingListLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                /*
                Intent intent = new Intent(ShowPostActivity.this, ShowPostRatingActivity.class);
                intent.putExtra(ShowPostRatingActivity.POST, mPostId);
                startActivity(intent);*/
            }

        });
        mSendComment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String text = mFieldComment.getText().toString();
                if (!TextUtils.isEmpty(text)) {
                    mFieldComment.getText().clear();
                    PostService postService = PeekMeServer.getPostService();
                    postService.addCommentAsync(mPostId, text, new ServerCallback<Comment>() {

                        @Override
                        public void onRequestDone(Comment comment, ServerException e) {
                            if (e != null) {
                                // TODO show error
                            } else {
                                Uri uri = Uri.withAppendedPath(CacheDataProvider.POST_URI, String.valueOf(mPostId) + "/comments");
                                System.out.println("Change on: " + uri);
                                getContentResolver().notifyChange(uri, null);
                            }
                        }

                    });
                }
            }

        });
        mAdapter = new CommentAdapter(this, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new IconListDecorator(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void loadUi(Intent intent) {
        mPostId = intent.getLongExtra(POST, 0L);
        System.out.println("Showing post id: " + mPostId);
        PostService postService = PeekMeServer.getPostService();
        postService.getPostAsync(mPostId, new ServerCallback<Post>() {

            @Override
            public void onRequestDone(Post item, ServerException e) {
                if (e != null) {
                    // TODO show error
                } else {
                    Uri uri = ContentUris.withAppendedId(CacheDataProvider.POST_URI, mPostId);
                    getContentResolver().notifyChange(uri, null);
                }
            }

        });
        postService.downloadLatestPostCommentsAsync(mPostId, new ServerCallback<Metadata>() {

            @Override
            public void onRequestDone(Metadata item, ServerException e) {
                if (e != null) {
                    // TODO show error
                } else {
                    // TODO store metadata for pagination
                    Uri uri = Uri.withAppendedPath(CacheDataProvider.POST_URI, String.valueOf(mPostId) + "/comments");
                    getContentResolver().notifyChange(uri, null);
                }
            }

        });
        getLoaderManager().initLoader(LOADER_POST, null, this);
        getLoaderManager().initLoader(LOADER_COMMENT_LIST, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        switch (id) {
            case LOADER_POST:
                return getPostBodyLoader();
            case LOADER_COMMENT_LIST:
                return getPostCommentList();
        }
        return null;
    }

    private Loader<Cursor> getPostBodyLoader() {
        String[] projections = new String[] {
                Schema.Post.POST_DESCRIPTION,
                Schema.Post.POST_MEDIA_URL,
                Schema.Post.POST_MEDIA_MIME_TYPE,
                Schema.Post.POST_CREATED_AT,
                Schema.Post.POST_RATING_TOTAL,
                Schema.Post.POST_RATING_COUNT,
                Schema.Post.POST_COMMENT_COUNT,
                Schema.User.USER_ID,
                Schema.User.USER_USERNAME,
                Schema.Picture.PICTURE_URL,
                Schema.Picture.PICTURE_MIME_TYPE
        };
        Uri uri = ContentUris.withAppendedId(CacheDataProvider.POST_URI, mPostId);
        return new CursorLoader(this, uri, projections, null, null, null);
    }

    private Loader<Cursor> getPostCommentList() {
        String[] projections = new String[] {
                Schema.Comment.COMMENT_ID,
                Schema.Comment.COMMENT_TEXT,
                Schema.Comment.COMMENT_CREATED_AT,
                Schema.User.USER_ID,
                Schema.User.USER_USERNAME,
                Schema.Picture.PICTURE_URL,
                Schema.Picture.PICTURE_MIME_TYPE
        };
        Uri uri = Uri.withAppendedPath(CacheDataProvider.POST_URI, String.valueOf(mPostId) + "/comments");
        return new CursorLoader(this, uri, projections, null, null, Schema.Comment.COMMENT_CREATED_AT + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case LOADER_POST:
                loadPost(cursor);
                break;
            case LOADER_COMMENT_LIST:
                mAdapter.swapCursor(cursor);
                break;
        }
    }

    private void loadPost(Cursor cursor) {
        if (cursor.moveToFirst()) {
            mUsername.setText(cursor.getString(cursor.getColumnIndex(Schema.User.USER_USERNAME)));
            String description = cursor.getString(cursor.getColumnIndex(Schema.Post.POST_DESCRIPTION));
            TextManager.applyHashTagRecognizer(mDescription, description, this);
            int ratingTotal = cursor.getInt(cursor.getColumnIndex(Schema.Post.POST_RATING_TOTAL));
            int ratingCount = cursor.getInt(cursor.getColumnIndex(Schema.Post.POST_RATING_COUNT));
            int commentCount = cursor.getInt(cursor.getColumnIndex(Schema.Post.POST_COMMENT_COUNT));
            float rating = (float) ratingTotal / ratingCount;
            mComment.setText(String.valueOf(commentCount));
            mTotal.setText(String.valueOf(ratingCount));
            mRating.setText(String.format(Locale.ENGLISH, "%.2f", rating));

            String pictureUrl = cursor.getString(cursor.getColumnIndex(Schema.Picture.PICTURE_URL));
            String mediaMimeType = cursor.getString(cursor.getColumnIndex(Schema.Post.POST_MEDIA_MIME_TYPE));

            if (pictureUrl != null) {
                Glide.with(this)
                        .load(pictureUrl)
                        .centerCrop()
                        .into(mUserIcon);
            } else {
                Glide.with(this)
                        .load(R.mipmap.ic_launcher)
                        .centerCrop()
                        .into(mUserIcon);
            }
            if (Thumbnail.isGif(mediaMimeType)) {
                Glide.with(this)
                        .load(cursor.getString(cursor.getColumnIndex(Schema.Post.POST_MEDIA_URL)))
                        .asGif()
                        .into(mImageView);
            } else {
                Glide.with(this)
                        .load(cursor.getString(cursor.getColumnIndex(Schema.Post.POST_MEDIA_URL)))
                        .centerCrop()
                        .into(mImageView);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case LOADER_COMMENT_LIST:
                mAdapter.swapCursor(null);
                break;
        }
    }

    public void showPostRater() {
        final NumberPicker picker = new NumberPicker(this);
        picker.setMaxValue(10);
        picker.setMinValue(1);
        new MaterialDialog.Builder(this)
                .title(R.string.dialog_rate_title)
                .customView(picker, false)
                .positiveText(android.R.string.ok)
                .negativeText(android.R.string.cancel)
                .neutralText(R.string.action_remove)
                .onAny(new MaterialDialog.SingleButtonCallback() {

                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        PostService postService = PeekMeServer.getPostService();
                        switch (which) {
                            case POSITIVE:
                                postService.ratePostAsync(mPostId, picker.getValue(), mRateCallback);
                                break;
                            case NEUTRAL:
                                postService.unRatePostAsync(mPostId, mRateCallback);
                                break;
                        }
                    }

                })
                .show();
    }

    private ServerCallback<Post> mRateCallback = new ServerCallback<Post>() {

        @Override
        public void onRequestDone(Post post, ServerException e) {
            if (e != null) {
                // TODO show error
            } else {
                Uri uri = ContentUris.withAppendedId(CacheDataProvider.POST_URI, post.getId());
                getContentResolver().notifyChange(uri, null);
            }
        }

    };

    @Override
    public void onHashTagClick(String hashTag) {
        Intent intent = new Intent(this, ShowTagActivity.class);
        intent.putExtra(ShowTagActivity.TAG, hashTag);
        startActivity(intent);
    }

    @Override
    public void onUserTagClick(String userTag) {
        UserService userService = PeekMeServer.getUserService();
        userService.getUserProfileAsync(userTag, new ServerCallback<User>() {

            @Override
            public void onRequestDone(User user, ServerException e) {
                if (e != null) {
                    // TODO maybe the user not exists
                } else {
                    Intent intent = new Intent(ShowPostActivity.this, ShowUserActivity.class);
                    intent.putExtra(ShowUserActivity.USER, user.getId());
                    startActivity(intent);
                }
            }

        });
    }

    @Override
    public void onUserClick(long userId) {
        Intent intent = new Intent(this, ShowUserActivity.class);
        intent.putExtra(ShowUserActivity.USER, userId);
        startActivity(intent);
    }

    @Override
    public void onCommentClick(long userId, final long commentId) {
        UserService userService = PeekMeServer.getUserService();
        if (userId == userService.getCurrentUserId()) {
            // comment clicked belongs to the current user
            new MaterialDialog.Builder(this)
                    .title(R.string.dialog_comment_action_title)
                    .items(R.array.dialog_current_user_actions)
                    .itemsCallback(new MaterialDialog.ListCallback() {

                        @Override
                        public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                            switch (position) {
                                case 1:
                                    PostService postService = PeekMeServer.getPostService();
                                    postService.deleteCommentAsync(mPostId, commentId, new ServerCallback<Void>() {

                                        @Override
                                        public void onRequestDone(Void item, ServerException e) {
                                            if (e != null) {
                                                // TODO print error
                                            } else {
                                                Uri uri = ContentUris.withAppendedId(CacheDataProvider.POST_URI, mPostId);
                                                getContentResolver().notifyChange(uri, null);
                                            }
                                        }

                                    });
                                    break;
                            }
                        }

                    })
                    .show();
        } else {
            // comment clicked belongs to another user
            new MaterialDialog.Builder(this)
                    .title(R.string.dialog_comment_action_title)
                    .items(R.array.dialog_other_user_actions)
                    .itemsCallback(new MaterialDialog.ListCallback() {

                        @Override
                        public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {

                        }

                    })
                    .show();
        }
    }
}