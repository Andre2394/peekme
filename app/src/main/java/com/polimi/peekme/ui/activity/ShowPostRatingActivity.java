package com.polimi.peekme.ui.activity;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.polimi.peekme.R;
import com.polimi.peekme.api.PeekMeServer;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.Metadata;
import com.polimi.peekme.api.service.PostService;
import com.polimi.peekme.storage.CacheDataProvider;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.ui.adapter.RatingAdapter;

public class ShowPostRatingActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {

    public static final String POST = "post";
    private static final int LOADER_POST_RATINGS = 1;

    private Toolbar mToolbar;
    private TextView mHelpTextView;
    private ProgressWheel mProgressWheel;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;

    private RatingAdapter mAdapter;

    private long mPostId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUi();
        setupUi();
        loadUi(getIntent());
    }

    private void initializeUi() {
        setContentView(R.layout.activity_show_post_rating);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mHelpTextView = (TextView) findViewById(R.id.error_text_view);
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    }

    private void setupUi() {
        mToolbar.setTitle(R.string.string_ratings);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                finish();
            }

        });
        mAdapter = new RatingAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mProgressWheel.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        mHelpTextView.setVisibility(View.GONE);
        mHelpTextView.setText(R.string.help_no_rating_found);
    }

    private void loadUi(Intent intent) {
        mPostId = intent.getLongExtra(POST, 0L);
        downloadLatestPostRatings();
        getLoaderManager().initLoader(LOADER_POST_RATINGS, null, this);
    }

    private void downloadLatestPostRatings() {
        /*
        PostService postService = PeekMeServer.getPostService();
        postService.downloadLatestPostRatingsAsync(mPostId, new ServerCallback<Metadata>() {

            @Override
            public void onRequestDone(Metadata item, ServerException e) {
                if (e != null) {
                    // TODO show error
                } else {
                    // TODO store metadata for pagination
                    Uri uri = Uri.withAppendedPath(CacheDataProvider.POST_URI, String.valueOf(mPostId) + "/ratings");
                    getContentResolver().notifyChange(uri, null);
                }
            }

        });*/
    }

    @Override
    public void onRefresh() {
        downloadLatestPostRatings();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        String[] projections = new String[] {
                Schema.Rating.RATING_ID,
                Schema.Rating.RATING_VALUE,
                Schema.User.USER_ID,
                Schema.User.USER_USERNAME,
                Schema.Picture.PICTURE_URL,
                Schema.Picture.PICTURE_MIME_TYPE
        };
        String pathSegment = String.valueOf(mPostId) + "/ratings";
        Uri uri = Uri.withAppendedPath(CacheDataProvider.POST_URI, pathSegment);
        return new CursorLoader(this, uri, projections, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
        mProgressWheel.setVisibility(View.GONE);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        mHelpTextView.setVisibility(mAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }
}