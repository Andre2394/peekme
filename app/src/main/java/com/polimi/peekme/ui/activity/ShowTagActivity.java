package com.polimi.peekme.ui.activity;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.polimi.peekme.R;
import com.polimi.peekme.api.PeekMeServer;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.Metadata;
import com.polimi.peekme.api.service.PostService;
import com.polimi.peekme.storage.CacheDataProvider;
import com.polimi.peekme.storage.PaginationHelper;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.ui.adapter.UserPostAdapter;

/**
 * Created by andre on 28/12/2016.
 */
public class ShowTagActivity extends AppCompatActivity implements UserPostAdapter.PostClickListener, LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener, PaginationHelper.LoaderCallback, ServerCallback<Metadata> {

    public static final String TAG = "tag";
    private static final String POST_PAGINATION_STATE = "ShowTagActivity:PostPaginationHelper";
    private static final int LOADER_POST = 1;

    private Toolbar mToolbar;
    private TextView mHelpTextView;
    private ProgressWheel mProgressWheel;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;

    private UserPostAdapter mAdapter;

    private String mTag;

    private PaginationHelper mPaginationHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUi();
        setupUi(savedInstanceState);
        loadUi(getIntent());
    }

    private void initializeUi() {
        setContentView(R.layout.activity_show_post_rating);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mHelpTextView = (TextView) findViewById(R.id.error_text_view);
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    }

    private void setupUi(Bundle savedInstanceState) {
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                finish();
            }

        });
        mAdapter = new UserPostAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mProgressWheel.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        mHelpTextView.setVisibility(View.GONE);
        mHelpTextView.setText(R.string.help_no_post_found);
        Bundle paginationState = savedInstanceState != null ? savedInstanceState.getBundle(POST_PAGINATION_STATE) : null;
        mPaginationHelper = new PaginationHelper(mRecyclerView, paginationState, this);
    }

    private void loadUi(Intent intent) {
        mTag = intent.getStringExtra(TAG);
        mToolbar.setTitle("#" + mTag);
        getLoaderManager().initLoader(LOADER_POST, null, this);
    }

    @Override
    public void onPostClick(long postId) {
        Intent intent = new Intent(this, ShowPostActivity.class);
        intent.putExtra(ShowPostActivity.POST, postId);
        startActivity(intent);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        PostService postService = PeekMeServer.getPostService();
        postService.downloadTagPostsAsync(mTag, this);
        String[] projections = new String[] {
                Schema.Post.POST_ID,
                Schema.Post.POST_MEDIA_URL,
                Schema.Post.POST_MEDIA_MIME_TYPE
        };
        String sortOrder = Schema.Post.POST_CREATED_AT + " DESC";
        Uri uri = Uri.withAppendedPath(CacheDataProvider.TAG_POST_URI, mTag);
        return new CursorLoader(this, uri, projections, null, null, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.changeCursor(cursor);
        mProgressWheel.setVisibility(View.GONE);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        mHelpTextView.setVisibility(mAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }

    @Override
    public void onRefresh() {
        mPaginationHelper.onRefresh();
        // TODO restart
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(POST_PAGINATION_STATE, mPaginationHelper.getSavedState());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPaginationHelper.release();
    }

    @Override
    public void onLoadMore(long lastId) {
        PostService postService = PeekMeServer.getPostService();
        postService.downloadTagPostsAsync(mTag, lastId, this);
    }

    @Override
    public void onRequestDone(Metadata metadata, ServerException e) {
        if (e != null) {
            // TODO show error
        } else {
            mPaginationHelper.onNewItems(metadata);
            Uri uri = Uri.withAppendedPath(CacheDataProvider.TAG_POST_URI, mTag);
            getContentResolver().notifyChange(uri, null);
        }
    }
}