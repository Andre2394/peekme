package com.polimi.peekme.ui.activity;

import android.app.LoaderManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.polimi.peekme.R;
import com.polimi.peekme.api.PeekMeServer;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.Metadata;
import com.polimi.peekme.api.model.Picture;
import com.polimi.peekme.api.model.Post;
import com.polimi.peekme.api.model.User;
import com.polimi.peekme.api.service.PostService;
import com.polimi.peekme.api.service.UserService;
import com.polimi.peekme.storage.CacheDataProvider;
import com.polimi.peekme.storage.PaginationHelper;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.storage.Thumbnail;
import com.polimi.peekme.ui.adapter.UserPostAdapter;

/**
 * Created by andre on 14/10/2016.
 */
public class ShowUserActivity extends AppCompatActivity implements UserPostAdapter.PostClickListener, LoaderManager.LoaderCallbacks<Cursor>,PaginationHelper.LoaderCallback {

    private static final String POST_PAGINATION_STATE = "ShowUserActivity:PostPaginationHelper";

    public static final String USER = "user";

    private static final int REQUEST_SELECT_USER_PICTURE = 1;

    private static final int LOADER_USER_PROFILE = 1;
    private static final int LOADER_USER_POSTS = 2;

    private Toolbar mToolbar;
    private TextView mUsername;
    private ImageView mImageView;
    private Button mHeaderButton;
    private TextView mFollowerTextView;
    private TextView mFollowedTextView;
    private RecyclerView mRecyclerView;

    private long mUserId;
    private boolean mIsCurrentUser;
    private boolean mFollowedByYou;
    private UserPostAdapter mPostAdapter;

    private PaginationHelper mPaginationHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUi();
        setupListeners(savedInstanceState);
        loadUi(getIntent());
    }

    private void initializeUi() {
        setContentView(R.layout.activity_show_user);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mUsername = (TextView) findViewById(R.id.text_view_username);
        mImageView = (ImageView) findViewById(R.id.image_view_user);
        mHeaderButton = (Button) findViewById(R.id.header_button);
        mFollowerTextView = (TextView) findViewById(R.id.text_view_follower);
        mFollowedTextView = (TextView) findViewById(R.id.text_view_followed);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    }

    private void setupListeners(Bundle savedInstanceState) {
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                finish();
            }

        });
        mImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                onUserPicturePressed();
            }

        });
        mHeaderButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (mIsCurrentUser) {
                    // TODO start profile editing
                } else {
                    UserService userService = PeekMeServer.getUserService();
                    if (mFollowedByYou) {
                        userService.unfollowUserAsync(mUserId, mFollowUnfollowCallback);
                    } else {
                        userService.followUserAsync(mUserId, mFollowUnfollowCallback);
                    }
                }
            }

        });
        mFollowerTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ShowUserActivity.this, ShowUserListActivity.class);
                intent.putExtra(ShowUserListActivity.MODE, ShowUserListActivity.FOLLOWER_LIST);
                intent.putExtra(ShowUserListActivity.USER, mUserId);
                startActivity(intent);
            }

        });
        mFollowedTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ShowUserActivity.this, ShowUserListActivity.class);
                intent.putExtra(ShowUserListActivity.MODE, ShowUserListActivity.FOLLOWED_LIST);
                intent.putExtra(ShowUserListActivity.USER, mUserId);
                startActivity(intent);
            }

        });
        mPostAdapter = new UserPostAdapter(this);
        mRecyclerView.setAdapter(mPostAdapter);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        Bundle paginationState = savedInstanceState != null ? savedInstanceState.getBundle(POST_PAGINATION_STATE) : null;
        mPaginationHelper = new PaginationHelper(mRecyclerView, paginationState, this);
    }

    private void loadUi(Intent intent) {
        mUserId = intent.getLongExtra(USER, 0);
        UserService userService = PeekMeServer.getUserService();
        mIsCurrentUser = userService.getCurrentUserId() == mUserId;
        getLoaderManager().initLoader(LOADER_USER_PROFILE, null, this);
        getLoaderManager().initLoader(LOADER_USER_POSTS, null, this);
    }

    private void onUserPicturePressed() {
        if (mIsCurrentUser) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.picker_select_picture)), REQUEST_SELECT_USER_PICTURE);
        } else {
            // TODO start slideshow with all user's pictures. now it will just open gallery to upload a new picture
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_SELECT_USER_PICTURE) {
            if (resultCode == RESULT_OK) {
                Uri uri = intent.getData();
                if (uri != null) {
                    UserService userService = PeekMeServer.getUserService();
                    userService.uploadUserPictureAsync(uri, new ServerCallback<Picture>() {

                        @Override
                        public void onRequestDone(Picture picture, ServerException e) {
                            if (e != null) {
                                // TODO show upload error
                            } else {
                                UserService userService = PeekMeServer.getUserService();
                                userService.updateUserPictureAsync(picture.getId(), new ServerCallback<User>() {

                                    @Override
                                    public void onRequestDone(User user, ServerException e) {
                                        if (e != null) {
                                            // TODO show error updating current user
                                        } else {
                                            Uri uri = ContentUris.withAppendedId(CacheDataProvider.USER_URI, user.getId());
                                            getContentResolver().notifyChange(uri, null);
                                        }
                                    }

                                });
                            }
                        }

                    });
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, intent);
        }
    }

    @Override
    public void onPostClick(long postId) {
        Intent intent = new Intent(this, ShowPostActivity.class);
        intent.putExtra(ShowPostActivity.POST, postId);
        startActivity(intent);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        switch (id) {
            case LOADER_USER_PROFILE:
                return getUserProfileLoader();
            case LOADER_USER_POSTS:
                return getUserPostsLoader();
        }
        return null;
    }

    private Loader<Cursor> getUserProfileLoader() {
        UserService userService = PeekMeServer.getUserService();
        userService.getUserProfileAsync(mUserId, new ServerCallback<User>() {

            @Override
            public void onRequestDone(User user, ServerException e) {
                if (e != null) {
                    // TODO print error
                } else {
                    Uri uri = ContentUris.withAppendedId(CacheDataProvider.USER_URI, user.getId());
                    getContentResolver().notifyChange(uri, null);
                }
            }

        });
        String[] projections = new String[] {
                Schema.User.USER_ID,
                Schema.User.USER_USERNAME,
                Schema.User.USER_PICTURE,
                Schema.Picture.PICTURE_URL,
                Schema.User.USER_FOLLOWER_COUNT,
                Schema.User.USER_FOLLOWED_COUNT,
                Schema.User.USER_FOLLOWED_BY_YOU
        };
        Uri uri = ContentUris.withAppendedId(CacheDataProvider.USER_URI, mUserId);
        return new CursorLoader(this, uri, projections, null, null, null);
    }

    private Loader<Cursor> getUserPostsLoader() {
        UserService userService = PeekMeServer.getUserService();
        userService.downloadLatestUserPostsAsync(mUserId, mUserPostsCallback);
        String[] projections = new String[] {
                Schema.Post.POST_ID,
                Schema.Post.POST_MEDIA_URL,
                Schema.Post.POST_MEDIA_MIME_TYPE
        };
        Uri uri = Uri.withAppendedPath(CacheDataProvider.USER_URI, String.valueOf(mUserId) + "/posts");
        return new CursorLoader(this, uri, projections, null, null, Schema.Post.POST_CREATED_AT + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case LOADER_USER_PROFILE:
                loadProfile(cursor);
                break;
            case LOADER_USER_POSTS:
                mPostAdapter.swapCursor(cursor);
                break;
        }
    }

    private void loadProfile(Cursor cursor) {
        if (cursor.moveToFirst()) {
            mUsername.setText(cursor.getString(cursor.getColumnIndex(Schema.User.USER_USERNAME)));
            String pictureUrl = cursor.getString(cursor.getColumnIndex(Schema.Picture.PICTURE_URL));
            if (pictureUrl != null) {
                Glide.with(this)
                        .load(pictureUrl)
                        .centerCrop()
                        .into(mImageView);
            } else {
                Glide.with(this)
                        .load(R.mipmap.ic_launcher)
                        .centerCrop()
                        .into(mImageView);
            }
            mFollowedTextView.setText(cursor.getString(cursor.getColumnIndex(Schema.User.USER_FOLLOWED_COUNT)));
            mFollowerTextView.setText(cursor.getString(cursor.getColumnIndex(Schema.User.USER_FOLLOWER_COUNT)));
            mFollowedByYou = cursor.getInt(cursor.getColumnIndex(Schema.User.USER_FOLLOWED_BY_YOU)) == 1;
        }
        if (mIsCurrentUser) {
            mHeaderButton.setText(R.string.button_edit_profile);
        } else {
            mHeaderButton.setText(mFollowedByYou ? R.string.action_unfollow : R.string.action_follow);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case LOADER_USER_POSTS:
                mPostAdapter.swapCursor(null);
                break;
        }
    }

    private ServerCallback<Metadata> mUserPostsCallback = new ServerCallback<Metadata>() {

        @Override
        public void onRequestDone(Metadata metadata, ServerException e) {
            if (e != null) {
                // TODO print error
            } else {
                mPaginationHelper.onNewItems(metadata);
                Uri uri = Uri.withAppendedPath(CacheDataProvider.USER_URI, String.valueOf(mUserId) + "/posts");
                getContentResolver().notifyChange(uri, null);
            }
        }

    };

    private ServerCallback<User> mFollowUnfollowCallback = new ServerCallback<User>() {

        @Override
        public void onRequestDone(User user, ServerException e) {
            if (e != null) {

            } else {
                long currentUser = PeekMeServer.getUserService().getCurrentUserId();
                ContentResolver contentResolver = getContentResolver();
                contentResolver.notifyChange(Uri.withAppendedPath(CacheDataProvider.USER_URI, String.valueOf(currentUser) + "/followed"), null);
                contentResolver.notifyChange(Uri.withAppendedPath(CacheDataProvider.USER_URI, String.valueOf(user.getId()) + "/follower"), null);
                contentResolver.notifyChange(CacheDataProvider.POST_URI, null);
            }
        }

    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(POST_PAGINATION_STATE, mPaginationHelper.getSavedState());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPaginationHelper.release();
    }

    @Override
    public void onLoadMore(long lastId) {
        UserService userService = PeekMeServer.getUserService();
        userService.downloadLatestUserPostsAsync(mUserId, lastId, mUserPostsCallback);
    }
}