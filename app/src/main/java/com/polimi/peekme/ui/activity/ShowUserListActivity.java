package com.polimi.peekme.ui.activity;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.polimi.peekme.R;
import com.polimi.peekme.api.PeekMeServer;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.Metadata;
import com.polimi.peekme.api.service.UserService;
import com.polimi.peekme.storage.CacheDataProvider;
import com.polimi.peekme.storage.PaginationHelper;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.ui.adapter.BaseUserAdapter;

/**
 * Created by andre on 16/10/2016.
 */
public class ShowUserListActivity extends AppCompatActivity implements BaseUserAdapter.UserClickListener, SwipeRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor>,PaginationHelper.LoaderCallback {

    public static final String MODE = "mode";
    public static final String USER = "user";

    private static final String POST_PAGINATION_STATE = "ShowUserListActivity:UserPaginationHelper";

    public static final int FOLLOWER_LIST = 1;
    public static final int FOLLOWED_LIST = 2;

    public static final int LOADER_USER_FOLLOWER = 1;
    public static final int LOADER_USER_FOLLOWED = 2;

    private Toolbar mToolbar;
    private TextView mHelpTextView;
    private ProgressWheel mProgressWheel;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;

    private BaseUserAdapter mAdapter;

    private long mUserId;
    private int mMode;

    private PaginationHelper mPaginationHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUi();
        setupUi(savedInstanceState);
        loadUi(getIntent());
    }

    private void initializeUi() {
        setContentView(R.layout.activity_show_user_list);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mHelpTextView = (TextView) findViewById(R.id.error_text_view);
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    }

    private void setupUi(Bundle savedInstanceState) {
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                finish();
            }

        });
        mAdapter = new BaseUserAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mProgressWheel.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        mHelpTextView.setVisibility(View.GONE);
        mHelpTextView.setText(R.string.help_no_user_found);
        Bundle paginationState = savedInstanceState != null ? savedInstanceState.getBundle(POST_PAGINATION_STATE) : null;
        mPaginationHelper = new PaginationHelper(mRecyclerView, paginationState, this);
    }

    private void loadUi(Intent intent) {
        mMode = intent.getIntExtra(MODE, 0);
        mUserId = intent.getLongExtra(USER, 0L);
        downloadUserList();
        LoaderManager loaderManager = getSupportLoaderManager();
        switch (mMode) {
            case FOLLOWER_LIST:
                mToolbar.setTitle(R.string.string_follower);
                loaderManager.initLoader(LOADER_USER_FOLLOWER, null, this);
                break;
            case FOLLOWED_LIST:
                mToolbar.setTitle(R.string.string_followed);
                loaderManager.initLoader(LOADER_USER_FOLLOWED, null, this);
                break;
            default:
                throw new IllegalArgumentException("Activity mode not valid");
        }
    }

    private void downloadUserList() {
        UserService userService = PeekMeServer.getUserService();
        switch (mMode) {
            case FOLLOWER_LIST:
                userService.downloadUserFollowerListAsync(mUserId, mFollowerCallback);
                break;
            case FOLLOWED_LIST:
                userService.downloadUserFollowedListAsync(mUserId, mFollowedCallback);
                break;
        }
    }

    @Override
    public void onRefresh() {
        mPaginationHelper.onRefresh();
        downloadUserList();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(POST_PAGINATION_STATE, mPaginationHelper.getSavedState());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPaginationHelper.release();
    }

    @Override
    public void onUserClick(long userId) {
        Intent intent = new Intent(ShowUserListActivity.this, ShowUserActivity.class);
        intent.putExtra(ShowUserActivity.USER, userId);
        startActivity(intent);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projections = new String[] {
                Schema.User.USER_ID,
                Schema.User.USER_USERNAME,
                Schema.Picture.PICTURE_URL,
                Schema.Picture.PICTURE_MIME_TYPE
        };
        String pathSegment;
        switch (id) {
            case LOADER_USER_FOLLOWER:
                pathSegment = String.valueOf(mUserId) + "/follower";
                break;
            case LOADER_USER_FOLLOWED:
                pathSegment = String.valueOf(mUserId) + "/followed";
                break;
            default:
                return null;
        }
        Uri uri = Uri.withAppendedPath(CacheDataProvider.USER_URI, pathSegment);
        return new CursorLoader(this, uri, projections, null, null, Schema.User.USER_USERNAME + " ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
        mProgressWheel.setVisibility(View.GONE);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        mHelpTextView.setVisibility(mAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void onLoadMore(long lastId) {
        UserService userService = PeekMeServer.getUserService();
        switch (mMode) {
            case FOLLOWER_LIST:
                userService.downloadUserFollowerListAsync(mUserId, lastId, mFollowerCallback);
                break;
            case FOLLOWED_LIST:
                userService.downloadUserFollowedListAsync(mUserId, lastId, mFollowedCallback);
                break;
        }
    }

    private ServerCallback<Metadata> mFollowerCallback = new ServerCallback<Metadata>() {

        @Override
        public void onRequestDone(Metadata metadata, ServerException e) {
            if (e != null) {
                // TODO show error
            } else {
                mPaginationHelper.onNewItems(metadata);
                Uri uri = Uri.withAppendedPath(CacheDataProvider.USER_URI, String.valueOf(mUserId) + "/follower");
                getContentResolver().notifyChange(uri, null);
            }
        }

    };

    private ServerCallback<Metadata> mFollowedCallback = new ServerCallback<Metadata>() {

        @Override
        public void onRequestDone(Metadata metadata, ServerException e) {
            if (e != null) {
                // TODO show error
            } else {
                mPaginationHelper.onNewItems(metadata);
                Uri uri = Uri.withAppendedPath(CacheDataProvider.USER_URI, String.valueOf(mUserId) + "/followed");
                getContentResolver().notifyChange(uri, null);
            }
        }

    };
}