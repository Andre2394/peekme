package com.polimi.peekme.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.polimi.peekme.R;
import com.polimi.peekme.service.UploadPostService;

/**
 * Created by andre on 18/10/2016.
 */
public class UploadPostActivity extends AppCompatActivity {

    public static final String MEDIA_URI = "media_uri";

    private Toolbar mToolbar;
    private ImageView mImageView;
    private EditText mDescription;

    private Uri mUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUi();
        setupUi();
        loadUi(getIntent());
    }

    private void initializeUi() {
        setContentView(R.layout.activity_upload_post);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mImageView = (ImageView) findViewById(R.id.image_view);
        mDescription = (EditText) findViewById(R.id.field_description);
    }

    private void setupUi() {
        mToolbar.setTitle(R.string.title_upload_post);
        mToolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                finish();
            }

        });
        mToolbar.inflateMenu(R.menu.menu_upload_post);
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_upload:
                        if (mUri != null) {
                            uploadPost();
                        } else {
                            Toast.makeText(UploadPostActivity.this, "No media selected", Toast.LENGTH_LONG).show();
                        }
                        break;
                }
                return false;
            }

        });
    }

    private void loadUi(Intent intent) {
        mUri = intent.getParcelableExtra(MEDIA_URI);
        Glide.with(this)
                .load(mUri)
                .centerCrop()
                .into(mImageView);
    }

    private void uploadPost() {
        Intent intent = new Intent(UploadPostActivity.this, UploadPostService.class);
        intent.putExtra(UploadPostService.POST_MEDIA, mUri);
        intent.putExtra(UploadPostService.POST_DESCRIPTION, mDescription.getText().toString());
        startService(intent);
        finish();
    }
}