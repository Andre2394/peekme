package com.polimi.peekme.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.polimi.peekme.R;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.storage.Thumbnail;

/**
 * Created by andre on 16/10/2016.
 */
public class BaseUserAdapter extends AbstractCursorAdapter<BaseUserAdapter.ViewHolder> {

    private final UserClickListener mUserClickListener;

    private int mIndexUserId;
    private int mIndexUserName;
    private int mIndexUserPicture;

    public BaseUserAdapter(UserClickListener userClickListener) {
        super(null, Schema.User.USER_ID);
        mUserClickListener = userClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_base_user_item, parent, false));
    }

    @Override
    protected void onLoadColumnIndices(@NonNull Cursor cursor) {
        mIndexUserId = cursor.getColumnIndex(Schema.User.USER_ID);
        mIndexUserName = cursor.getColumnIndex(Schema.User.USER_USERNAME);
        mIndexUserPicture = cursor.getColumnIndex(Schema.Picture.PICTURE_URL);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, Cursor cursor) {
        holder.mUsername.setText(cursor.getString(mIndexUserName));
        String pictureUrl = cursor.getString(mIndexUserPicture);
        if (pictureUrl != null) {
            Glide.with(holder.getContext())
                    .load(pictureUrl)
                    .centerCrop()
                    .into(holder.mIcon);
        } else {
            Glide.with(holder.getContext())
                    .load(R.mipmap.ic_launcher)
                    .centerCrop()
                    .into(holder.mIcon);
        }
    }

    /*package-local*/ class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView mIcon;
        private TextView mUsername;

        public ViewHolder(View itemView) {
            super(itemView);
            mIcon = (ImageView) itemView.findViewById(R.id.user_icon);
            mUsername = (TextView) itemView.findViewById(R.id.user_username);
            itemView.setOnClickListener(this);
        }

        private Context getContext() {
            return itemView.getContext();
        }

        @Override
        public void onClick(View view) {
            if (mUserClickListener != null) {
                long userId = getSafeCursor(getAdapterPosition()).getLong(mIndexUserId);
                mUserClickListener.onUserClick(userId);
            }
        }
    }

    public interface UserClickListener {

        void onUserClick(long userId);
    }
}