package com.polimi.peekme.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.polimi.peekme.R;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.storage.Thumbnail;
import com.polimi.peekme.ui.TextManager;

/**
 * Created by andre on 23/10/2016.
 */
public class CommentAdapter extends AbstractCursorAdapter<CommentAdapter.ViewHolder> {

    private int mIndexUserId;
    private int mIndexUserName;
    private int mIndexUserPicture;
    private int mIndexCommentId;
    private int mIndexCommentText;

    private final TextManager.TagClickListener mCallback;
    private final CommentClickListener mClickListener;

    public CommentAdapter(CommentClickListener clickListener, TextManager.TagClickListener callback) {
        super(null, Schema.Comment.COMMENT_ID);
        mClickListener = clickListener;
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_comment_item, parent, false));
    }

    @Override
    protected void onLoadColumnIndices(@NonNull Cursor cursor) {
        mIndexUserId = cursor.getColumnIndex(Schema.User.USER_ID);
        mIndexUserName = cursor.getColumnIndex(Schema.User.USER_USERNAME);
        mIndexUserPicture = cursor.getColumnIndex(Schema.Picture.PICTURE_URL);
        mIndexCommentId = cursor.getColumnIndex(Schema.Comment.COMMENT_ID);
        mIndexCommentText = cursor.getColumnIndex(Schema.Comment.COMMENT_TEXT);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, Cursor cursor) {
        String username = cursor.getString(mIndexUserName);
        String text = cursor.getString(mIndexCommentText);
        String comment = username + "\n" + text;
        TextManager.applyHashTagRecognizer(holder.mTextView, comment, mCallback);
        String pictureUrl = cursor.getString(mIndexUserPicture);
        if (pictureUrl != null) {
            Glide.with(holder.getContext())
                    .load(pictureUrl)
                    .centerCrop()
                    .into(holder.mUserIcon);
        } else {
            Glide.with(holder.getContext())
                    .load(R.mipmap.ic_launcher)
                    .centerCrop()
                    .into(holder.mUserIcon);
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView mUserIcon;
        private TextView mTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            mUserIcon = (ImageView) itemView.findViewById(R.id.user_icon);
            mTextView = (TextView) itemView.findViewById(R.id.comment_text);
            mUserIcon.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        private Context getContext() {
            return itemView.getContext();
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                Cursor cursor = getSafeCursor(getAdapterPosition());
                long userId = cursor.getLong(mIndexUserId);
                if (view == mUserIcon) {
                    mClickListener.onUserClick(userId);
                } else {
                    long commentId = cursor.getLong(mIndexCommentId);
                    mClickListener.onCommentClick(userId, commentId);
                }
            }
        }
    }

    public interface CommentClickListener {

        void onUserClick(long userId);

        void onCommentClick(long userId, long commentId);
    }
}