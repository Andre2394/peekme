package com.polimi.peekme.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.polimi.peekme.R;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.storage.Thumbnail;
import com.polimi.peekme.ui.TextManager;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

import java.util.Date;
import java.util.Locale;

/**
 * Created by andre on 18/10/2016.
 */
public class PostAdapter extends AbstractCursorAdapter<PostAdapter.ViewHolder> {

    private final PostClickListener mCallback;

    private int mIndexUserId;
    private int mIndexUserName;
    private int mIndexUserPicture;
    private int mIndexPostId;
    private int mIndexPostDescription;
    private int mIndexPostMedia;
    private int mIndexPostMimeType;
    private int mIndexRatingTotal;
    private int mIndexRatingCount;
    private int mIndexCommentCount;
    private int mIndexCreatedAt;

    public PostAdapter(PostClickListener callback) {
        super(null, Schema.Post.POST_ID);
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_post_item, parent, false));
    }

    /*
    private void bindUser(BaseUser user, ViewHolder holder) {
        holder.mAuthorName.setText(user.getUsername());
        if (user.hasPicture()) {
            Picture picture = user.getPicture();
            Glide.with(holder.getContext())
                    .load(picture.getResizedUrl(300, 300))
                    .centerCrop()
                    .into(holder.mAuthorIcon);
        } else {
            Glide.with(holder.getContext())
                    .load(R.mipmap.ic_launcher)
                    .centerCrop()
                    .into(holder.mAuthorIcon);
        }
    }

    private void bindPost(Post post, ViewHolder holder) {
        Media media = post.getMedia();
        holder.mDescription.setText(post.getDescription());
        Glide.with(holder.getContext())
                .load(media.getResizedUrl(1024, 1024))
                .centerCrop()
                .into(holder.mImage);
        int rounded = (int) (post.getRating() * 100);
        holder.mTotal.setText(String.valueOf(post.getRatingCount()));
        holder.mComment.setText(String.valueOf(post.getCommentCount()));
        holder.mRating.setText(String.valueOf((float) rounded / 100));
        holder.mDateTime.setText(DateUtils.getRelativeTimeSpanString(post.getCreatedAt().getTime(),
                System.currentTimeMillis(), 0L, DateUtils.FORMAT_ABBREV_ALL));
    } */

    @Override
    protected void onLoadColumnIndices(@NonNull Cursor cursor) {
        mIndexUserId = cursor.getColumnIndex(Schema.User.USER_ID);
        mIndexUserName = cursor.getColumnIndex(Schema.User.USER_USERNAME);
        mIndexUserPicture = cursor.getColumnIndex(Schema.Picture.PICTURE_URL);
        mIndexPostId = cursor.getColumnIndex(Schema.Post.POST_ID);
        mIndexPostDescription = cursor.getColumnIndex(Schema.Post.POST_DESCRIPTION);
        mIndexPostMedia = cursor.getColumnIndex(Schema.Post.POST_MEDIA_URL);
        mIndexPostMimeType = cursor.getColumnIndex(Schema.Post.POST_MEDIA_MIME_TYPE);
        mIndexRatingTotal = cursor.getColumnIndex(Schema.Post.POST_RATING_TOTAL);
        mIndexRatingCount = cursor.getColumnIndex(Schema.Post.POST_RATING_COUNT);
        mIndexCommentCount = cursor.getColumnIndex(Schema.Post.POST_COMMENT_COUNT);
        mIndexCreatedAt = cursor.getColumnIndex(Schema.Post.POST_CREATED_AT);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, Cursor cursor) {
        holder.mAuthorName.setText(cursor.getString(mIndexUserName));
        String description = cursor.getString(mIndexPostDescription);
        TextManager.applyHashTagRecognizer(holder.mDescription, description, mCallback);
        int ratingTotal = cursor.getInt(mIndexRatingTotal);
        int ratingCount = cursor.getInt(mIndexRatingCount);
        int commentCount = cursor.getInt(mIndexCommentCount);
        float rating = (float) ratingTotal / ratingCount;
        holder.mRating.setText(String.format(Locale.ENGLISH, "%.2f", rating));
        holder.mComment.setText(String.valueOf(commentCount));
        holder.mTotal.setText(String.valueOf(ratingCount));
        holder.mDateTime.setText(getRelativeTimeStamp(cursor.getLong(mIndexCreatedAt)));
        // holder.mDateTime.setText(DateUtils.getRelativeTimeSpanString(cursor.getLong(mIndexCreatedAt), System.currentTimeMillis(), 0L, DateUtils.FORMAT_ABBREV_ALL));

        String pictureUrl = cursor.getString(mIndexUserPicture);
        String mediaMimeType = cursor.getString(mIndexPostMimeType);

        if (pictureUrl != null) {
            Glide.with(holder.getContext())
                    .load(pictureUrl)
                    .centerCrop()
                    .into(holder.mAuthorIcon);
        } else {
            Glide.with(holder.getContext())
                    .load(R.mipmap.ic_launcher)
                    .centerCrop()
                    .into(holder.mAuthorIcon);
        }
        if (Thumbnail.isGif(mediaMimeType)) {
            Glide.with(holder.getContext())
                    .load(cursor.getString(mIndexPostMedia))
                    .asGif()
                    .into(holder.mImage);
        } else {
            Glide.with(holder.getContext())
                    .load(cursor.getString(mIndexPostMedia))
                    .centerCrop()
                    .into(holder.mImage);
        }
    }

    private CharSequence getRelativeTimeStamp(long millis) {
        DateTime dateTime = new DateTime(millis, DateTimeZone.UTC);
        long localMillis = dateTime.getMillis();
        return DateUtils.getRelativeTimeSpanString(localMillis, System.currentTimeMillis(), 0L, DateUtils.FORMAT_ABBREV_ALL);
    }

    /*package-local*/ class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView mAuthorIcon;
        private TextView mAuthorName;
        private ImageView mImage;
        private TextView mDescription;
        private TextView mDateTime;
        private TextView mRating;
        private TextView mComment;
        private TextView mTotal;

        private View mRatingLayout;
        private View mRatingListLayout;
        private View mAuthorLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            mAuthorIcon = (ImageView) itemView.findViewById(R.id.post_author_icon);
            mAuthorName = (TextView) itemView.findViewById(R.id.post_author_name);
            mImage = (ImageView) itemView.findViewById(R.id.post_image);
            mDescription = (TextView) itemView.findViewById(R.id.post_description);
            mDateTime = (TextView) itemView.findViewById(R.id.post_date_time);
            mRating = (TextView) itemView.findViewById(R.id.post_rating);
            mComment = (TextView) itemView.findViewById(R.id.post_comment);
            mTotal = (TextView) itemView.findViewById(R.id.post_total);
            mRatingLayout = itemView.findViewById(R.id.layout_rating);
            mRatingListLayout = itemView.findViewById(R.id.layout_rating_list);
            mAuthorLayout = itemView.findViewById(R.id.post_author_layout);
            itemView.setOnClickListener(this);
            mRatingLayout.setOnClickListener(this);
            mRatingListLayout.setOnClickListener(this);
            mAuthorLayout.setOnClickListener(this);
        }

        private Context getContext() {
            return itemView.getContext();
        }

        @Override
        public void onClick(View view) {
            if (mCallback != null) {
                Cursor cursor = getSafeCursor(getAdapterPosition());
                if (view == mAuthorLayout) {
                    mCallback.onUserClick(cursor.getLong(mIndexUserId));
                } else if (view == mRatingLayout) {
                    mCallback.onRatePost(cursor.getLong(mIndexPostId));
                } else if (view == mRatingListLayout) {
                    mCallback.onRateListClick(cursor.getLong(mIndexPostId));
                } else {
                    mCallback.onPostClick(cursor.getLong(mIndexPostId));
                }
            }
        }
    }

    public interface PostClickListener extends TextManager.TagClickListener {

        void onUserClick(long userId);

        void onPostClick(long postId);

        void onRatePost(long postId);

        void onRateListClick(long postId);
    }
}