package com.polimi.peekme.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.polimi.peekme.R;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.storage.Thumbnail;

/**
 * Created by andre on 23/10/2016.
 */
public class RatingAdapter extends AbstractCursorAdapter<RatingAdapter.ViewHolder> {

    private int mIndexUserId;
    private int mIndexUserName;
    private int mIndexUserPicture;
    private int mIndexRatingValue;

    public RatingAdapter() {
        super(null, Schema.Rating.RATING_ID);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_rating_item, parent, false));
    }

    @Override
    protected void onLoadColumnIndices(@NonNull Cursor cursor) {
        mIndexUserId = cursor.getColumnIndex(Schema.User.USER_ID);
        mIndexUserName = cursor.getColumnIndex(Schema.User.USER_USERNAME);
        mIndexUserPicture = cursor.getColumnIndex(Schema.Picture.PICTURE_URL);
        mIndexRatingValue = cursor.getColumnIndex(Schema.Rating.RATING_VALUE);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, Cursor cursor) {
        holder.mName.setText(cursor.getString(mIndexUserName));
        holder.mRating.setText(cursor.getString(mIndexRatingValue));
        String pictureUrl = cursor.getString(mIndexUserPicture);
        if (pictureUrl != null) {
            Glide.with(holder.getContext())
                    .load(pictureUrl)
                    .centerCrop()
                    .into(holder.mIcon);
        } else {
            Glide.with(holder.getContext())
                    .load(R.mipmap.ic_launcher)
                    .centerCrop()
                    .into(holder.mIcon);
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mIcon;
        private TextView mName;
        private TextView mRating;

        public ViewHolder(View itemView) {
            super(itemView);
            mIcon = (ImageView) itemView.findViewById(R.id.user_icon);
            mName = (TextView) itemView.findViewById(R.id.user_username);
            mRating = (TextView) itemView.findViewById(R.id.user_rating);
        }

        private Context getContext() {
            return itemView.getContext();
        }
    }
}