package com.polimi.peekme.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.polimi.peekme.R;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.storage.Thumbnail;
import com.polimi.peekme.ui.view.SquaredImageView;

/**
 * Created by andre on 15/10/2016.
 */
public class UserPostAdapter extends AbstractCursorAdapter<UserPostAdapter.PostViewHolder> {

    private final PostClickListener mCallback;

    private int mIndexPostMedia;
    private int mIndexPostMimeType;

    public UserPostAdapter(PostClickListener callback) {
        super(null, Schema.Post.POST_ID);
        mCallback = callback;
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PostViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_small_post_item, parent, false));
    }

    @Override
    protected void onLoadColumnIndices(@NonNull Cursor cursor) {
        mIndexPostMedia = cursor.getColumnIndex(Schema.Post.POST_MEDIA_URL);
        mIndexPostMimeType = cursor.getColumnIndex(Schema.Post.POST_MEDIA_MIME_TYPE);
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, Cursor cursor) {
        String mediaUrl = cursor.getString(mIndexPostMedia);
        Glide.with(holder.getContext())
                .load(mediaUrl)
                .centerCrop()
                .into(holder.mImageView);
    }

    /*package-local*/ class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView mImageView;

        public PostViewHolder(View itemView) {
            super(itemView);
            mImageView = (SquaredImageView) itemView;
            mImageView.setOnClickListener(this);
        }

        private Context getContext() {
            return itemView.getContext();
        }

        @Override
        public void onClick(View view) {
            if (mCallback != null) {
                int position = getAdapterPosition();
                long postId = UserPostAdapter.this.getItemId(position);
                mCallback.onPostClick(postId);
            }
        }
    }

    public interface PostClickListener {

        void onPostClick(long postId);
    }
}