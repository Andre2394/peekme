package com.polimi.peekme.ui.decorator;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.polimi.peekme.R;

/**
 * Created by andre on 04/11/2016.
 */
public class IconListDecorator extends RecyclerView.ItemDecoration {

    private Resources mResources;
    private Drawable mDrawable;

    public IconListDecorator(Context context) {
        mResources = context.getResources();
        mDrawable = ContextCompat.getDrawable(context, R.drawable.line_divider);
    }

    @Override
    public void onDrawOver(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
        int leftMargin = mResources.getDimensionPixelSize(R.dimen.adapter_single_item_text_left_margin);
        int rightMargin = mResources.getDimensionPixelSize(R.dimen.adapter_single_item_margin);
        int left = parent.getPaddingLeft() + leftMargin;
        int right = parent.getWidth() - (parent.getPaddingRight() + rightMargin);
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDrawable.getIntrinsicHeight();
            mDrawable.setBounds(left, top, right, bottom);
            mDrawable.draw(canvas);
        }
    }
}