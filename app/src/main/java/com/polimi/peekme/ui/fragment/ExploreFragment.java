package com.polimi.peekme.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.polimi.peekme.R;
import com.polimi.peekme.api.PeekMeServer;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.Metadata;
import com.polimi.peekme.api.model.Post;
import com.polimi.peekme.api.service.ExploreService;
import com.polimi.peekme.api.service.PostService;
import com.polimi.peekme.storage.CacheDataProvider;
import com.polimi.peekme.storage.PaginationHelper;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.ui.activity.ShowPostActivity;
import com.polimi.peekme.ui.adapter.PostAdapter;
import com.polimi.peekme.ui.adapter.UserPostAdapter;

import java.util.List;

/**
 * Created by andre on 20/10/2016.
 */
public class ExploreFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ServerCallback<Metadata>, PaginationHelper.LoaderCallback, LoaderManager.LoaderCallbacks<Cursor>, UserPostAdapter.PostClickListener {

    private static final String POST_PAGINATION_STATE = "ExploreFragment:PostPaginationHelper";

    private static final int LOADER_POST = 1;

    private TextView mHelpTextView;
    private ProgressWheel mProgressWheel;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;

    private UserPostAdapter mAdapter;

    private PaginationHelper mPaginationHelper;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_section_explore, container, false);
        mHelpTextView = (TextView) view.findViewById(R.id.error_text_view);
        mProgressWheel = (ProgressWheel) view.findViewById(R.id.progress_wheel);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mAdapter = new UserPostAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        Bundle paginationState = savedInstanceState != null ? savedInstanceState.getBundle(POST_PAGINATION_STATE) : null;
        mPaginationHelper = new PaginationHelper(mRecyclerView, paginationState, this);
        mProgressWheel.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        mHelpTextView.setVisibility(View.GONE);
        mHelpTextView.setText(R.string.help_no_post_found);
        getLoaderManager().initLoader(LOADER_POST, null, this);
    }

    @Override
    public void onRefresh() {
        mPaginationHelper.onRefresh();
        ExploreService exploreService = PeekMeServer.getExploreService();
        exploreService.downloadLatestPostsAsync(this);
    }

    @Override
    public void onRequestDone(Metadata metadata, ServerException e) {
        if (e != null) {
            // TODO show error message
        } else {
            mPaginationHelper.onNewItems(metadata);
            Activity activity = getActivity();
            if (activity != null) {
                // fragment may be detached when async loader has finished his task!
                activity.getContentResolver().notifyChange(CacheDataProvider.EXPLORE_POST_URI, null);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(POST_PAGINATION_STATE, mPaginationHelper.getSavedState());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPaginationHelper.release();
    }

    @Override
    public void onLoadMore(long lastId) {
        ExploreService exploreService = PeekMeServer.getExploreService();
        exploreService.downloadLatestPostsAsync(lastId, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        ExploreService exploreService = PeekMeServer.getExploreService();
        exploreService.downloadLatestPostsAsync(this);
        String[] projections = new String[] {
                Schema.Post.POST_ID,
                Schema.Post.POST_MEDIA_URL,
                Schema.Post.POST_MEDIA_MIME_TYPE
        };
        String sortOrder = Schema.Post.POST_CREATED_AT + " DESC";
        Uri uri = CacheDataProvider.EXPLORE_POST_URI;
        return new CursorLoader(getContext(), uri, projections, null, null, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.changeCursor(cursor);
        mProgressWheel.setVisibility(View.GONE);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        mHelpTextView.setVisibility(mAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }

    @Override
    public void onPostClick(long postId) {
        Intent intent = new Intent(getActivity(), ShowPostActivity.class);
        intent.putExtra(ShowPostActivity.POST, postId);
        startActivity(intent);
    }
}