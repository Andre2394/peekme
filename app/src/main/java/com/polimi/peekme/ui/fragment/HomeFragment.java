package com.polimi.peekme.ui.fragment;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.polimi.peekme.R;
import com.polimi.peekme.storage.PaginationHelper;
import com.polimi.peekme.api.PeekMeServer;
import com.polimi.peekme.api.ServerCallback;
import com.polimi.peekme.api.ServerException;
import com.polimi.peekme.api.model.Metadata;
import com.polimi.peekme.api.model.Post;
import com.polimi.peekme.api.model.User;
import com.polimi.peekme.api.service.PostService;
import com.polimi.peekme.api.service.UserService;
import com.polimi.peekme.storage.CacheDataProvider;
import com.polimi.peekme.storage.Schema;
import com.polimi.peekme.ui.activity.CameraActivity;
import com.polimi.peekme.ui.activity.EditorActivity;
import com.polimi.peekme.ui.activity.ShowPostActivity;
import com.polimi.peekme.ui.activity.ShowPostRatingActivity;
import com.polimi.peekme.ui.activity.ShowTagActivity;
import com.polimi.peekme.ui.activity.ShowUserActivity;
import com.polimi.peekme.ui.adapter.PostAdapter;

/**
 * Created by andre on 18/10/2016.
 */
public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        PostAdapter.PostClickListener, LoaderManager.LoaderCallbacks<Cursor>, ServerCallback<Metadata>,PaginationHelper.LoaderCallback {

    private static final String POST_PAGINATION_STATE = "HomeFragment:PostPaginationHelper";

    private static final int LOADER_POST = 1;

    private static final int REQUEST_SELECT_MEDIA = 1;
    private static final int REQUEST_TAKE_MEDIA = 2;

    private TextView mHelpTextView;
    private ProgressWheel mProgressWheel;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private FloatingActionMenu mFloatingMenu;
    private FloatingActionButton mFloatingSelectMedia;
    private FloatingActionButton mFloatingTakeMedia;

    private PostAdapter mAdapter;

    private PaginationHelper mPaginationHelper;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_section_home, container, false);
        mHelpTextView = (TextView) view.findViewById(R.id.error_text_view);
        mProgressWheel = (ProgressWheel) view.findViewById(R.id.progress_wheel);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mFloatingMenu = (FloatingActionMenu) view.findViewById(R.id.floating_menu);
        mFloatingSelectMedia = (FloatingActionButton) view.findViewById(R.id.floating_button_select_media);
        mFloatingTakeMedia = (FloatingActionButton) view.findViewById(R.id.floating_button_take_media);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mAdapter = new PostAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mProgressWheel.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        mHelpTextView.setVisibility(View.GONE);
        mHelpTextView.setText(R.string.help_no_post_found);
        mFloatingSelectMedia.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, getString(R.string.picker_select_picture)), REQUEST_SELECT_MEDIA);
                mFloatingMenu.close(true);
            }

        });
        mFloatingTakeMedia.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CameraActivity.class);
                startActivityForResult(intent, REQUEST_TAKE_MEDIA);
                mFloatingMenu.close(true);
            }

        });
        Bundle paginationState = savedInstanceState != null ? savedInstanceState.getBundle(POST_PAGINATION_STATE) : null;
        mPaginationHelper = new PaginationHelper(mRecyclerView, paginationState, this);
        getLoaderManager().initLoader(LOADER_POST, null, this);
    }

    @Override
    public void onRefresh() {
        mPaginationHelper.onRefresh();
        PostService postService = PeekMeServer.getPostService();
        postService.downloadLatestPostsAsync(this);
    }

    @Override
    public void onUserClick(long userId) {
        Intent intent = new Intent(getActivity(), ShowUserActivity.class);
        intent.putExtra(ShowUserActivity.USER, userId);
        startActivity(intent);
    }

    @Override
    public void onPostClick(long postId) {
        Intent intent = new Intent(getActivity(), ShowPostActivity.class);
        intent.putExtra(ShowPostActivity.POST, postId);
        startActivity(intent);
    }

    @Override
    public void onRatePost(final long postId) {
        final NumberPicker picker = new NumberPicker(getActivity());
        picker.setMaxValue(10);
        picker.setMinValue(1);
        new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_rate_title)
                .customView(picker, false)
                .positiveText(android.R.string.ok)
                .negativeText(android.R.string.cancel)
                .neutralText(R.string.action_remove)
                .onAny(new MaterialDialog.SingleButtonCallback() {

                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        PostService postService = PeekMeServer.getPostService();
                        switch (which) {
                            case POSITIVE:
                                postService.ratePostAsync(postId, picker.getValue(), mRateCallback);
                                break;
                            case NEUTRAL:
                                postService.unRatePostAsync(postId, mRateCallback);
                                break;
                        }
                    }

                })
                .show();
    }

    private ServerCallback<Post> mRateCallback = new ServerCallback<Post>() {

        @Override
        public void onRequestDone(Post item, ServerException e) {
            if (e != null) {
                // TODO show error
            } else {
                Uri uri = ContentUris.withAppendedId(CacheDataProvider.POST_URI, item.getId());
                getActivity().getContentResolver().notifyChange(uri, null);
            }
        }

    };

    @Override
    public void onRateListClick(long postId) {
        /*
        Intent intent = new Intent(getActivity(), ShowPostRatingActivity.class);
        intent.putExtra(ShowPostRatingActivity.POST, postId);
        startActivity(intent);*/
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        PostService postService = PeekMeServer.getPostService();
        postService.downloadLatestPostsAsync(this);
        String[] projections = new String[] {
                Schema.Post.POST_ID,
                Schema.Post.POST_DESCRIPTION,
                Schema.Post.POST_MEDIA_URL,
                Schema.Post.POST_MEDIA_MIME_TYPE,
                Schema.Post.POST_CREATED_AT,
                Schema.Post.POST_RATING_TOTAL,
                Schema.Post.POST_RATING_COUNT,
                Schema.Post.POST_COMMENT_COUNT,
                Schema.User.USER_ID,
                Schema.User.USER_USERNAME,
                Schema.Picture.PICTURE_URL,
                Schema.Picture.PICTURE_MIME_TYPE
        };
        String sortOrder = Schema.Post.POST_CREATED_AT + " DESC";
        Uri uri = CacheDataProvider.HOME_POST_URI;
        return new CursorLoader(getContext(), uri, projections, null, null, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.changeCursor(cursor);
        mProgressWheel.setVisibility(View.GONE);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        mHelpTextView.setVisibility(mAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }

    @Override
    public void onRequestDone(Metadata metadata, ServerException e) {
        if (e != null) {
            // TODO something went wrong while downloading data from server!
        } else {
            mPaginationHelper.onNewItems(metadata);
            Activity activity = getActivity();
            if (activity != null) {
                // fragment may be detached when async loader has finished his task!
                activity.getContentResolver().notifyChange(CacheDataProvider.HOME_POST_URI, null);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_SELECT_MEDIA) {
            if (resultCode == Activity.RESULT_OK) {
                Intent i = new Intent(getActivity(), EditorActivity.class);
                i.putExtra(EditorActivity.MEDIA_URI, intent.getData());
                startActivity(i);
            }
        } else if (requestCode == REQUEST_TAKE_MEDIA) {
            if (resultCode == Activity.RESULT_OK) {
                // TODO retrieve picture from camera activity and start EditorActivity
            }
        } else {
            super.onActivityResult(requestCode, resultCode, intent);
        }
    }

    @Override
    public void onHashTagClick(String hashTag) {
        Intent intent = new Intent(getActivity(), ShowTagActivity.class);
        intent.putExtra(ShowTagActivity.TAG, hashTag);
        startActivity(intent);
    }

    @Override
    public void onUserTagClick(final String userTag) {
        UserService userService = PeekMeServer.getUserService();
        userService.getUserProfileAsync(userTag, new ServerCallback<User>() {

            @Override
            public void onRequestDone(User user, ServerException e) {
                if (e != null) {
                    // TODO maybe the user not exists
                } else {
                    Intent intent = new Intent(getActivity(), ShowUserActivity.class);
                    intent.putExtra(ShowUserActivity.USER, user.getId());
                    startActivity(intent);
                }
            }

        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(POST_PAGINATION_STATE, mPaginationHelper.getSavedState());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPaginationHelper.release();
    }

    @Override
    public void onLoadMore(long lastId) {
        PostService postService = PeekMeServer.getPostService();
        postService.downloadLatestPostsAsync(lastId, this);
    }
}